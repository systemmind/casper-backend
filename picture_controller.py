from configuration import glob as settings
from casper_db import NotFoundError
import os
import base64

class PicturesController(object):
  def getPicture(self, name):
    filepath = os.path.join(settings.pictures(), name)
    return {
      "picture": {
        "encoding": "base64",
        "data": self._getBase64Picture(filepath)
      }
    }
    
  def getPicture200x200(self, name):
    filepath = os.path.join(settings.pictures(), "200x200", name)
    return {
      "picture": {
        "encoding": "base64",
        "data": self._getBase64Picture(filepath)
      }
    }

  def _getBase64Picture(self, filepath):
    if not os.path.isfile(filepath):
      raise NotFoundError("picture not found: " + filepath)

    with open(filepath, 'rb') as bites:
      tmp = bites.read()
      tmp = base64.b64encode(tmp).decode('ascii')
      return tmp

  def getPicturesByEmotion(self, emotionName):
    pictures = []
    for root, dirs, files in os.walk(settings.pictures()):
      for name in files:
        if emotionName.lower() in name:
          pictures.append(name)

    if not len(pictures):
      raise NotFoundError("pictures not found")

    return pictures
