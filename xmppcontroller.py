import os
import requests
import json
from casper_logger import Logger as logger
from configuration import glob as settings
import xmlrpc.client


class XmppController:
  def __init__(self):
    self._server = settings.xmppServer()
    self._admin = settings.xmppAdminUser()
    self._token = settings.xmppAdminToken()
    self._host = settings.xmppAdminHost()
    self._port = settings.xmppAdminPort()
    self._ejabberdctrl = settings.ejabberdctl()

  def createUser(self, user, passwd):
    url = f"http://{self._host}:{self._port}/api/register"

    data = {
      'user': user,
      'password': passwd,
      'host': self._server
    }

    res = requests.post(url, data=data)
    result = res.text

    if not res.ok:
      res.raise_fro_status()

    return result

  def sendMessage(self, type, user, message):
    url = f"http://{self._host}:{self._port}/api/send_message"

    body = {
      "message" : {
        type: message
      }
    }
    data = {
      "type": "normal",
      "from": f"{self._admin}@{self._server}",
      "to": f"{user}@{self._server}",
      "subject": "",
      "body": json.dumps(body)
    }   

    res = requests.post(url, data=data)

    if not res.ok:
      res.raise_for_status()
    
    return True

class XmppControllerXmlrpc:
  def __init__(self):
    self._proxy = xmlrpc.client.ServerProxy(
      f"http://{settings.xmppAdminHost()}:{settings.xmppAdminPort()}"
    )

    self._credentials = { 'user': settings.xmppAdminUser(),
                          'password': settings.xmppAdminToken(),
                          'server': settings.xmppServer(),
                          'admin': True }

  def _ctrl(self, command, payload):
    fn = getattr(self._proxy, command)
    return fn(self._credentials, payload)

  def createUser(self, user, passwd):
    return self._ctrl('register', {'user': f"user{user}",
                                   'host': settings.xmppServer(),
                                   'password': passwd})

  # create multiple users chat
  def createRoom(self, measurement):
    return self._ctrl('create_room', {'name': f"Measurement_{measurement}",
                                      'service': f"conference.{settings.xmppServer()}",
                                      'host': settings.xmppServer()})

  def addUserToRoom(self, user, measurement):
    return self._ctrl('set_room_affiliation', {'name': f"Measurement_{measurement}",
                                               'service': f"conference.{settings.xmppServer()}",
                                               'jid': f"user{user}@{settings.xmppServer()}",
                                               'affiliation': "member"})

  def addToRoster(self, localuser, item):
    return self._ctrl('add_rosteritem', {'localuser': f"user{localuser}",
                                         'localserver': settings.xmppServer(),
                                         'user': item,
                                         'server': settings.xmppServer(),
                                         'nick': item,
                                         'group': "Friends",
                                         'subs': "both"})

  def sendMessage(self, type, user, message):
    return self._ctrl(
      'send_message',
      {
        "type": "normal",
        "from": f"{settings.xmppAdminUser()}@{settings.xmppServer()}",
        "to": f"{user}@{settings.xmppServer()}",
        "subject": "",
        "body": message
      }
    )
  
  def xmppUserName(self, id):
    return f"user{id}"
