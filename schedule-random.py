#!/usr/bin/python3
# !!!!!!!!!!!!!!!!!! this script is deprecated, don't use it !!!!!!!!!!!!!!!!!!
import sys
import argparse
import json
import random
from datetime import datetime
from datetime import timedelta
from configuration import glob as settings
from casper_db import Database
from survey_controller import SurveyController
from notification_controller import NotificationController

connection = Database().getConnection()
cursor = connection.cursor(dictionary=True)
client = settings.client()

notificationController = NotificationController(connection)
surveyController = SurveyController()

usernotificationsset = []

for conf in config:
  measurement = int(conf["measurement"])
  indent = int(conf["indent"])
  questions = conf["questions"]
  loader = conf["loader"]
  expire = int(conf["expire"])
  divider = int(conf["userdiv"])

  cursor.execute("""
    SELECT users.id, users.token as usertoken, usersdevices.token as devicetoken
    FROM users
    LEFT JOIN usernotifications
      ON users.id = usernotifications.user
    LEFT JOIN usersdevices
      ON usersdevices.user = users.id
    WHERE users.measurement = %s
      AND users.authorized > 0
      AND usernotifications.date IS NULL
      AND users.measurement = usernotifications.measurement
      AND usernotifications.indent=%s;
  """, (measurement,indent))

  users = cursor.fetchall()
  userlen = len(users)
  if userlen < 1:
    continue

  cursor.execute("""
    SELECT COUNT(*)
    FROM users
    LEFT JOIN measurements
    ON users.measurement = measurements.id
    WHERE users.measurement = %s
      AND users.authorized > 0
      AND measurements.started > 0
      AND measurements.stop_date >= DATE(NOW());
  """, (measurement,))

  result = cursor.fetchone()
  if len(result) == 0:
    continue

  count = result["COUNT(*)"]
  if count > divider:
    count = int(count / divider)

  if count > userlen:
    count = userlen

  for i in random.sample(range(0, userlen), count):
    user = users[i]
    userId = user['id']
    userToken = user['usertoken']
    devicetoken = user["devicetoken"]

    surveyId = surveyController.createSurvey(
      userId,
      measurement,
      connection,
      datetime.now() + timedelta(minutes=expire)
    )

    surveyController.addQuestions(surveyId, questions, connection)
    link = notificationController.createSurveyLink(client, surveyId, loader, userToken)
    notificationController.createStaticNotification(
      userId,
      link,
      loader,
      datetime.now(),
      datetime.now() + timedelta(minutes=expire)
    )

    notificationController.sendSurveyXmppNotification(userId, link, loader)
    if devicetoken:
      notificationController.sendSurveyPushNotification(devicetoken, link, loader)

    usernotificationsset.append((surveyId, userId, measurement))


if len(usernotificationsset) > 0:
  cursor.executemany("""
    UPDATE usernotifications
       SET survey=%s, date=NOW(), body=NULL
    WHERE user=%s
      AND measurement=%s
      AND date IS NULL;
  """, usernotificationsset)

  connection.commit()
