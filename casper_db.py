#! /usr/bin/env python

from os import path
from singleton import Singleton
import datetime
import re
from casper_logger import Logger
import csv
from bcrypt import hashpw, gensalt
from configuration import glob as settings
from string import Template

import mysql.connector

HOST = settings.databaseHost()
#USER = Configuration().databaseUser()
#PASSWORD = Configuration().databasePassword()
#DATABASE = Configuration().databaseName()
USER = settings.databaseUser()
PASSWORD = settings.databasePassword()
DATABASE = settings.databaseName()

# admin_login = 'admin'
admin_password_hashed = '$2b$13$16au0MQ0SCJvVQcLjQ9EculgJ6/nulmP22YeX1ofcrt77P38K7QXm'

dateformat = '%Y-%m-%d'

def datetime2str(obj):
  def func(o):
    for key in o.keys():
      if isinstance(o[key], datetime.date):
        o[key] = o[key].strftime(dateformat)

  if isinstance(obj, list):
    for o in obj:
      func(o)
  elif isinstance(obj, dict):
    func(obj)
  else:
    raise Exception("unexpected argument type")

  return obj

class NotFoundError(Exception):
  def __init__(self, message):
    super(NotFoundError, self).__init__(message)

#class DatabaseImpl:
class Database:
  def __init__(self, host=None, user=None, database=None, password=None):
    self.db = None
    self.cursor = None
    self.host = host or settings.databaseHost()
    self.user = user or settings.databaseUser()
    self.database = database or settings.databaseName()
    self.password = password or settings.databasePassword()

  def connect(self):
    self.db = mysql.connector.connect(
      host=self.host,
      user=self.user,
      password=self.password,
      database=self.database,
      use_unicode=True
    )

    #self.cursor = self.db.cursor(dictionary=True)

  def close(self):
    pass
    """
    if self.db is not None:
      Logger.debug("Closing database...")
      self.db.close()
      self.db = None
    """

  def getConnection(self):
    connection = mysql.connector.connect(
      host=self.host,
      user=self.user,
      password=self.password,
      database=self.database,
      use_unicode=True
    )

    cursor = connection.cursor(dictionary=True)
    cursor.execute(f"USE {self.database};")
    return connection

  def init(self, connection=None):
    if not connection:
        connection = self.getConnection()

    cursor = connection.cursor(dictionary=True)
    cursor.execute(f"CREATE DATABASE IF NOT EXISTS {self.database};")
    cursor.execute(f"USE {self.database};")

    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        user(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
                    user VARCHAR(129), \
                    hashed_password VARCHAR(129));
    """)

    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        welcome_emails(
          id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
          path VARCHAR(320)
        );
    """)

    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        measurements(
          id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
          user int not null, \
          start_date DATE, \
          stop_date DATE, \
          name VARCHAR(255), \
          company_url VARCHAR(320), \
          manager_email VARCHAR(320), \
          welcome_email VARCHAR(320), \
          welcome_email_id INT, \
          started INT, \
          FOREIGN KEY (user) REFERENCES user (id));
    """)

    cursor.execute("""
      create table if not exists \
      measurementemails(
        id int not null auto_increment primary key, \
        host varchar(64) not null, \
        port int not null, \
        user varchar(64)  not null, \
        password varchar(64)  not null \
      );
    """)

    cursor.execute("""
      create table if not exists \
      measurementtelegrams(
        id int not null auto_increment primary key, \
        api_id varchar(64) not null, \
        api_hash varchar(32) not null, \
        user varchar(64) not null, \
        public_keys varchar(2304) \
      );
    """)

    # rename users to employees
    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        users(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
              token VARCHAR(129), \
              date DATE, \
              authorized INT(1), \
              phone VARCHAR(32), \
              name TEXT, \
              measurement INT,  \
              FOREIGN KEY (measurement) REFERENCES measurements (id));
    """)

    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        useremails(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
          user INT, \
          email VARCHAR(256), \
          FOREIGN KEY (user) REFERENCES users (id));
    """)

    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        usersdevices(
          id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
          user INT, \
          token VARCHAR(164), \
          FOREIGN KEY (user) REFERENCES users (id));
    """)

    # schedule_discrete - seconds
    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        schedulers( \
          id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
          measurement INT, \
          begin TIME, \
          end TIME, \
          schedule_discrete INT, \
          survey_expiration INT, \
          loader VARCHAR(32), \
          template TEXT, \
          remind INT, \
          type VARCHAR(32), \
          FOREIGN KEY (measurement) REFERENCES measurements (id) \
        ); \
    """)


    cursor.execute("""
      ALTER TABLE 
        schedulers
          MODIFY COLUMN template TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;
    """)

    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        survey(
          id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
          begin DATETIME(6), \
          end DATETIME(6), \
          user INT NOT NULL, \
          measurement INT NOT NULL, \
          scheduler INT NOT NULL, \
          FOREIGN KEY (measurement) REFERENCES measurements (id), \
          FOREIGN KEY (user) REFERENCES users (id), \
          FOREIGN KEY (scheduler) REFERENCES schedulers (id));
    """)

    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        notifications(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
              user INT, \
              body TEXT, \
              created DATETIME, \
              expired DATETIME, \
              delivered DATETIME, \
              FOREIGN KEY (user) REFERENCES users (id));
    """)

    # this table will keep all notifications that are sent to users
    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        usernotifications( \
          id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
          measurement INT, \
          user INT, \
          survey INT, \
          indent INT, \
          date DATETIME, \
          body TEXT, \
          FOREIGN KEY (measurement) REFERENCES measurements (id), \
          FOREIGN KEY (user) REFERENCES users (id), \
          FOREIGN KEY (survey) REFERENCES survey (id) \
        ); \
    """)

    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        questions(
          name VARCHAR(128) NOT NULL PRIMARY KEY, \
          priority INT, \
          type VARCHAR(128) \
        ); \
    """)

    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        emails_templates(
          id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
          user int,
          title VARCHAR(128), \
          body TEXT, \
          type VARCHAR(32), \
          foreign key (user) references user (id) \
        ); \
    """)


    cursor.execute("""
      ALTER TABLE 
        emails_templates
          MODIFY COLUMN title TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;
    """)    


    cursor.execute("""
      ALTER TABLE 
        emails_templates
          MODIFY COLUMN body TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;
    """) 


    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        scheduler_questions( \
          id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
          scheduler INT, \
          question VARCHAR(128), \
          FOREIGN KEY (scheduler) REFERENCES schedulers (id), \
          FOREIGN KEY (question) REFERENCES questions (name) \
        ); \
    """)


    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        scheduler_emails(
          id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
          scheduler INT, \
          email INT, \
          FOREIGN KEY (scheduler) REFERENCES schedulers (id), \
          FOREIGN KEY (email) REFERENCES emails_templates (id) \
        ); \
    """)

    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        employeequestions(
          id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
          survey INT, \
          ordern INT, \
          answered BOOLEAN, \
          skipped BOOLEAN, \
          postponed BOOLEAN, \
          FOREIGN KEY (survey) REFERENCES survey (id) \
        ); \
    """)

    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        numberinputs(
          id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
          value INT, \
          question INT NOT NULL, \
          ordern INT, \
          name VARCHAR(64), \
          indent VARCHAR(256), \
          timestamp DATETIME, \
          FOREIGN KEY (question) REFERENCES employeequestions (id) \
        ); \
    """)


    cursor.execute("""
      ALTER TABLE 
        numberinputs
          MODIFY COLUMN value TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;
    """) 


    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        textinputs( \
          id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
          value TEXT, \
          question INT NOT NULL, \
          ordern INT, \
          name VARCHAR(64), \
          indent VARCHAR(256), \
          timestamp DATETIME, \
          FOREIGN KEY (question) REFERENCES employeequestions (id) \
        ); \
    """)

    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        emotions(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
          emotion VARCHAR(129));
    """)

    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        facts(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
                    body TEXT, \
                    emotion INT, \
                    FOREIGN KEY (emotion) REFERENCES emotions (id));
    """)

    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        tips(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
                    body TEXT, \
                    emotion INT, \
                    FOREIGN KEY (emotion) REFERENCES emotions (id));
    """)

    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        feedback(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
              user INT, \
              date DATE, \
              fact INT, \
              tip INT, \
              response TEXT, \
              emotion INT, \
              picture VARCHAR(129), \
              FOREIGN KEY (user) REFERENCES users (id), \
              FOREIGN KEY (fact) REFERENCES facts (id), \
              FOREIGN KEY (emotion) REFERENCES emotions (id), \
              FOREIGN KEY (tip) REFERENCES tips (id));
    """)

    for name in ['admin', 'root']:
      cursor.execute(f"""SELECT user FROM user where user='{name}'""")
      if not cursor.fetchall():
        self.addAdmin(name, admin_password_hashed, cursor)

      #self.cursor.execute("""INSERT INTO user(user, hashed_password) VALUES (%s,%s);""", ( admin_login, admin_password_hashed))

    cursor.execute("""
      CREATE TABLE IF NOT EXISTS \
        admin_token(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
                    user_id INT, \
                    token VARCHAR(129), \
                    expired DATETIME);
    """)

    connection = self.getConnection()
    self.initQuestions(connection)
    # self.initEmotions(connection)
    # self.initFacts(connection)
    # self.initTips(connection)
    self.close()
    connection.close()

  def initQuestions(self, connection):
    cursor = connection.cursor(dictionary=True)
    statement = f"""
      SELECT TABLE_NAME
        FROM information_schema.tables
      WHERE table_schema = '{self.database}'
        AND  TABLE_NAME LIKE 'question__%'
    """
    cursor.execute(statement)
    tables = cursor.fetchall()
    for table in tables:
      question = re.search('(?<=_)(\\w*)', table['TABLE_NAME']).group(0)
      cursor.execute("SELECT * FROM questions WHERE name = %s;", (question,))
      if not cursor.fetchone():
       cursor.execute(""" INSERT INTO questions(name) VALUES (%s);""", ( [question] ))

    cursor.execute("""UPDATE questions SET type = 'static' WHERE name = 'activity' AND type IS NULL""")
    cursor.execute("""UPDATE questions SET type = 'static' WHERE name = 'feeling' AND type IS NULL""")
    cursor.execute("""UPDATE questions SET type = 'static' WHERE name = 'reason' AND type IS NULL""")
    cursor.execute("""UPDATE questions SET type = 'static' WHERE name = 'duration' AND type IS NULL""")
    cursor.execute("""UPDATE questions SET type = 'static' WHERE name = 'todo' AND type IS NULL""")
    cursor.execute("""UPDATE questions SET type = 'static' WHERE name = 'agree' AND type IS NULL""")
    cursor.execute("""UPDATE questions SET type = 'demographic' WHERE name = 'age' AND type IS NULL""")
    cursor.execute("""UPDATE questions SET type = 'demographic' WHERE name = 'experience' AND type IS NULL""")
    cursor.execute("""UPDATE questions SET type = 'demographic' WHERE name = 'gender' AND type IS NULL""")
    cursor.execute("""UPDATE questions SET type = 'target' WHERE name = 'happiness' AND type IS NULL""")
    cursor.execute("""UPDATE questions SET type = 'target' WHERE name = 'productivity' AND type IS NULL""")
    cursor.execute("""UPDATE questions SET type = 'target' WHERE name = 'rubbishbin' AND type IS NULL""")
    cursor.execute("""UPDATE questions SET type = 'target' WHERE name = 'goodactivities' AND type IS NULL""")

    connection.commit()

  def initEmotions(self, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM emotions""")
    emotions = cursor.fetchall()
    cursor.close()
    if not emotions:
      Logger.debug(f"restore emotions from backup")
      self.executeBackUp(path.join(settings.dbDumps(), 'emotions.sql'), connection)

  def initFacts(self, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM facts""")
    facts = cursor.fetchall()
    cursor.close()
    if not facts:
      Logger.debug(f"restore facts from backup")
      self.executeBackUp(path.join(settings.dbDumps(), 'facts.sql'), connection)

  def initTips(self, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM tips""")
    tips = cursor.fetchall()
    cursor.close()
    if not tips:
      Logger.debug(f"restore tips from backup")
      self.executeBackUp(path.join(settings.dbDumps(), 'tips.sql'), connection)

  def collect_data(self, connection, measurement_id):
    cursor = connection.cursor(dictionary=True)
    def export(filename, title, query):
      with open(filename, mode = 'w') as data_file:
        data_writer = csv.DictWriter(data_file, fieldnames=title, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        data_writer.writeheader()
        cursor.execute(query)
        table = cursor.fetchall()
        for item in table:
          data_writer.writerow(item)

    filename = 'data/measurement%s_survey.csv' % measurement_id
    title = ['id', 'user', 'begin', 'end']
    query = 'SELECT id, user, begin, end FROM casper.survey WHERE measurement = %s;' % measurement_id
    export(filename=filename, title=title, query=query)

    filename = 'data/measurement%s_activity.csv' % measurement_id
    title = ['survey', 'date', 'time', 'shown', 'activity']
    query = 'SELECT survey, date, time, shown, activity FROM casper.question_activity WHERE survey IN (SELECT id FROM casper.survey WHERE measurement = %s);' % measurement_id
    export(filename=filename, title=title, query=query)

    filename = 'data/measurement%s_age.csv' % measurement_id
    title = ['survey', 'date', 'time', 'shown', 'age']
    query = 'SELECT survey, date, time, shown, age FROM casper.question_age WHERE survey IN (SELECT id FROM casper.survey WHERE measurement = %s);' % measurement_id
    export(filename=filename, title=title, query=query)

    filename = 'data/measurement%s_agree.csv' % measurement_id
    title = ['survey', 'date', 'time', 'shown', 'meaning', 'useful', 'achievement']
    query = 'SELECT survey, date, time, shown, meaning, useful, achievement FROM casper.question_agree WHERE survey IN (SELECT id FROM casper.survey WHERE measurement = %s);' % measurement_id
    export(filename=filename, title=title, query=query)

    filename = 'data/measurement%s_experience.csv' % measurement_id
    title = ['survey', 'date', 'time', 'shown', 'experience']
    query = 'SELECT survey, date, time, shown, experience FROM casper.question_experience WHERE survey IN (SELECT id FROM casper.survey WHERE measurement = %s);' % measurement_id
    export(filename=filename, title=title, query=query)

    filename = 'data/measurement%s_feeling.csv' % measurement_id
    title = ['survey', 'date', 'time', 'shown', 'emotion', 'value']
    query = 'SELECT survey, date, time, shown, emotion, value FROM casper.question_feeling WHERE survey IN (SELECT id FROM casper.survey WHERE measurement = %s);' % measurement_id
    export(filename=filename, title=title, query=query)

    filename = 'data/measurement%s_reason.csv' % measurement_id
    title = ['survey', 'date', 'time', 'shown', 'reason']
    query = 'SELECT survey, date, time, shown, reason FROM casper.question_reason WHERE survey IN (SELECT id FROM casper.survey WHERE measurement = %s);' % measurement_id
    export(filename=filename, title=title, query=query)

    filename = 'data/measurement%s_gender.csv' % measurement_id
    title = ['survey', 'date', 'time', 'shown', 'gender']
    query = 'SELECT survey, date, time, shown, gender FROM casper.question_gender WHERE survey IN (SELECT id FROM casper.survey WHERE measurement = %s);' % measurement_id
    export(filename=filename, title=title, query=query)

    filename = 'data/measurement%s_duration.csv' % measurement_id
    title = ['survey', 'date', 'time', 'shown', 'duration']
    query = 'SELECT survey, date, time, shown, duration FROM casper.question_duration WHERE survey IN (SELECT id FROM casper.survey WHERE measurement = %s);' % measurement_id
    export(filename=filename, title=title, query=query)

    filename = 'data/measurement%s_todo.csv' % measurement_id
    title = ['survey', 'date', 'time', 'shown', 'have', 'want', 'should']
    query = 'SELECT survey, date, time, shown, have, want, should FROM casper.question_todo WHERE survey IN (SELECT id FROM casper.survey WHERE measurement = %s);' % measurement_id
    export(filename=filename, title=title, query=query)

    filename = 'data/measurement%s_happiness.csv' % measurement_id
    title = ['survey', 'date', 'time', 'shown', 'happiness']
    query = 'SELECT survey, date, time, shown, happiness FROM casper.question_happiness WHERE survey IN (SELECT id FROM casper.survey WHERE measurement = %s);' % measurement_id
    export(filename=filename, title=title, query=query)

    filename = 'data/measurement%s_productivity.csv' % measurement_id
    title = ['survey', 'date', 'time', 'shown', 'productivity']
    query = 'SELECT survey, date, time, shown, productivity FROM casper.question_productivity WHERE survey IN (SELECT id FROM casper.survey WHERE measurement = %s);' % measurement_id
    export(filename=filename, title=title, query=query)

    filename = 'data/measurement%s_rubbishbin.csv' % measurement_id
    title = ['survey', 'date', 'time', 'shown', 'activity']
    query = 'SELECT survey, date, time, shown, activity FROM casper.question_rubbishbin WHERE survey IN (SELECT id FROM casper.survey WHERE measurement = %s);' % measurement_id
    export(filename=filename, title=title, query=query)

    filename = 'data/measurement%s_goodactivities.csv' % measurement_id
    title = ['survey', 'date', 'time', 'shown', 'activity']
    query = 'SELECT survey, date, time, shown, activity FROM casper.question_goodactivities WHERE survey IN (SELECT id FROM casper.survey WHERE measurement = %s);' % measurement_id
    export(filename=filename, title=title, query=query)

  def executeBackUp(self, fileName, connection):
    cursor = connection.cursor(dictionary=True)
    with open(fileName, 'r', encoding="utf-8") as file:
      backup = file.read()
      for result in cursor.execute(backup, multi=True):
        pass

      connection.commit()
      cursor.close()

  def addAdmin(self, name, password, cursor):
    cursor.execute(
      """INSERT INTO user(user, hashed_password) VALUES (%s,%s);""",
      (name, password)
    )


'''
class Database(Singleton, DatabaseImpl):
  pass
'''
