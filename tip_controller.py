from casper_db import NotFoundError
from emotion_controller import EmotionsController

class TipsController(object):
  def insertTip(self, body, emotionId, connection):
    EmotionsController().getEmotionById(emotionId, connection)
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""INSERT INTO tips (body, emotion)
                      VALUES(%s, %s);""", (body, emotionId))
    id = cursor.lastrowid
    connection.commit()
    return self.getTipById(id, connection)

  def deleteTip(self, tipId, connection):
    tip = self.getTipById(tipId, connection)
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""DELETE from tips WHERE id = %s""", (tipId, ))
    connection.commit()
    return tip

  def getTipById(self, tipId, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM tips WHERE id = %s;""", (tipId,))
    tip = cursor.fetchone()
    if not tip:
      raise NotFoundError("tip not found")

    return tip

  def getTipsByEmotion(self, emotionId, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM tips WHERE emotion = %s;""", (emotionId,))
    tips = cursor.fetchall()
    if not len(tips):
      raise NotFoundError("tip not found")

    return tips

  def editTip(self, tipId, body, connection):
    self.getTipById(tipId, connection)
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""UPDATE tips SET body = %s WHERE id = %s""", (body, tipId))
    connection.commit()
    return self.getTipById(tipId, connection)
