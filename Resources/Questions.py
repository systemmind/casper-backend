from flask import request, jsonify
from flask_restful import Resource
from Resources.utils import checkAdminToken
from question_table_controller import QuestionTableController

class Questions(Resource):
  @checkAdminToken
  def get(self, connection, user, token):
    ctrl = QuestionTableController()
    return jsonify(ctrl.all(connection))
