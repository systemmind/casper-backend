from flask import request, jsonify, json
from flask_restful import Resource
from Resources.utils import checkEmployeeRequestPreconditions, parseGetFilters
import Models.EmployeeQuestions as Questions

jd = json.dumps
jl = json.loads

class EmployeeQuestions(Resource):
  @checkEmployeeRequestPreconditions
  @parseGetFilters
  def get(self, connection, filters=None, op=None, **kwargs):
    questions = jl(jd(Questions.read(connection.cursor(dictionary=True), filters=filters, operator=op, **kwargs)))
    return questions, 200

  @checkEmployeeRequestPreconditions
  def post(self, connection, survey, **kwargs):
    body = request.get_json()
    index = Questions.create(connection.cursor(), survey=survey, **body)
    connection.commit()
    return {'id': index}, 200


class EmployeeQuestion(Resource):
  @checkEmployeeRequestPreconditions
  def get(self, connection, question, **kwargs):
    questions = jl(jd(Questions.read(connection.cursor(dictionary=True), question=question, **kwargs)))
    return (dict(questions[0]), 200) if len(questions) > 0 else ({}, 404)

  @checkEmployeeRequestPreconditions
  def put(self, connection, question, **kwargs):
    body = request.get_json() or {}
    rowcount = Questions.edit(connection.cursor(), question, **body)
    connection.commit()
    return {'rowcount': rowcount}, 200
