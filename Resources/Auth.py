from flask import request
from flask_restful import Resource
from Resources.utils import checkAdminToken, checkAdminPassword
from Models import Auth as AuthModel


class Login(Resource):
  @checkAdminPassword
  def post(self, connection, user):
    token = AuthModel.createToken(connection.cursor(), user)
    connection.commit()
    return {'token': token, 'user': user}, 200


class Auth(Resource):
  @checkAdminToken
  def put(self, connection, user, token, action=None):
    if action == 'token':
      rowcount = AuthModel.updateToken(connection.cursor(), user, token)
      connection.commit()
      return {'rowcount': rowcount}
    elif action == 'password':
      password = request.get_json()['password']
      rowcount = AuthModel.editPassword(connection.cursor(), user, password)
      connection.commit()
      return {'rowcount': rowcount}
    else:
      return {'error': 'undefined action'}, 403
