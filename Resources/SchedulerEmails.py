from flask import request
from flask_restful import Resource
from Resources.utils import checkAdminToken, checkUserMeasurementPermission
import Models.SchedulerEmails as Emails


class SchedulerEmails(Resource):
  @checkAdminToken
  @checkUserMeasurementPermission
  def get(self, connection, user, token, measurement, scheduler):
    return Emails.read(connection.cursor(dictionary=True), scheduler=scheduler)

  @checkAdminToken
  @checkUserMeasurementPermission
  def delete(self, connection, user, token, measurement, scheduler):
    index = Emails.delete(connection.cursor(dictionary=True), scheduler=scheduler)
    connection.commit()
    return {}, 200


class SchedulerEmail(Resource):
  @checkAdminToken
  @checkUserMeasurementPermission
  def post(self, connection, user, token, measurement, scheduler, email):
    index = Emails.create(connection.cursor(dictionary=True), scheduler=scheduler, email=email)
    connection.commit()
    return {'id': index}, 200

  @checkAdminToken
  @checkUserMeasurementPermission
  def delete(self, connection, user, token, measurement, scheduler, email):
    index = Emails.delete(connection.cursor(dictionary=True), scheduler=scheduler, email=email)
    connection.commit()
    return {}, 200
