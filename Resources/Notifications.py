from flask import request
from flask_restful import Resource
from Resources.utils import checkAdminToken
import Notification


class Notifications(Resource):
  @checkAdminToken
  def post(self, connection, user, token):
    for kind in dict(request.json).keys():
      ctrl = Notification.create(kind, **(request.json[kind]))
      ctrl.send(connection)
