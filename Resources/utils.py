from flask import request
from casper_logger import Logger
import flask.json as json
from admin_controller import AdminController
from user_controller import UserController, AuthorizationError
from casper_db import Database
from Models.utils import matchSqlOperator, matchNumChar
from Models import Auth
from Models import Users
from Models import Measurements
from Models import Emails as EmailsTemplates

def retError(error):
  return {'error': error}

def checkAdminToken(func):
  def decorator(self, user, **kwargs):
    token = request.headers.get('token-Authorization')
    if not token:
      return 'missed token', 412

    connection = Database().getConnection()
    if not Auth.verifyToken(connection.cursor(), user, token):
      return 'access denied', 401

    return func(self, connection, user, token, **kwargs)

  return decorator


def checkRootToken(func):
  def decorator(self, **kwargs):
    token = request.headers.get('token-Authorization')
    if not token:
      return 'missed token', 412

    connection = Database().getConnection()
    if not Auth.verifyRootToken(connection.cursor(), token):
      return 'access denied', 401

    return func(self, connection, token, **kwargs)

  return decorator


def checkRootOrAdminToken(func):
  def decorator(self, user, **kwargs):
    token = request.headers.get('token-Authorization')
    if not token:
      return 'missed token', 412

    connection = Database().getConnection()
    if not Auth.verifyRootToken(connection.cursor(), token) and not Auth.verifyToken(connection.cursor(), user, token):
      return 'access denied', 401

    return func(self, connection, user, token, **kwargs)

  return decorator


def checkAdminPassword(func):
  def decorator(self, **kwargs):
    connection = Database().getConnection()
    obj = request.get_json()
    login = obj['login']
    passwd = obj['password']
    user = Auth.checkPassword(connection.cursor(dictionary=True), login, passwd)
    return func(self, connection, user, **kwargs)

  return decorator


def checkIsRoot(func):
  def decorator(self, connection, user, *args, **kwargs):
    isRoot = Users.isRoot(connection.cursor(), user)
    return func(self, connection, isRoot, user, *args, **kwargs)

  return decorator


def checkUserEmailTemplatePermission(func):
  def decorator(self, connection, user, token, *args, email=None, **kwargs):
    error = (retError('forbidden'), 403)

    if not email:
      return error

    cursor = connection.cursor()
    if not EmailsTemplates.isUserValid(cursor, email, user) and not Auth.verifyRootToken(cursor, token):
      return error

    return func(self, connection, user, token, *args, email=email, **kwargs)

  return decorator


def checkUserMeasurementPermission(func):
  def decorator(self, connection, user, token, *args, measurement=None, **kwargs):
    error = (retError('forbidden'), 403)

    if not measurement:
      return error

    cursor = connection.cursor()
    if not Measurements.isUserValid(cursor, measurement, user) and not Auth.verifyRootToken(cursor, token):
      return error

    return func(self, connection, user, token, *args, measurement=measurement, **kwargs)

  return decorator

"""
def checkEmployeeToken(func):
  def decorator(self, user, **kwargs):
    try:
      token = request.headers.get('token-Authorization')
      if not token:
        return 'missed token', 412

      '''
      user = kwargs['user']
      if not user:
        return 'missed user', 400
      '''

      connection = Database().getConnection()
      control = UserController()
      user2 = control.findUserByToken(token, connection)["id"]
      if user != user2:
        return 'access denied', 401

      return func(self, connection, user=user, **kwargs)
    except AuthorizationError as error:
      return f"{error}", 401

  return decorator
"""

'''
def checkEmployeeSurveyToken(User=False, Survey=False):
  def decorator(func):
    def wrapper(self, user, survey, **kwargs):
      token = request.headers.get('token-Authorization')
      if not token:
        return 'missed token', 412
      
      connection = Database().getConnection()
      cursor = connection.cursor()
      cursor.execute("""
      select user.user, users.token, survey.id as survey
        from users
  left join survey
          on users.id = survey.user
      where user.id = %s
        and survey.id = %s
      """, (user, survey))
      user, token, survey = cursor.fetchone()
      if (user and token and survey:
        if User:
          kwargs["user"] = user
        if Survey:
          kwargs["survey"] = survey
        return func(self, connection.cursor(dictionary=True), **kwargs)
      else:
        return "forbidden", 403
    
    return wrapper
  return decorator
'''

def getUsersQuery(func):
  def decorator(self, **kwargs):
    sEmails = request.args.get("emails")
    sDevices = request.args.get("devices")
    return func(self, emails=(sEmails == '1'), devices=(sDevices == '1'), **kwargs)
  return decorator

def checkEmployeeRequestPreconditions(func):
  def decorator(self, user, survey=None, question=None, control=None, **kwargs):
    sqlSelect = ["users.*"]
    sqlJoin = []
    sqlWhere = ["users.id=%s"]
    values = (user,)
    kwargs["user"] = user

    token = request.headers.get('token-Authorization')
    if not token:
      return 'missed token', 412

    sqlWhere.append("users.token = %s")
    values = values + (token,)

    query = request.args.get("q")
    args = json.loads(query) if query else []
    controlType = None
    if "controlType" in args:
      controlType = matchNumChar(args["controlType"])
      kwargs["controlType"] = controlType

    if survey:
      survey = int(survey)
      sqlSelect.append("survey.id as survey")
      sqlJoin.append("left join survey on users.id = survey.user")
      sqlWhere.append("survey.id = %s")
      values = values + (survey,)
      kwargs["survey"] = survey

      if question:
        question = int(question)
        sqlSelect.append("employeequestions.id as question")
        sqlJoin.append("left join employeequestions on employeequestions.survey = survey.id")
        sqlWhere.append("employeequestions.id = %s")
        values = values + (question,)
        kwargs["question"] = question

    if control:
      control = int(control)
      if not controlType:
        return "control type is not defined", 400

      sqlSelect.append(f"{controlType}s.id as {controlType}")
      if not survey and not question:
        sqlJoin.append("left join survey on users.id = survey.user")
        sqlJoin.append("left join employeequestions on employeequestions.survey = survey.id")

      sqlJoin.append(f"left join {controlType}s on {controlType}s.question = employeequestions.id")
      sqlWhere.append(f"{controlType}s.id = %s")
      values = values + (control,)
      kwargs["control"] = control

    sqlQuery = f"""
    select  {",".join(sqlSelect)}
      from  users
            {" ".join(sqlJoin)}
     where  {" and ".join(sqlWhere)}
    """

    connection = Database().getConnection()
    cursor = connection.cursor()
    cursor.execute(sqlQuery, values)
    result = cursor.fetchall()
    if len(result) < 1:
      return "not found", 404

    if len(result) > 1:
      return "ambiguous", 300

    return func(self, connection, **kwargs)
  return decorator


def parseGetFilters(func):
  def decorator(self, *args, **kwargs):
    query = request.args.get("q")
    obj = json.loads(query) if query else {}
    filters = None
    op = None
    if "filters" in obj:
      filters = obj["filters"]
      op = matchSqlOperator(obj["op"] if "op" in obj else "and")

    return func(self, *args, filters=filters, op=op, **kwargs)

  return decorator
