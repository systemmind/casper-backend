# This is admin users model

from flask import request, json
from flask_restful import Resource
from bcrypt import hashpw, gensalt
from Resources.utils import checkAdminToken, checkRootToken, checkRootOrAdminToken
import Models.Users as UsersModel


class Users(Resource):
  @checkRootToken
  def post(self, connection, token, **kwargs):
    body = request.get_json()
    password = body.pop('password', None)
    if not password:
      return {'error': 'invalid password'}, 403

    lastrowid = UsersModel.create(connection.cursor(dictionary=True), hashed_password=hashpw(password.encode('utf-8'), gensalt()),  **body, **kwargs)
    connection.commit()
    return {'id': lastrowid}, 200

  @checkRootToken
  def get(self, connection, token, **kwargs):
    return UsersModel.read(connection.cursor(dictionary=True), **kwargs)


class User(Resource):
  @checkRootOrAdminToken
  def get(self, connection, user, token, **kwargs):
    result = UsersModel.read(connection.cursor(dictionary=True), id=user, **kwargs)
    if len(result):
      return result[0], 200

    return {}, 404

  @checkRootToken
  def delete(self, connection, token, user=None, **kwargs):
    if not user:
      return {'error': 'user id missing'}, 403

    rowcount = UsersModel.delete(connection.cursor(dictionary=True), id=user, **kwargs)
    connection.commit()
    return {'rowcount': rowcount}, 200
