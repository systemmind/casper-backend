from flask import request, json
from flask_restful import Resource
from Resources.utils import checkAdminToken, checkUserMeasurementPermission
from Models.utils import match
import Models.Surveys as SurveysModel

jl = json.loads
jd = json.dumps

class Surveys(Resource):
  @checkAdminToken
  @checkUserMeasurementPermission
  def post(self, connection, user, token, employee=None, **kwargs):
    body = request.get_json()
    index = SurveysModel.create(connection.cursor(), user=employee, **body, **kwargs)
    connection.commit()
    return {'id': index}, 200


from notification_controller import NotificationController
from configuration import glob as settings
import Models.Employees as EmployeesModel

class SurveyLinks(Resource):
  @checkAdminToken
  @checkUserMeasurementPermission
  def get(self, connection, user, token, measurement, survey=None):
    employee = int(request.args.get("employee"))
    loader = match(request.args.get("loader"), "^[0-9|A-z]+$")
    userMysql = EmployeesModel.read(connection.cursor(dictionary=True), user=employee, measurement=measurement)
    if len(userMysql) != 1:
      return {'error': 'forbidden'}, 403

    token = userMysql[0]['token']
    ctrl = NotificationController(connection)   # TODO: we don't need this actually, remove this line after NotificationController refactroing
    link = ctrl.createSurveyLink(settings.client(), employee, survey, loader, token)
    return {'link': link}, 200
