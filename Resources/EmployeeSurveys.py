from flask import request, jsonify, json
from flask_restful import Resource
from Resources.utils import checkEmployeeRequestPreconditions, parseGetFilters
import Models.EmployeeSurveys as Surveys


class EmployeeSurveys(Resource):
  @checkEmployeeRequestPreconditions
  @parseGetFilters
  def get(self, connection, user, filters=None, op=None):
    # TODO: fix datetime serrialization and remove excess json.dumps, json.load calls
    surveys = json.dumps(Surveys.read(connection.cursor(dictionary=True), user=user, filters=filters, operator=op, template=True))
    return json.loads(surveys), 200


class EmployeeSurvey(Resource):
  @checkEmployeeRequestPreconditions
  def get(self, connection, survey, **kwargs):
    surveys = Surveys.read(connection.cursor(dictionary=True), survey=survey, template=True, **kwargs)
    # TODO: fix datetime serrialization and remove excess json.dumps, json.load calls
    surveys = json.loads(json.dumps(surveys))
    return (dict(surveys[0]), 200) if len(surveys) > 0 else ({}, 404)
