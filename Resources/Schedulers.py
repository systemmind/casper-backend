from flask import request, json
from flask_restful import Resource
from Resources.utils import checkAdminToken, checkUserMeasurementPermission
import Models.Schedulers as SchedulersModel
import Models.EmployeeControls as EmployeeControls
import Models.EmployeeQuestions as EmployeeQuestions
import Models.EmployeeNotifications as EmployeeNotifications
import Models.SchedulerQuestions as Questions
import Models.Surveys as Surveys
import Models.SchedulerEmails as Emails

jl = json.loads
jd = json.dumps

# TODO: remove SchedulerQuestions model because we should use scheduler template
class Schedulers(Resource):
  @checkAdminToken
  @checkUserMeasurementPermission
  def get(self, connection, user, token, **kwargs):
    cursor = connection.cursor(dictionary=True)
    schedulers = jl(jd(SchedulersModel.read(cursor, **kwargs), default=str))
    for scheduler in schedulers:
      scheduler["questions"] = list(map(lambda x: x['question'], Questions.read(cursor, scheduler["id"])))

    return schedulers

  @checkAdminToken
  @checkUserMeasurementPermission
  def post(self, connection, user, token, measurement, **kwargs):
    cursor = connection.cursor()
    questions = []
    body = request.get_json()
    if "questions" in body:
      questions = body["questions"]
      del body["questions"]

    index = SchedulersModel.create(cursor, measurement=measurement, **body)
    if len(questions):
      Questions.create(cursor, scheduler=index, questions=questions)

    connection.commit()
    return {'id': index}, 200

  @checkAdminToken
  @checkUserMeasurementPermission
  def delete(self, connection, user, token, measurement):
    cursor = connection.cursor()
    Questions.delete(cursor, measurement=measurement)
    Emails.delete(cursor, measurement=measurement)
    rowcount = SchedulersModel.delete(cursor, measurement=measurement)
    connection.commit()

    return {'rowcount': rowcount}, 200


class Scheduler(Resource):
  @checkAdminToken
  @checkUserMeasurementPermission
  def get(self, connection, user, token, scheduler, **kwargs):
    cursor = connection.cursor(dictionary=True)
    result = SchedulersModel.read(cursor, scheduler=scheduler)
    if len(result):
      scheduler = result[0]
      scheduler['questions'] = list(map(lambda x: x['question'], Questions.read(cursor, scheduler["id"])))
      return jl(jd(scheduler, default=str)), 200

    return {}, 404

  @checkAdminToken
  @checkUserMeasurementPermission
  def put(self, connection, user, token, scheduler, **kwargs):
    cursor = connection.cursor()
    result = SchedulersModel.read(cursor, scheduler=scheduler)
    questions = []
    body = request.get_json()
    if "questions" in body:
      questions = body["questions"]
      del body["questions"]

    rowcount = SchedulersModel.edit(cursor, scheduler, **body)
    if len(questions):
      Questions.edit(cursor, scheduler=scheduler, questions=questions)

    connection.commit()
    return {'rowcount': rowcount}, 200

  @checkAdminToken
  @checkUserMeasurementPermission
  def delete(self, connection, user, token, scheduler, measurement=None, **kwargs):
    rowcount = 0
    cursor = connection.cursor()
    if scheduler:
      EmployeeControls.delete(cursor, "numberinput", scheduler=scheduler)
      EmployeeControls.delete(cursor, "textinput", scheduler=scheduler)
      EmployeeQuestions.delete(cursor, scheduler=scheduler)
      Questions.delete(cursor, scheduler=scheduler)
      EmployeeNotifications.delete(cursor, scheduler=scheduler)
      Surveys.delete(cursor, scheduler=scheduler)
      Emails.delete(cursor, scheduler=scheduler)
      rowcount = SchedulersModel.delete(cursor, scheduler=scheduler)
    elif measurement:
      rowcount = SchedulersModel.delete(cursor, measurement=measurement)

    connection.commit()
    
    return {'rowcount': rowcount}, 200
