from flask import request
from flask_restful import Resource
from Resources.utils import checkAdminToken, checkIsRoot, checkUserEmailTemplatePermission
import Models.Emails as Emails

class EmailsTemplates(Resource):
  @checkAdminToken
  @checkIsRoot
  def get(self, connection, isRoot, user, token):
    cursor = connection.cursor(dictionary=True)
    emails = Emails.read(cursor) if isRoot else Emails.read(cursor, user=user)
    return emails, 200

  @checkAdminToken
  @checkIsRoot
  def post(self, connection, isRoot, user, token):
    body = request.get_json()
    cursor = connection.cursor(dictionary=True)
    index = Emails.create(cursor, **body) if isRoot else Emails.create(cursor, user=user, **body)
    connection.commit()
    return {'id': index}, 200

class EmailsTemplate(Resource):
  @checkAdminToken
  @checkUserEmailTemplatePermission
  def put(self, connection, user, token, email):
    body = request.get_json()
    index = Emails.edit(connection.cursor(dictionary=True), id=email, **body)
    connection.commit()
    return {}, 200

  @checkAdminToken
  @checkUserEmailTemplatePermission
  def delete(self, connection, user, token, email):
    index = Emails.delete(connection.cursor(dictionary=True), id=email)
    connection.commit()
    return {}, 200
