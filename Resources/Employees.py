import Models.Employees as EmployeesModel
import Models.EmployeeEmails as EmployeeEmailsModel
import Models.EmployeeDevices as EmployeeDevicesModel
from flask import request, json
from Resources.utils import checkAdminToken, getUsersQuery
from flask_restful import Resource
from casper_db import Database
from admin_controller import AdminController

jl = json.loads
jd = json.dumps

class Employees(Resource):
  @getUsersQuery
  @checkAdminToken
  def get(self, connection, user, token, emails=False, devices=False, **kwargs):
    users = EmployeesModel.read(connection.cursor(dictionary=True), emails=emails, devices=devices, **kwargs)
    result = {}
    for user in users:
      tmp = result.setdefault(user["id"], user)
      if emails:
        useremail = user.pop('email', None)
        e = tmp.setdefault('emails', [])
        if useremail:
          e.append(useremail)

      if devices:
        useredevice = user.pop('device', None)
        d = tmp.setdefault('devices', [])
        if useredevice:
          d.append(useredevice)

    return jl(jd(list(result.values()))), 200


  @checkAdminToken
  def post(self, connection, user, token, **kwargs):
    body = request.get_json()
    isOneUser = isinstance(body, dict)
    if isOneUser:
      body = [body]

    allUsers = []

    for user in body:
      emails = user.pop('emails', None)
      devices = user.pop('devices', None)
      user = EmployeesModel.create(connection.cursor(dictionary=True), **user, **kwargs)
      allUsers.append(user)
      if emails:
        EmployeeEmailsModel.create(connection.cursor(), user, emails=emails)
      if devices:
        EmployeeDevicesModel.create(connection.cursor(), user, tokens=devices)

    connection.commit()
    return {"id": allUsers[0]} if isOneUser else allUsers, 200


class Employee(Resource):
  @checkAdminToken
  def get(self, connection, user, token, **kwargs):
    result = jl(jd(EmployeesModel.read(connection.cursor(dictionary=True), user, **kwargs)))
    if len(result):
      return result[0], 200

    return {}, 404


  @checkAdminToken
  def put(self, connection, user, token, employee=None, **kwargs):
    body = request.get_json()
    emails = body.pop('emails', None)
    devices = body.pop('devices', None)
    cursor = connection.cursor()
    rowcount = EmployeesModel.edit(cursor, employee, **body)
    if emails is not None:
      EmployeeEmailsModel.delete(cursor, user=employee)
      EmployeeEmailsModel.create(cursor, user, emails=emails)
    if devices is not None:
      EmployeeDevicesModel.delete(cursor, user=employee)
      EmployeeDevicesModel.create(cursor, employee, tokens=devices)

    connection.commit()
    return {'rowcount': rowcount}, 200


  @checkAdminToken
  def delete(self, connection, user, token, employee=None, **kwargs):
    cursor = connection.cursor()
    EmployeeDevicesModel.delete(cursor, user=employee)
    EmployeeEmailsModel.delete(cursor, user=employee)
    rowcount = EmployeesModel.delete(cursor, user=employee)
    connection.commit()
    return {'rowcount': rowcount}, 200
