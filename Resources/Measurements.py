from flask import request, json, jsonify
from flask_restful import Resource
from casper_db import Database, datetime2str
from admin_controller import AdminController
from Resources.utils import checkAdminToken, checkIsRoot, checkUserMeasurementPermission
import Models.Schedulers as Schedulers
import Models.SchedulerQuestions as SchedulerQuestions
import Models.SchedulerEmails as SchedulerEmails
import Models.Measurements as MeasurementsModel
import Models.Employees as EmployeesModel
import Models.EmployeeDevices as EmployeeDevicesModel
import Models.EmployeeEmails as EmployeeEmailsModel
import Models.EmployeeControls as EmployeeControls
import Models.EmployeeQuestions as EmployeeQuestions
import Models.EmployeeNotifications as EmployeeNotifications
import Models.ApplicationNotifications as ApplicationNotifications
import Models.Surveys as SurveyModel

# next imports are requred for manual scheduling only
import random
from datetime import datetime
from datetime import timedelta
from configuration import glob as settings
from survey_controller import SurveyController
from notification_controller import NotificationController
from notification_email import sendEmailsTemplate

jl = json.loads
jd = json.dumps

class Measurements(Resource):
  @checkAdminToken
  @checkIsRoot
  def get(self, connection, isRoot, user, token, **kwargs):
    cursor = connection.cursor(dictionary=True)
    result = datetime2str(MeasurementsModel.read(cursor) if isRoot else MeasurementsModel.read(cursor, user=user))
    return result, 200

  @checkAdminToken
  @checkIsRoot
  def post(self, connection, isRoot, user, token, **kwargs):
    body = request.get_json()
    cursor = connection.cursor()
    result = MeasurementsModel.create(cursor, **body) if isRoot else MeasurementsModel.create(cursor, user=user, **body)
    connection.commit()    
    return {"id": result}, 200


class Measurement(Resource):
  @checkAdminToken
  @checkUserMeasurementPermission
  def get(self, connection, user, token, measurement, **kwargs):
    result = datetime2str(MeasurementsModel.read(connection.cursor(dictionary=True), measurement, **kwargs))
    if len(result):
      return result[0], 200

    return {}, 404

  @checkAdminToken
  @checkUserMeasurementPermission
  def put(self, connection, user, token, measurement, **kwargs):
    body = request.get_json()
    rowcount = MeasurementsModel.edit(connection.cursor(), measurement, **body)
    connection.commit()
    return {'rowcount': rowcount}, 200

  @checkAdminToken
  @checkUserMeasurementPermission
  def delete(self, connection, user, token, measurement):
    cursor = connection.cursor()
    EmployeeControls.delete(cursor, "numberinput", measurement=measurement)
    EmployeeControls.delete(cursor, "textinput", measurement=measurement)
    EmployeeQuestions.delete(cursor, measurement=measurement)
    SchedulerQuestions.delete(cursor, measurement=measurement)
    EmployeeNotifications.delete(cursor, measurement=measurement)
    SurveyModel.delete(cursor, measurement=measurement)
    SchedulerEmails.delete(cursor, measurement=measurement)
    Schedulers.delete(cursor, measurement=measurement)
    ApplicationNotifications.delete(cursor, measurement=measurement)
    EmployeeDevicesModel.delete(cursor, measurement=measurement)
    EmployeeEmailsModel.delete(cursor, measurement=measurement)
    EmployeesModel.delete(cursor, measurement=measurement)
    rowcount = MeasurementsModel.delete(cursor, measurement=measurement)
    connection.commit()
    return {'rowcount': rowcount}, 200


class MeasurementAction(Resource):
  @checkAdminToken
  @checkUserMeasurementPermission
  def post(self, connection, user, token, measurement, action):
    return self._start(connection, measurement) if action == 'start' \
      else self._stop(connection, measurement) if action == 'stop' \
      else self._schedule(connection, measurement) if action == 'schedule' \
      else ({'error': 'undefined action'}, 404)

  def _start(self, connection, measurement):
    cursor = connection.cursor(dictionary=True)
    MeasurementsModel.edit(cursor, measurement, started=1)
    connection.commit()
    return datetime2str(MeasurementsModel.read(cursor, measurement)[0])

  def _stop(self, connection, measurement):
    cursor = connection.cursor(dictionary=True)
    MeasurementsModel.edit(cursor, measurement, started=0)
    connection.commit()
    return datetime2str(MeasurementsModel.read(cursor, measurement)[0])

  # TODO: combine this code with schedule-notification.py script
  def _schedule(self, connection, measurement):
    cursor = connection.cursor(dictionary=True)
    body = request.get_json()
    schedulerId = body["scheduler"]

    cursor.execute("select * from schedulers where id = %s", (schedulerId,))
    schedulers = cursor.fetchall()
    assert len(schedulers) < 2
    if not schedulers or len(schedulers) < 1:
      return {'error': 'invalid scheduler id'}, 500

    scheduler = schedulers[0]

    cursor.execute("""
      select * from users where measurement = %s
    """, (measurement,))

    users = cursor.fetchall()
    client = settings.client()

    surveyExpiration = scheduler["survey_expiration"]
    loader = scheduler["loader"]

    cursor.execute("""
      select title, body, type
        from scheduler_emails
  left join emails_templates
          on scheduler_emails.email = emails_templates.id
      where scheduler_emails.scheduler=%s
    """, (schedulerId,))

    emailsTemplates = cursor.fetchall()

    notificationController = NotificationController(connection)
    surveyController = SurveyController()

    notifications = []
    for user in users:
      userId = user['id']
      userToken = user['token']

      cursor.execute("""select * from useremails where user=%s""", (userId,))
      emails = list(map(lambda x: x['email'], cursor.fetchall()))

      surveyId = surveyController.createSurvey(
        userId,
        measurement,
        schedulerId,
        connection,
        datetime.now() + timedelta(minutes=surveyExpiration)
      )

      link = notificationController.createSurveyLink(client, userId, surveyId, loader, userToken)

      emailTemplate = random.choice(emailsTemplates)

      sendEmailsTemplate(
        settings.smtpHost(),
        settings.smtpPort(),
        settings.smtpUser(),
        settings.smtpPassword(),
        emails, emailTemplate,
        link, loader
      )

      cursor.execute("""
        INSERT INTO
          usernotifications(measurement, user, survey, indent, date, body)
        VALUES (%s, %s, %s, %s, NOW(), NULL);
      """, (measurement, userId, surveyId, schedulerId))

      notifications.append(cursor.lastrowid)
    
    connection.commit()
    return notifications
