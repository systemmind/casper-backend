from flask import request, jsonify, json
from flask_restful import Resource
from Resources.utils import checkEmployeeRequestPreconditions, parseGetFilters
import Models.EmployeeControls as Controls

jd = json.dumps
jl = json.loads

class EmployeeControls(Resource):
  @checkEmployeeRequestPreconditions
  @parseGetFilters
  def get(self, connection, controlType, filters=None, op=None, **kwargs):
    result = jl(jd(Controls.read(connection.cursor(dictionary=True), controlType, filters=filters, operator=op, **kwargs)))
    return result, 200

  @checkEmployeeRequestPreconditions
  def post(self, connection, controlType, question, **kwargs):
    body = request.get_json() or {}
    rowid = Controls.create(connection.cursor(), controlType, question=question, **body)
    connection.commit()
    return {'id': rowid}, 200


class EmployeeControlsExt(EmployeeControls):
  pass


class EmployeeControl(Resource):
  @checkEmployeeRequestPreconditions
  def get(self, connection, control, controlType, **kwargs):
    result = jl(jd(Controls.read(connection.cursor(dictionary=True), controlType, control=control, **kwargs)))
    return (dict(result[0]), 200) if len(result) > 0 else ({}, 404)

  @checkEmployeeRequestPreconditions
  def put(self, connection, controlType, control, **kwargs):
    body = request.get_json() or {}
    rowcount = Controls.edit(connection.cursor(), controlType, control, **body)
    connection.commit()
    return {'rowcount': rowcount}, 200

  @checkEmployeeRequestPreconditions
  def delete(self, connection, controlType, control, **kwargs):
    rowcount = Controls.delete(connection.cursor(), controlType, control)
    connection.commit()
    return {'rowcount': rowcount}, 200

class EmployeeControlExt(EmployeeControl):
  pass
