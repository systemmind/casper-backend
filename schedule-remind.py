#!/usr/bin/python3
import sys
import math
import argparse
import json
import random
from datetime import datetime
from datetime import timedelta
from configuration import glob as settings
from casper_db import Database, NotFoundError
from survey_controller import SurveyController
from question_table_controller import QuestionTableController
from notification_controller import NotificationController
from question_controller import QuestionController
from survey_controller import SurveyController
from notification_controller import NotificationController
from notification_email import sendEmail


connection = Database().getConnection()
client = settings.client()

notificationController = NotificationController(connection)
surveyController = SurveyController()

usernotificationsset = []

cursor = connection.cursor(dictionary=True)
cursor.execute("""
  select schedulers.*
    from schedulers
left join measurements on schedulers.measurement = measurements.id
   where schedulers.begin <= now()
     and schedulers.end > now()
     and measurements.started = 1
     and measurements.stop_date >= now();
""")

schedulers = cursor.fetchall()
#cursor.close()

for scheduler in schedulers:
  measurement = scheduler["measurement"]
  indent = scheduler["id"]
  end = scheduler["end"]
  remind = scheduler["remind"]
  loader = scheduler["loader"]
  #cursor = connection.cursor(dictionary=True)

  #print(scheduler)
  # select notifications only current scheduler
  selectSchedulerNotifications = """
    select * from usernotifications where indent = %s
  """

  # select latest notifications of the current scheduler
  selectLatestNotifications = f"""
    select survey, max(date) as date
      from ({selectSchedulerNotifications}) as notifs
  group by survey
  """

  # select surveys required to remind
  selectSchedulerNotifications = f"""
    select usernotifications.survey
      from usernotifications
inner join ({selectLatestNotifications}) as lastnotif
        on usernotifications.survey = lastnotif.survey
       and usernotifications.date = lastnotif.date
     where now() > date_add(usernotifications.date, interval %s minute)
  """

  # filter expired surveys and add questions
  selectRemindSurveys = f"""
    select survey.id, surveyquestions.question
      from survey
inner join ({selectSchedulerNotifications}) as remindSurveys
        on survey.id = remindSurveys.survey
 left join surveyquestions
        on survey.id = surveyquestions.survey
     where survey.end > now()
  """

  cursor.execute(selectRemindSurveys, (indent, remind))

  result = cursor.fetchall()
  cursor.nextset()
  #print(result)
  if not result:
    continue

  #print("get completed survey query")
  completedSurvey = surveyController.getCompletedSurveysQuery(result, cursor)
  #print(completedSurvey)

  selectRemindSurveysFull = f"""
    select survey.* from survey
inner join ({selectSchedulerNotifications}) as remindSurveys
        on survey.id = remindSurveys.survey
     where survey.end > now()
  """

  selectDevices = f"""
    select surveys.id as survey, surveys.end, users.id as user, users.token, usersdevices.token as device
      from ({selectRemindSurveysFull}) as surveys
 left join ({completedSurvey}) as completedSurveys
        on surveys.id = completedSurveys.id
inner join users
        on surveys.user = users.id
 left join usersdevices
        on surveys.user = usersdevices.user
     where completedSurveys.id is null;
  """

  # print(selectDevices)
  cursor.execute(selectDevices, (indent, remind))

  result = cursor.fetchall()
  cursor.nextset()
  print(f"devices count: {len(result)}")

  for item in result:
    survey = item['survey']
    user   = item['user']
    token  = item['token']
    device = item['device']
    end = item['end']

    link = notificationController.createSurveyLink(client, user, survey, loader, token)
    notificationController.createStaticNotification(
      user,
      link,
      loader,
      datetime.now(),
      datetime.now() + (end - datetime.now())
    )

    notificationController.sendSurveyXmppNotification(user, link, loader)
    if device:
      msg = "You have an incomplete survey"
      notificationController.sendSurveyPushNotificationMsg(device, link, msg)

    usernotificationsset.append((measurement, user, survey, indent))

  selectEmails = f"""
    select surveys.id as survey, users.id as user, users.token, useremails.email
      from ({selectRemindSurveysFull}) as surveys
 left join ({completedSurvey}) as completedSurveys
        on surveys.id = completedSurveys.id
inner join users
        on surveys.user = users.id
inner join useremails
        on surveys.user = useremails.user
     where useremails.email is not null
       and completedSurveys.id is null;
  """
  #print(selectEmails)
  cursor.execute(selectEmails, (indent, remind))
  result = cursor.fetchall()

  #print(f"emails: {result}")
  print(f"email count: {len(result)}")
  for item in result:
    survey = item['survey']
    email = item['email']
    token = item['token']
    link = notificationController.createSurveyLink(client, user, survey, loader, token)
    sendEmail(
      settings.smtpHost(),
      settings.smtpPort(),
      settings.smtpUser(),
      settings.smtpPassword(),
      email, link, "You have an incomplete survey", loader,
      f"Oops! You almost missed your <a href='{link}'>diary</a> entry."
    )

print(f"insert to user notifications: {usernotificationsset}")
cursor.executemany("""
  INSERT INTO
    usernotifications(measurement, user, survey, indent, date, body)
  VALUES (%s, %s, %s, %s, NOW(), NULL);
""", usernotificationsset)

connection.commit()
