from flask import request, jsonify
from casper_db import Database
from casper_logger import Logger as logger
from xmppcontroller import XmppControllerXmlrpc
from configuration import glob as settings


def createXmppUser(measurement, user):
  try:
    token = request.headers.get('token-Authorization')
    if not verifyUserToken(user, token):
      return ('access denied', 401)
    
    ctrl = XmppControllerXmlrpc()
    password = token[:6]
    if not ctrl.createUser(user, password):
      raise Exception("failed to create XMPP user")

    if not ctrl.addToRoster(user, 'casper'):
      raise Exception("failed to add casper to XMPP user roster")

    if not ctrl.addToRoster(user, 'support'):
      raise Exception("failed to add support to XMPP user roster")

    result = {
      "server": settings.xmppServer(),
      "user": ctrl.xmppUserName(user),
      "password": password
    }

    return (jsonify(result), 200)

  except Exception as error:
    logger.exception(error)
    return (str(error), 500)
