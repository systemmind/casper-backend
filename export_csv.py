#! /usr/bin/env python

import argparse
import csv
from casper_db import Database

parser = argparse.ArgumentParser()
parser.add_argument("-m", "--measurement", help="measurement ID number", type=int)
parser.add_argument("-d", "--database", help="database name", type=str)
args = parser.parse_args()

db = None

measurement = int(args.measurement)
database = args.database

db = Database()
connection = db.getConnection()
cursor = connection.cursor(dictionary=True)

def export(filename, title, query):
  with open(filename, mode = 'w') as data_file:
    data_writer = csv.DictWriter(data_file, fieldnames=title, delimiter='|', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    data_writer.writeheader()
    cursor.execute(query)
    table = cursor.fetchall()
    for item in table:
      data_writer.writerow(item)

def getTitles(table):
  cursor.execute(f'DESCRIBE {table};')
  return list(map(lambda x: x['Field'], cursor.fetchall()))

cursor.execute(f"USE {database};")

surveyTable = 'survey'
filename = f'measurement{measurement}_{surveyTable}.csv'
query = f'SELECT * FROM {surveyTable} WHERE measurement = {measurement};'
export(filename=filename, title=getTitles(surveyTable), query=query)

cursor.execute(f"""SELECT TABLE_NAME FROM information_schema.tables
                   WHERE table_schema = '{database}' AND  TABLE_NAME LIKE 'question__%'""")
tables = cursor.fetchall()
for item in tables:
  table = item["TABLE_NAME"]
  filename = f'measurement{measurement}_{table}.csv'
  query = f"""
    SELECT {table}.*
      FROM {table}
 LEFT JOIN {surveyTable}
        ON {table}.survey = {surveyTable}.id
     WHERE {surveyTable}.measurement = {measurement};
  """

  export(filename=filename, title=getTitles(table), query=query)
