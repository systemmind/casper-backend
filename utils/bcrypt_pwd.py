from bcrypt import hashpw, gensalt, checkpw

plaintext_password = input("Enter your password:\n").encode("utf-8")
rounds_number = input("Enter the rounds number (12 by default):\n")
hashed = hashpw(plaintext_password, gensalt(rounds = int(rounds_number)))
print(f"Your bcrypt-hashed password is:\n{str(hashed)}")
password_attempt = input("Enter your password again:\n").encode("utf-8")
check = str("" if checkpw(password_attempt, hashed) else "do not ".upper())
print(f"Passwords {check} match")
