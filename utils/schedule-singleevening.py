#!/usr/bin/python3
import sys
import argparse
from datetime import datetime
from datetime import timedelta
sys.path.append('../.')
from configuration import glob as settings
from casper_db import Database, NotFoundError
from survey_controller import SurveyController
from question_table_controller import QuestionTableController
from notification_controller import NotificationController
from question_controller import QuestionController

parser = argparse.ArgumentParser()
parser.add_argument("-m", "--measurement", help="measurement")
parser.add_argument("-u", "--user", help="user")

args = parser.parse_args()

if args.measurement is None:
  print("--measurement argument requred")
  sys.exit(-1)

user = None
if args.user is not None:
  user = int(args.user)

if args.measurement != 'active':
  measurement = int(args.measurement)
else:
  measurement = args.measurement

connection = Database().getConnection()
cursor = connection.cursor(dictionary=True)
client = settings.client()

loader = "singleevening"
expire = 240

# TODO: add order
questions = [
  "activity",
  "duration",
  "feeling",
  "reason",
  "agree",
  "todo",
  "activity",
  "duration",
  "feeling",
  "reason",
  "agree",
  "todo",
  "activity",
  "duration",
  "feeling",
  "reason",
  "agree",
  "todo"
]

questionController = QuestionController()

questionTablesController = QuestionTableController()
targetQuestionTables = questionTablesController.listByType(connection, 'target')
questions.extend(list(map(lambda element: element['name'], targetQuestionTables)))
questions.append(questionController.getQuestionEmotionName())

def collectAuthUsers(measurementids, cursor):
  tmp = map(lambda x: f"measurement = {x}", measurementids)
  where = " OR ".join(tmp)
  query = f"SELECT * FROM users WHERE {where} AND authorized > 0;"
  print("Query: " + query)
  cursor.execute(query)
  return cursor.fetchall()

notificationController = NotificationController(connection)

where = "" if measurement == 'active' else f"AND measurements.id = {measurement}"
cursor.execute(
  f"SELECT users.id, users.measurement, users.token, usersdevices.token as device "
  f"FROM users "
  f"INNER JOIN measurements ON measurements.id = users.measurement "
  f"LEFT JOIN usersdevices ON usersdevices.user = users.id "
  f"WHERE users.authorized > 0 AND measurements.started > 0 AND measurements.stop_date >= DATE(NOW()) {where};"
)

users = cursor.fetchall()
if user:
  users = filter(lambda x: x["id"] == user, users)

for user in users:
  userId = user['id']
  userToken = user['token']
  devicetoken = user["device"]
  measurement = user["measurement"]
  surveyController = SurveyController()

  surveyId = surveyController.createSurvey(
    userId,
    measurement,
    connection,
    datetime.now() + timedelta(minutes=expire)
  )

  surveyController.addQuestions(surveyId, questions, connection)
  link = notificationController.createSurveyLink(client, surveyId, loader, userToken)
  message = "Hi! Your survey is available."

  notificationController.createStaticNotification(
    userId,
    link,
    loader,
    datetime.now(),
    datetime.now() + timedelta(minutes=expire)
  )

  notificationController.sendSurveyXmppNotificationMsg(userId, link, message)
  if devicetoken:
    notificationController.sendSurveyPushNotification(devicetoken, link, loader)
