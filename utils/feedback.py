#!/usr/bin/python3
import sys
import argparse
from datetime import datetime
from datetime import timedelta
sys.path.append('../.')
# from configuration import Configuration
from configuration import glob as settings
from casper_db import Database, NotFoundError
from feedback_controler import FeedbackController

parser = argparse.ArgumentParser()
parser.add_argument("-u", "--user", help="user")
parser.add_argument("-e", "--emotion", help="emotion")

args = parser.parse_args()

if args.user is None:
  print("--user argument requred")
  sys.exit(-1)

if args.emotion is None:
  print("--emotion argument requred")
  sys.exit(-1)

user = int(args.user)
emotion = args.emotion

feedbackController = FeedbackController()

connection = Database().getConnection()
cursor = connection.cursor(dictionary=True)
client = settings.client()

cursor.execute("""SELECT * FROM users WHERE id = %s AND authorized > 0;""", (user,))
users = cursor.fetchall()
if not len(users):
  raise Exception("no user")

userToken = users[0]['token']

feedbackController.createFeedbackAndNotification(user, userToken, emotion, connection)
