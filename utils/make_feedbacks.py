#!/usr/bin/python3
import sys
import argparse
sys.path.append('../.')
import random
from casper_db import Database, NotFoundError
from notification_controller import NotificationController
from user_controller import UserController
from picture_controller import PicturesController
from tip_controller import TipsController
from fact_controller import FactsController
from emotion_controller import EmotionsController
from feedback_controler import FeedbackController

parser = argparse.ArgumentParser()
parser.add_argument("-u", "--user", help="user")

args = parser.parse_args()

if args.user is None:
  print("--user argument requred")
  sys.exit(-1)

user = int(args.user)

def createFeedbackLink(user, emotion, fact, tip, picture, token, connection):
  feedController = FeedbackController()
  nController = NotificationController(connection)
  feedbackId = feedController.insertFeedback(user, emotion, fact, tip, picture, connection)
  assert(feedbackId is not None)
  return nController.createFeedbackLink(feedbackId["id"], token)

def bindFactsAndTips(facts, tips):
  factsTips = []
  if (len(facts) > len(tips)):
    for i in facts:
      if len(tips) > 1:
        factsTips.append((i, tips.pop(0)))
      else:
        factsTips.append((i, tips[0]))

    return factsTips

  elif (len(tips) > len(facts)):
    for i in tips:
      if len(facts) > 1:
        factsTips.append((facts.pop(0), i))
      else:
        factsTips.append((facts[0], i))

    return factsTips

  else:
    for i in zip(facts, tips):
      factsTips.append((i[0], i[1]))

    return factsTips

def getBindedFactsTipsByEmotion(emotion, connection):
  facts = FactsController().getFactsByEmotion(emotion["id"], connection)
  fIds = list(map(lambda x: x['id'], list(facts)))
  tips = TipsController().getTipsByEmotion(emotion["id"], connection)
  tIds = list(map(lambda x: x['id'], list(tips)))
  return bindFactsAndTips(fIds, tIds)

def createLinks(user, connection):
  links = []
  user = UserController().findUserById(user, connection)
  if not user["authorized"]:
    print("user {} not autorized".format(user["id"]))
    sys.exit(-1)

  emotions = EmotionsController().getEmotions(connection)
  for emotion in emotions:
    try:
      pictures = PicturesController().getPicturesByEmotion(emotion["emotion"])
    except NotFoundError:
      pictures = None

    for i in getBindedFactsTipsByEmotion(emotion, connection):
      if not pictures:
        links.append(createFeedbackLink(user["id"], emotion["id"], i[0], i[1], pictures, user["token"], connection))
      elif len(pictures) > 1:
        links.append(createFeedbackLink(user["id"], emotion["id"], i[0], i[1], pictures.pop(0), user["token"], connection))
      else:
        links.append(createFeedbackLink(user["id"], emotion["id"], i[0], i[1], pictures[0], user["token"], connection))

  with open("feedbacksLinks.txt", 'w') as otput:
    for link in links:
      otput.write("{}\n".format(link))
      print("{}\n".format(link))


connection = Database().getConnection()
createLinks(user, connection)
connection.close()
