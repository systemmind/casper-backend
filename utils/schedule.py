#!/usr/bin/python3
import sys
import argparse
from datetime import datetime
from datetime import timedelta
sys.path.append('../.')
from configuration import glob as settings
from casper_db import Database, NotFoundError
from survey_controller import SurveyController
from question_table_controller import QuestionTableController
from notification_controller import NotificationController
from question_controller import QuestionController

parser = argparse.ArgumentParser()
parser.add_argument("-m", "--measurement", help="measurement")
parser.add_argument("-u", "--user", help="user")
parser.add_argument("-d", "--delay", help="delay of notification starting from now, seconds")
parser.add_argument("-x", "--expired", help="notification expiration time, seconds")
parser.add_argument("-w", "--when", help="morning, afternoon, evening")

args = parser.parse_args()

if args.measurement is None:
  print("--measurement argument requred")
  sys.exit(-1)

user = None
if args.user is not None:
  user = int(args.user)

if args.delay is None:
  print("--delay argument requred")
  sys.exit(-1)

if args.expired is None:
  print("--expired argument requred")
  sys.exit(-1)

if args.when is None:
  print("--when argument requred")
  sys.exit(-1)

if args.when != "morning" and args.when != "afternoon" and args.when != "evening":
  print("--when argument is invalid")
  sys.exit(-1)

if args.measurement != 'active':
  measurement = int(args.measurement)
else:
  measurement = args.measurement

delay = int(args.delay)
when = args.when
expired = int(args.expired)

connection = Database().getConnection()
cursor = connection.cursor(dictionary=True)
client = settings.client()
questions = [
  "activity",
  "duration",
  "feeling",
  "reason",
  "agree",
  "todo"
]

questionController = QuestionController()
if (args.when == "evening"):
    questionTablesController = QuestionTableController()
    targetQuestionTables = questionTablesController.listByType(connection, 'target')
    questions.extend(list(map(lambda element: element['name'], targetQuestionTables)))
    questions.append(questionController.getQuestionEmotionName())

"""
def __scheduleConcreteQuestion(questionName, surveyId, controller):
  funcName = "insert_question_%s" % questionName
  if hasattr(controller, funcName):
    getattr(controller, funcName)(surveyId, connection)
"""

def collectAuthUsers(measurementids, cursor):
  tmp = map(lambda x: f"measurement = {x}", measurementids)
  where = " OR ".join(tmp)
  query = f"SELECT * FROM users WHERE {where} AND authorized > 0;"
  print("Query: " + query)
  cursor.execute(query)
  return cursor.fetchall()

notificationController = NotificationController(connection)
now = datetime.now() + timedelta(seconds=delay)
expired = now + timedelta(seconds=expired)

where = "" if measurement == 'active' else f"AND measurements.id = {measurement}"
cursor.execute(
  f"SELECT users.id, users.measurement, users.token, usersdevices.token as device "
  f"FROM users "
  f"INNER JOIN measurements ON measurements.id = users.measurement "
  f"LEFT JOIN usersdevices ON usersdevices.user = users.id "
  f"WHERE users.authorized > 0 AND measurements.started > 0 AND measurements.stop_date >= DATE(NOW()) {where};"
)

users = cursor.fetchall()

if user:
  users = filter(lambda x: x["id"] == user, users)

for user in users:
  userId = user['id']
  userToken = user['token']
  devicetoken = user["device"]
  measurement = user["measurement"]
  surveyController = SurveyController()
  surveyId = surveyController.createSurvey(userId, measurement, connection)
  surveyController.addQuestions(surveyId, questions, connection)
  #link = notificationController.createSurveyLink(client, surveyId, questions, userToken)
  link = notificationController.createSurveyLink(client, surveyId, when, userToken)
  notificationController.createStaticNotification(userId, link, when, now, expired)

  notificationController.sendSurveyXmppNotification(userId, link, when)
  if devicetoken:
    notificationController.sendSurveyPushNotification(devicetoken, link, when)

  """
  for q in questions:
    __scheduleConcreteQuestion(q, surveyId, questionController)
  """
