import sys
sys.path.append('../.')
import argparse
from casper_db import Database
from question_controller import QuestionController
from datetime import time, datetime
import mysql.connector
import csv
from collections import OrderedDict
import operator

MORNING_BEGIN = time(hour = 8, minute = 0)
MORNING_END = time(hour = MORNING_BEGIN.hour + 2, minute = 0)
AFTERNOON_BEGIN = time(hour = 12, minute = 0)
AFTERNOON_END = time(hour = AFTERNOON_BEGIN.hour + 2, minute = 0)
EVENING_BEGIN = time(hour = 16, minute = 0)
EVENING_END = time(hour = EVENING_BEGIN.hour + 2, minute = 0)
demographic = []

parser = argparse.ArgumentParser()
parser.add_argument("-m", "--measurement", help="measurement")
args = parser.parse_args()


if args.measurement is None:
  print("--measurement argument requred")
  sys.exit(-1)


measurement = int(args.measurement)

def getUsersByMeasurement(measurement, connection):
  cursor = connection.cursor(dictionary=True)
  cursor.execute("SELECT id FROM users WHERE measurement = %s AND authorized > 0;", (measurement, ))
  users = cursor.fetchall()
  ids = map(lambda x: x['id'], list(users))
  return ids

def getAllMeasurements(connection):
  cursor = connection.cursor(dictionary=True)
  cursor.execute("SELECT id FROM measurements WHERE start_date <= CURDATE();")
  measurements = cursor.fetchall()
  ids = map(lambda x: x['id'], list(measurements))
  return ids

def getAllUsers(connection):
  cursor = connection.cursor(dictionary=True)
  cursor.execute("SELECT id FROM users WHERE authorized > 0;")
  users = cursor.fetchall()
  ids = map(lambda x: x['id'], list(users))
  return ids

def getAllSurveysByMeasurement(measurement, connection):
  cursor = connection.cursor(dictionary=True)
  cursor.execute("SELECT id FROM survey WHERE measurement = %s;", (measurement, ))
  surveys = cursor.fetchall()
  ids = map(lambda x: x['id'], list(surveys))
  return ids

def getAllSurveysByMeasurementAndGroupByDate(measurement, connection):
  od = OrderedDict()
  cursor = connection.cursor(dictionary=True)
  cursor.execute("SELECT date, id FROM survey WHERE measurement = %s ORDER BY date;", (measurement, ))
  surveys = cursor.fetchall()
  if not surveys:
    return None

  for survey in surveys:
    if survey['date'] in od.keys():
      od[survey['date']].append(survey['id'])
    else:
      od.update({survey['date']: [survey['id']]})

  return od

def getAllSurveysByUser(user, connection):
  cursor = connection.cursor(dictionary=True)
  cursor.execute("SELECT id FROM survey WHERE user = %s;", (user, ))
  surveys = cursor.fetchall()
  ids = map(lambda x: x['id'], list(surveys))
  return ids

def getUserSurves(user, connection):
  cursor = connection.cursor(dictionary=True)
  cursor.execute("SELECT id FROM survey where user = %s;", (user, ))
  surveys = cursor.fetchall()
  ids = map(lambda x: x['id'], list(surveys))
  return ids

def getAmountUserQuestions(user, connection):
  surveys = getAllSurveysByUser(user, connection)
  summ = 0
  for survey in surveys:
    summ = summ + getAmountQuestionsInSurvey(survey, connection)

  return summ

def getAnsweredUserQuestuions(user, connection):
  surveys = getAllSurveysByUser(user, connection)
  summ = 0
  for survey in surveys:
    summ = summ + getAmountAnsweredInSurvey(survey, connection)

  return summ

def getAmountAnsweredQuestionsByUser(user, connection):
  amountQuestions = getAmountUserQuestions(user, connection)
  amountAnswered = getAnsweredUserQuestuions(user, connection)
  return {"amount": amountQuestions, "answered": amountAnswered}

def getAmountAndAnsweredInMeasurement(measurement, connection):
  amount = 0
  answered = 0
  surveys = getAllSurveysByMeasurement(measurement, connection)
  for survey in surveys:
    if isDemographic(survey):
      continue
    amount = amount + getAmountQuestionsInSurvey(survey, connection)
    answered = answered + getAmountAnsweredInSurvey(survey, connection)

  return {"answered": answered, "amount": amount}

def getDemographic(connection):
  global demographic
  cursor = connection.cursor(dictionary=True)
  cursor.execute("SELECT survey FROM question_gender;")
  genders = cursor.fetchall()
  genders = map(lambda x: x['survey'], list(genders))
  cursor.execute("SELECT survey FROM question_experience;")
  ex = cursor.fetchall()
  ex = map(lambda x: x['survey'], list(ex))
  cursor.execute("SELECT survey FROM question_age;")
  ages = cursor.fetchall()
  ages = map(lambda x: x['survey'], list(ages))

  tmpList = []
  tmpList.extend(genders)
  tmpList.extend(ex)
  tmpList.extend(ages)
  demographic = list(dict.fromkeys(tmpList))



def isDemographic(survey):
  global demographic
  return survey in demographic

def getTimeBoundedSurveysByMeasurement(measurement, begin, end, connection):
  cursor = connection.cursor(dictionary=True)
  cursor.execute("SELECT * FROM survey WHERE measurement = %s AND begin >= %s AND end <= %s;", (measurement, begin, end))
  surveys = cursor.fetchall()
  ids = map(lambda x: x['id'], list(surveys))
  return ids

def getTimeBoundedSurveysByMeasurementAndDate(measurement, date, begin, end, connection):
  cursor = connection.cursor(dictionary=True)
  cursor.execute("SELECT id FROM survey WHERE measurement = %s AND date = %s AND begin >= %s AND end <= %s;", (measurement, date, begin, end))
  surveys = cursor.fetchall()
  ids = []
  for survey in surveys:
    if isDemographic(survey['id']):
      continue

    if begin == EVENING_BEGIN:
      print("this is evening survey {} amount questions = {}".format(survey['id'], getAmountQuestionsInSurvey(survey['id'], connection, True)))
    ids.append(survey['id'])

  return ids

def getAmountQuestionsInSurvey(survey, connection, evening=False):
  cursor = connection.cursor(dictionary=True)
  cursor.execute("SELECT name FROM questions;")
  questions = cursor.fetchall()
  names = map(lambda x: x['name'], list(questions))
  summ = 0
  for name in names:
    query = "SELECT * FROM question_%s WHERE survey = %s;" % (cursor._connection.converter.escape(str(name)), cursor._connection.converter.escape(str(survey)))
    cursor.execute(query)

    if cursor.fetchone():
      summ = summ + 1
      if evening:
        print(name)

  return summ

def getAmountAnsweredInSurvey(survey, connection):
  cursor = connection.cursor(dictionary=True)
  cursor.execute("SELECT name FROM questions;")
  questions = cursor.fetchall()
  names = map(lambda x: x['name'], list(questions))
  summ = 0
  for name in names:
    if QuestionController().questionAnswered(str(survey), name, connection):
      summ = summ + 1

  return summ

def getAnsweredPercent(amount, answered):
  if not amount:
    return 0
  percent = 100.0 / amount * answered
  return round(percent, 2)

def getQuestionsSurveysAmountAnswered(surveys, connection):
  amount = 0
  answered = 0
  for survey in surveys:
    amount = amount + getAmountQuestionsInSurvey(survey, connection)
    answered = answered + getAmountAnsweredInSurvey(survey, connection)

  return {"answered": answered, "amount": amount}

def collectResponseMeasurement(measurement, connection):
  data = getAmountAndAnsweredInMeasurement(measurement, connection)
  with open("measurement_{}_response_rate.csv".format(measurement), mode = 'w') as data_file:
    data_file.write("measurement,percent\n")
    percent = getAnsweredPercent(data["amount"], data["answered"])
    #print("Amount in measurement {} answered {}".format(data['amount'], data['answered']))
    row = "{},{}\n".format(measurement, percent)
    data_file.write(row)


def collectResponseUsers(measurement, connection):
  dct = {}
  title = ("user,percent\n")
  filename = None
  users = getUsersByMeasurement(measurement, connection)
  for user in users:
    amountAnswered = getAmountAnsweredQuestionsByUser(user, connection)
    percent = getAnsweredPercent(amountAnswered['amount'], amountAnswered['answered'])
    dct.update({user: percent})

  srtd = sorted(dct.items(), key=operator.itemgetter(1))
  od = OrderedDict()
  for item in srtd:
    od.update({item[0]: item[1]})


  with open('measurement_{}_users_response.csv'.format(measurement), mode = 'w') as data_file:
    data_file.write(title)
    for key in od.keys():
      row = "{},{}\n".format(key, dct[key])
      data_file.write(row)

def collectResponseBydateAndDaytime(measurement, connection):
  cursor = connection.cursor(dictionary=True)
  cursor.execute("SELECT DISTINCT date FROM survey WHERE measurement = %s ORDER BY date;", (measurement, ))
  dates = cursor.fetchall()
  dates = list(map(lambda x: x['date'], list(dates)))
  questionsMorning = ['questions, morning']
  answersMorning = ['answers, morning']
  questionsAfternoon = ['questions, afternoon']
  answersAfternoon = ['answers, afternoon']
  questionsEvening = ['questions, evening']
  answersEvening = ['answers, evening']
  rows = []
  for date in dates:
    morningSurveys = getTimeBoundedSurveysByMeasurementAndDate(measurement, date, MORNING_BEGIN, MORNING_END, connection)
    afternoonSurveys = getTimeBoundedSurveysByMeasurementAndDate(measurement, date, AFTERNOON_BEGIN, AFTERNOON_END, connection)
    eveningSurveys = getTimeBoundedSurveysByMeasurementAndDate(measurement, date, EVENING_BEGIN, EVENING_END, connection)
    morningSurveys =  getQuestionsSurveysAmountAnswered(morningSurveys, connection)
    afternoonSurveys =  getQuestionsSurveysAmountAnswered(afternoonSurveys, connection)
    eveningSurveys =  getQuestionsSurveysAmountAnswered(eveningSurveys, connection)
    questionsMorning.append(morningSurveys['amount'])
    answersMorning.append(morningSurveys['answered'])
    questionsAfternoon.append(afternoonSurveys['amount'])
    answersAfternoon.append(afternoonSurveys['answered'])
    questionsEvening.append(eveningSurveys['amount'])
    answersEvening.append(eveningSurveys['answered'])

  dates.insert(0, '')
  rows.append(dates)
  rows.append(questionsMorning)
  rows.append(answersMorning)
  rows.append(questionsAfternoon)
  rows.append(answersAfternoon)
  rows.append(questionsEvening)
  rows.append(answersEvening)

  with open("measurement_{}_response_by_date_daytime.csv".format(measurement), mode = 'w') as data_file:
    writer = csv.writer(data_file)
    for row in rows:
      writer.writerow(row)




connection = Database().getConnection()
getDemographic(connection)
collectResponseUsers(measurement, connection)
collectResponseMeasurement(measurement, connection)
collectResponseBydateAndDaytime(measurement, connection)

connection.close()
