from casper_db import Database
from casper_logger import Logger
import mysql.connector
from bcrypt import checkpw
import random
import string
from custom_exceptions import PasswordMismatchError, UserNotFoundError

class AdminController():
  def createAuthToken(self, login, password, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("SELECT hashed_password FROM user WHERE user = %s;", (login,))
    hashed = cursor.fetchone()

    if hashed:
      result = checkpw(password.encode('utf-8'), hashed['hashed_password'].encode('utf-8'))
      if result:
        cursor.execute("SELECT id FROM user WHERE user = %s;", (login,))
        user_id = cursor.fetchone()['id']
        token = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(128))
        cursor.execute("INSERT INTO admin_token(user_id, token, expired) VALUES (%s, %s, DATE_ADD(NOW(), INTERVAL 120 MINUTE));", ( user_id, token ))
        connection.commit()
        return token
      else:
        raise PasswordMismatchError("invalid password")
    else:
      raise UserNotFoundError("user not found")

  def verifyAuthToken(self, token, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("SELECT id FROM admin_token WHERE token = %s AND expired > NOW();", (token,))
    return cursor.fetchone() != None

  def updateAuthToken(self, token, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("UPDATE admin_token SET expired = DATE_ADD(NOW(), INTERVAL 15 MINUTE) WHERE token = %s;", (token,))
    connection.commit()
    cursor.execute("SELECT expired FROM admin_token WHERE token = %s;", (token,))
    return cursor.fetchone()['expired']

  def deleteExpiredTokens(self, connection):
    cursor = connection.cursor(dictionary=True)
    query = "DELETE FROM admin_token WHERE expired < NOW();"
    cursor.execute(query)
    connection.commit()
    query = "SELECT id FROM admin_token WHERE expired > NOW();"
    cursor.execute(query)
    return len(cursor.fetchall())

  def createUser(self, name, passwd, connection):
    cursor = connection.cursor()
    Database().addAdmin(name, passwd, cursor)
    connection.commit()
