from casper_db import Database, NotFoundError
import mysql.connector
from casper_logger import Logger
from Models.utils import buildSqlFilters, matchNumChar


def read(cursor, user, survey, question=None, filters=None, operator=None):
  sqlSelect = f"""
    select employeequestions.*
      from employeequestions
 left join survey
        on employeequestions.survey = survey.id
  """

  sqlWhere = ["survey.user=%s", "survey.id=%s"]
  values = (user, survey)

  if question:
    sqlWhere.append("employeequestions.id=%s")
    values = values + (question,)

  w, v = buildSqlFilters(cursor, filters, operator)
  if w:
    sqlWhere.append(w)
    values = values + v

  sqlQuery = f"""{sqlSelect} where {" and ".join(sqlWhere)}"""
  cursor.execute(sqlQuery, values)
  return cursor.fetchall()


def create(cursor, **kwargs):
  sqlColumns = [matchNumChar(k) for k in kwargs.keys()]
  sqlValues = ["%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())

  cursor.execute(f"""insert into employeequestions({",".join(sqlColumns)}) values({",".join(sqlValues)})""", values)
  return cursor.lastrowid


def edit(cursor, question, answered=None, skipped=None, postponed=None):
  sqlSet = []
  values = ()
  if answered is not None:
    sqlSet.append("answered=%s")
    values = values + (answered,)

  if skipped is not None:
    sqlSet.append("skipped=%s")
    values = values + (skipped,)

  if postponed is not None:
    sqlSet.append("postponed=%s")
    values = values + (postponed,)

  if sqlSet:
    sqlQuery = f"""update employeequestions set {", ".join(sqlSet)} where id=%s"""
    cursor.execute(sqlQuery, values + (question,))
    return cursor.rowcount
  else:
    return -1


def delete(cursor, question=None, scheduler=None, measurement=None, **kwargs):
  '''
  if measurement:
    cursor.execute("""
       delete employeequestions, numberinputs, textinputs
         from employeequestions
    left join numberinputs
           on employeequestions.id=numberinputs.question
    left join textinputs
           on employeequestions.id=textinputs.question
    left join users
           on users.id=employeequestions.id
        where users.measurement=%s;
    """, (measurement,))
    return cursor.rowcount

  sqlWhere = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())

  if question:
    sqlWhere.append("id=%s")
    values = values + (question,)

    cursor.execute(f"""
      delete from employeequestions where {" and ".join(sqlWhere)};
    """, values)
    return cursor.rowcount
  '''

  sqlWhere = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())

  if question:
    sqlWhere.append("id=%s")
    values = values + (question,)

    cursor.execute(f"""
      delete from employeequestions where {" and ".join(sqlWhere)};
    """, values)
    return cursor.rowcount
  else:
    if measurement:
      where = "survey.measurement=%s"
      values = (measurement,)

    if scheduler:
      where = "survey.scheduler=%s"
      values = (scheduler,)

    cursor.execute(f"""
        delete employeequestions
          from employeequestions
    left join survey
            on survey.id=employeequestions.survey
        where {where};
    """, values)
    return cursor.rowcount
