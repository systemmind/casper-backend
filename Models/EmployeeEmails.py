from casper_db import Database
from Models.utils import matchNumChar


def create(cursor, user, email=None, emails=None):
  insert = """
    insert into useremails (user, email)
          values (%s, %s);
  """
  if email:
    cursor.execute(insert, (user, email))
    return cursor.lastrowid
  elif emails:
    cursor.executemany(insert, [(user, email) for email in emails])


def read(cursor, useremail=None, **kwargs):
  sqlWhere = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())
  
  if useremail:
    sqlWhere.append("id=%s")
    values = values + (useremail,)
    
  cursor.execute(f"""
    select * from useremails {('where ' + " and ".join(sqlWhere)) if len(sqlWhere)>0 else ""};
  """, values)
  
  return cursor.fetchall()


def delete(cursor, useremail=None, measurement=None, user=None, **kwargs):
  if measurement: 
    cursor.execute("""
      delete useremails
        from useremails
   left join users
          on useremails.user=users.id
       where users.measurement=%s;          
    """, (measurement,))
    return cursor.rowcount

  if user:
    cursor.execute("delete from useremails where user=%s", (user,))
    return cursor.rowcount
