# This is model of notifications that is shown by Qt app (it is called static notifications in this python app).
# They are deprecated actually, but maybe it still may be used.


def delete(cursor, measurement=None, **kwargs):
  if measurement:
    cursor.execute(f"""
      delete notifications from notifications
   left join users on notifications.user = users.id
       where users.measurement=%s;
    """, (measurement,))
  else:
    sqlWhere = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
    values = tuple(kwargs.values())

    cursor.execute(f"""delete from notifications where {" and ".join(sqlWhere)};""", values)

  return cursor.rowcount
