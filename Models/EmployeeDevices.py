from Models.utils import matchNumChar


def create(cursor, user, token=None, tokens=None):
  insert = """
    insert into usersdevices (user, token)
          values (%s, %s);
  """
  if token:
    cursor.execute(insert, (user,token))
    return cursor.lastrowid
  elif tokens:
    cursor.executemany(insert, [(user, token) for token in tokens])


def read(cursor, userdevice=None, **kwargs):
  sqlWhere = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())
  
  if userdevice:
    sqlWhere.append("id=%s")
    values = values + (userdevice,)
    
  cursor.execute(f"""
    select * from usersdevices {('where ' + " and ".join(sqlWhere)) if len(sqlWhere)>0 else ""};
  """, values)
  
  return cursor.fetchall()


def delete(cursor, user=None, measurement=None, **kwargs):
  if measurement:
    cursor.execute("""
      delete usersdevices
        from usersdevices
   left join users
          on usersdevices.user=users.id
       where users.measurement=%s;
    """, (measurement,))
    return cursor.rowcount

  if user:
    cursor.execute("delete from usersdevices where user=%s", (user,))
    return cursor.rowcount
