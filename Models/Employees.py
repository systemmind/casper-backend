import random
import string
from Models.utils import matchNumChar


def generateUserToken():
  return ''.join(
  random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(32))


def create(cursor, **kwargs):
  token = generateUserToken()
  keys = ",".join([matchNumChar(k) for k in kwargs.keys()])
  values = tuple(kwargs.values())
  params = ",".join(["%s"]*len(values))

  cursor.execute(f"""
    insert into users (token, date, {keys}) values (%s, curdate(), {params});
  """, (token, ) + values)
  return cursor.lastrowid


def read(cursor, user=None, emails=False, devices=False, **kwargs):
  sqlWhere = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())

  if user:
    sqlWhere.append("id=%s")
    values = values + (user,)

  qSql = f"""
    select users.*
    { ", useremails.email as email" if emails else "" }
    { ", usersdevices.token as device" if devices else "" }
    from users
    {
      "left join useremails on users.id = useremails.user" if emails else ""
    }
    {
      "left join usersdevices on users.id = usersdevices.user" if devices else ""
    }
    {('where ' + " and ".join(sqlWhere)) if len(sqlWhere) > 0 else ""}
  """

  cursor.execute(qSql, values)
  return cursor.fetchall()


def edit(cursor, user, **kwargs):
  sqlSet = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())
  cursor.execute(f"""
    update users set {', '.join(sqlSet)} where id=%s;
  """, values + (user,))

  return cursor.rowcount


def delete(cursor, user=None, measurement=None, **kwargs): 
  if user is None and measurement is not None:
    cursor.execute("""
      delete users, survey
        from users 
   left join survey
          on users.id=survey.user
       where users.measurement=%s;          
    """, (measurement,))
    return cursor.rowcount

  sqlWhere = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values()) 

  if user:
    sqlWhere.append("id=%s")
    values = values + (user,)

  cursor.execute(f"""
    delete from users where {" and ".join(sqlWhere)};
  """, values)
  
  return cursor.rowcount
