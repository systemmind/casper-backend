# This is admin users controller
from Models.utils import matchNumChar


def read(cursor, **kwargs):
  sqlWhere = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())
  cursor.execute(f"""
    select id, user from user
    {('where ' + " and ".join(sqlWhere)) if len(sqlWhere) > 0 else ""}
  """, values)

  return cursor.fetchall()


def isRoot(cursor, user):
  cursor.execute("""select * from user where id=%s and (user='admin' or user='root')""", (user,))
  return len(cursor.fetchall()) == 1

def create(cursor, **kwargs):
  keys = ",".join([matchNumChar(k) for k in kwargs.keys()])
  values = tuple(kwargs.values())
  params = ",".join(["%s"]*len(values))
  cursor.execute(f"""insert into user ({keys}) values ({params});""", values)
  return cursor.lastrowid


def delete(cursor, **kwargs):
  sqlWhere = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values()) 

  cursor.execute(f"""delete from user where {" and ".join(sqlWhere)}""", values)

  return cursor.rowcount
