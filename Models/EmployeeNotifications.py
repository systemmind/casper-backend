from casper_db import Database, NotFoundError
import mysql.connector

def delete(cursor, scheduler=None, measurement=None, **kwargs):
  where = ""
  values = ()

  if measurement:
    where = "survey.measurement=%s"
    values = (measurement,)

  if scheduler:
    where = "survey.scheduler=%s"
    values = (scheduler,)

  cursor.execute(f"""
      delete usernotifications
        from usernotifications
   left join survey
          on survey.id=usernotifications.survey
       where {where};
  """, values)

  return cursor.rowcount