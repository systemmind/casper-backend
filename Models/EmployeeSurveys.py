from casper_db import Database, NotFoundError
import mysql.connector
from casper_logger import Logger
from Models.utils import buildSqlFilters

def read(cursor, user=None, survey=None, filters=None, operator=None, template=False):
  sqlSelect = ["survey.*"]
  sqlWhere = []
  sqlJoin = ""
  values = ()

  if user:
    sqlWhere.append("survey.user=%s")
    values = values + (user,)

  if survey:
    sqlWhere.append("survey.id=%s")
    values = values + (survey,)

  w, v = buildSqlFilters(cursor, filters, operator)
  if w:
    sqlWhere.append(w)
    values = values + v

  if template:
    sqlSelect.append("schedulers.template")
    sqlJoin = "left join schedulers on survey.scheduler = schedulers.id"

  sqlQuery = f"""
    select {",".join(sqlSelect)}
      from survey
      {sqlJoin}
     where {" and ".join(sqlWhere)}
  """

  cursor.execute(sqlQuery, values)
  return cursor.fetchall()
