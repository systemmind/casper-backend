import string
import random
from bcrypt import checkpw, hashpw, gensalt
from custom_exceptions import PasswordMismatchError, UserNotFoundError

def checkPassword(cursor, login, password):
  cursor.execute("select id, hashed_password from user where user = %s;", (login,))
  record = cursor.fetchone()

  if record:
    if checkpw(password.encode('utf-8'), record['hashed_password'].encode('utf-8')):
      return record['id']
    else:
      raise PasswordMismatchError("invalid password")
  else:
    raise UserNotFoundError("user not found")


def editPassword(cursor, user, password):
  cursor.execute("update user set hashed_password = %s where id = %s", (hashpw(password.encode('utf-8'), gensalt()), user))
  return cursor.rowcount


def createToken(cursor, user):
  token = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(128))
  cursor.execute("insert into admin_token(user_id, token, expired) values (%s, %s, date_add(now(), interval 120 minute));", ( user, token ))
  return token


def verifyToken(cursor, user, token):
  cursor.execute("select id from admin_token where token = %s and expired > now() and user_id = %s;", (token, user))
  result = cursor.fetchall()
  return len(result) == 1


def verifyRootToken(cursor, token):
  cursor.execute(f"""
    select user.id from admin_token
 left join user on admin_token.user_id = user.id
     where admin_token.token = %s and admin_token.expired > now() and (user.user="admin" or user.user="root");
  """, (token,))
  result = cursor.fetchall()
  return len(result) == 1


def updateToken(cursor, user, token):
  cursor.execute("update admin_token set expired = date_add(now(), interval 15 minute) where token = %s and user = %s;", (token, user))
  return cursor.rowcount
