from casper_db import Database, NotFoundError
import mysql.connector
from casper_logger import Logger
from Models.utils import matchNumChar


def read(cursor, scheduler=None, **kwargs):
  sqlWhere = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())

  if scheduler:
    sqlWhere.append("id=%s")
    values = values + (scheduler,)

  cursor.execute(f"""select * from schedulers where {" and ".join(sqlWhere)}""", values)
  return cursor.fetchall()


def create(cursor, **kwargs):
  keys = ",".join([matchNumChar(k) for k in kwargs.keys()])
  values = tuple(kwargs.values())
  params = ",".join(["%s"]*len(values))

  cursor.execute(f"""insert into schedulers ({keys}) values ({params});""", values)
  return cursor.lastrowid


def edit(cursor, scheduler, **kwargs):
  sqlSet = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())
  cursor.execute(f"""update schedulers set {",".join(sqlSet)} where id=%s;""", values + (scheduler,))
  return cursor.rowcount


# Function should process both sheduler and measurement argument
def delete(cursor, scheduler=None, **kwargs):
  sqlWhere = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())
  if scheduler:
    sqlWhere.append("id=%s")
    values = values + (scheduler,)

  cursor.execute(f"""delete from schedulers where {" and ".join(sqlWhere)};""", values)
  return cursor.rowcount
