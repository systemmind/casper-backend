from Models.utils import matchNumChar


def create(cursor, **kwargs):
  keys = ",".join([matchNumChar(k) for k in kwargs.keys()])     # wrap cursor._connection.converter.escape() ?
  values = tuple(kwargs.values())
  params = ",".join(["%s"]*len(values))

  cursor.execute(f"""
    insert into survey ({keys}) values ({params});
  """, values)
  return cursor.lastrowid


def delete(cursor, survey=None, measurement=None, scheduler=None, **kwargs):
  '''
  if measurement:
    cursor.execute("""
      delete survey
        from survey
   left join users
          on users.id=survey.user
       where users.measurement=%s;
    """, (measurement,))
    return cursor.rowcount

  sqlWhere = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values()) 

  if survey:
    sqlWhere.append("id=%s")
    values = values + (survey,)
  
    cursor.execute(f"""
        delete from survey where {" and ".join(sqlWhere)};
    """, values)
    return cursor.rowcount
  '''

  sqlWhere = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values()) 

  if survey:
    sqlWhere.append("id=%s")
    values = values + (survey,)

  if measurement:
    sqlWhere.append("measurement=%s")
    values = values + (measurement,)

  if scheduler:
    sqlWhere.append("scheduler=%s")
    values = values + (scheduler,)

  cursor.execute(f"""delete from survey where {" and ".join(sqlWhere)};""", values)
  return cursor.rowcount
