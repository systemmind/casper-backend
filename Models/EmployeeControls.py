from Models.utils import buildSqlFilters, matchNumChar
from datetime import datetime

def read(cursor, controlType, user, survey=None, question=None, control=None, filters=None, operator=None):
  controlType = matchNumChar(controlType)

  sqlSelect = f"""
    select {controlType}s.*, employeequestions.ordern as `question.order`
      from {controlType}s
 left join employeequestions
        on {controlType}s.question = employeequestions.id
 left join survey
        on employeequestions.survey = survey.id
  """

  sqlWhere = ["survey.user=%s"]
  values = (user,)

  if survey:
    sqlWhere.append("survey.id=%s")
    values = values + (int(survey),)

  if question:
    sqlWhere.append("employeequestions.id=%s")
    values = values + (int(question),)

  if control:
    sqlWhere.append(f"{controlType}s.id=%s")
    values = values + (int(control),)

  w, v = buildSqlFilters(cursor, filters, operator)
  if w:
    sqlWhere.append(w)
    values = values + v

  sqlQuery = f"""{sqlSelect} where {" and ".join(sqlWhere)}"""
  cursor.execute(sqlQuery, values)
  return cursor.fetchall()


def create(cursor, controlType, **kwargs):
  kwargs['timestamp'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
  controlType = matchNumChar(controlType)
  keys = [matchNumChar(k) for k in kwargs.keys()]
  sqlQuery = f"""insert into {controlType}s({",".join(keys)}) values({",".join("%s" for k in keys)})"""
  cursor.execute(sqlQuery, tuple(kwargs.values()))
  return cursor.lastrowid


def edit(cursor, controlType, control, **kwargs):
  controlType = matchNumChar(controlType)
  sqlSet = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())

  if sqlSet:
    sqlQuery = f"""update {controlType}s set {", ".join(sqlSet)}, timestamp=now() where id=%s"""
    cursor.execute(sqlQuery, values + (int(control),))
    return cursor.rowcount
  else:
    return -1


def delete(cursor, controlType, control=None, scheduler=None, measurement=None):
  controlName = f"{controlType}s"
  if control:
    controlType = matchNumChar(controlType)
    cursor.execute(f"delete from {controlName} where id=%s", (int(control),))
  else:
    where = ""
    values = ()

    if scheduler:
      where = "survey.scheduler = %s"
      values = (scheduler,)

    if measurement:
      where = "survey.measurement = %s"
      values = (measurement,)

    cursor.execute(f"""
         delete {controlName} from {controlName}
      left join employeequestions on {controlName}.question = employeequestions.id
      left join survey on employeequestions.survey = survey.id
          where {where}
    """, values)

  return cursor.rowcount
