from casper_db import Database, NotFoundError
import mysql.connector
from casper_logger import Logger


def read(cursor, scheduler):
  cursor.execute("""select * from scheduler_questions where scheduler=%s""", (scheduler,))
  return cursor.fetchall()


def create(cursor, scheduler, question=None, questions=None):
  if questions:
    items = list(map(lambda x: (scheduler, x), questions))
    cursor.executemany("insert into scheduler_questions (scheduler, question) values (%s,%s)", items)
  else:
    cursor.execute("insert into scheduler_questions (scheduler, question) values (%s,%s)", (scheduler, question))
    return cursor.lastrowid


def delete(cursor, scheduler=None, measurement=None):
  if measurement:
    cursor.execute("""
      delete scheduler_questions
        from scheduler_questions
  left join schedulers
          on scheduler_questions.scheduler=schedulers.id
      where schedulers.measurement=%s;
    """, (measurement,))
    cursor.rowcount

  if scheduler:
    cursor.execute("delete from scheduler_questions where scheduler=%s", (scheduler,))
    return cursor.rowcount


def edit(cursor, scheduler, questions):
  delete(cursor, scheduler)
  create(cursor, scheduler, questions=questions)
