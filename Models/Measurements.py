from casper_db import Database, NotFoundError
import mysql.connector
from casper_logger import Logger
from Models.utils import matchNumChar

def isUserValid(cursor, measurement, user):
  cursor.execute(f"""select * from measurements where id=%s and user=%s;""", (measurement, user))
  return len(cursor.fetchall()) == 1

def create(cursor, **kwargs):
  keys = ",".join([matchNumChar(k) for k in kwargs.keys()])
  values = tuple(kwargs.values())
  params = ",".join(["%s"]*len(values))
  cursor.execute(f"""
    insert into measurements ({keys}) values ({params});
  """, values)
  
  return cursor.lastrowid


def edit(cursor, measurement, **kwargs):
  sqlSet = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())
  cursor.execute(f"""
    update measurements set {', '.join(sqlSet)} where id=%s;
  """, values + (measurement,))
  
  return cursor.rowcount


def read(cursor, measurement=None, **kwargs):
  sqlWhere = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())

  if measurement:
    sqlWhere.append("id=%s")
    values = values + (measurement,)
  
  cursor.execute(f"""
    select * from measurements {('where ' + " and ".join(sqlWhere)) if len(sqlWhere)>0 else ""};
  """, values)
  
  return cursor.fetchall()


def delete(cursor, measurement=None, **kwargs):
  sqlWhere = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())
  
  if measurement:
    sqlWhere.append("id=%s")
    values = values + (measurement,)
  
  cursor.execute(f"""
    delete from measurements where {" and ".join(sqlWhere)};
  """, values)
  
  return cursor.rowcount
