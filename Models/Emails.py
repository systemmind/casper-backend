from casper_db import Database, NotFoundError
import mysql.connector
from casper_logger import Logger
from Models.utils import matchNumChar

def isUserValid(cursor, email, user):
  cursor.execute(f"""select * from emails_templates where id=%s and user=%s;""", (email, user))
  return len(cursor.fetchall()) == 1


def read(cursor, **kwargs):
  sqlWhere = [f"{matchNumChar(key)}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())

  cursor.execute(f"""select * from emails_templates {('where ' + " and ".join(sqlWhere)) if len(sqlWhere)>0 else ""}""", values)
  return cursor.fetchall()


def create(cursor, **kwargs):
  keys = ",".join([matchNumChar(k) for k in kwargs.keys()])
  values = tuple(kwargs.values())
  params = ",".join(["%s"]*len(values))
  cursor.execute(f"""insert into emails_templates ({keys}) values ({params});""", values)
  return cursor.lastrowid


def edit(cursor, id, **kwargs):
  params = ",".join([f"{matchNumChar(key)}=%s" for key in kwargs.keys()])
  values = tuple(kwargs.values())
  cursor.execute(f"""update emails_templates set {params} where id=%s;""", values + (id,))


def delete(cursor, id):
  cursor.execute("""delete from emails_templates where id=%s""", (id,))

