from casper_db import Database, NotFoundError
import mysql.connector
from casper_logger import Logger
from Models.utils import matchNumChar

def read(cursor, **kwargs):
  where = " AND ".join([f"{matchNumChar(key)}=%s" for key in kwargs.keys()])
  values = tuple(kwargs.values())

  where = f"where {where}" if len(where) else ""
  cursor.execute(f"""select * from scheduler_emails {where}""", values)
  return cursor.fetchall()

def create(cursor, scheduler, email):
  cursor.execute("insert into scheduler_emails(scheduler, email) values(%s, %s)", (scheduler, email))
  return cursor.lastrowid

def delete(cursor, measurement=None, **kwargs):
  if measurement:
    cursor.execute("""
      delete scheduler_emails
        from scheduler_emails
  left join schedulers
          on scheduler_emails.scheduler=schedulers.id
      where schedulers.measurement=%s;
    """, (measurement,))
    return cursor.rowcount

  where = " AND ".join([f"{matchNumChar(key)}=%s" for key in kwargs.keys()])
  values = tuple(kwargs.values())

  where = f"where {where}" if len(where) else ""
  cursor.execute(f"""delete from scheduler_emails {where}""", (values))
  return cursor.rowcount

'''
def deleteByMeasurement(cursor, measurement):
  cursor.execute("""
    delete scheduler_emails
      from scheduler_emails
 left join schedulers
        on scheduler_emails.scheduler=schedulers.id
     where schedulers.measurement=%s;
  """, (measurement,))
'''