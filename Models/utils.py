import re

def match(value, exp):
  if value:
    pattern = re.compile(exp)
    if not pattern.match(value):
      raise f"invalid value"

  return value

def matchSqlOperator(value):
  return match(value, "^([<=>]{1,3}|!=|in|is|(is\\snot)|like|less|and|or)$")

def matchNumChar(value):
  return match(value, '^[0-9|A-z|\\.]+$')

def buildReadSqlFilter(cursor, name, op, value, field=None):
  name  = matchNumChar(name)
  op    = matchSqlOperator(op)
  field = matchNumChar(field) if field else None
  return f"{name} {op} %s", (value,)

def buildSqlFilters(cursor, filters, operator):
  tmpWhere = []
  tmpValues = ()

  operator = matchSqlOperator(operator)
  if filters:
    for obj in filters:
      w, v = buildReadSqlFilter(cursor, **obj)
      tmpWhere.append(w)
      tmpValues = tmpValues + v

  op = f" {operator} " if operator else " and "
  return f"({op.join(tmpWhere)})" if len(tmpWhere) > 0 else "", tmpValues

'''
def buildFiltersConditions(cursor, filters):
  condits = []
  values = ()
  if 'filters' in filters:
    for filt in filters['filters']:
      w, v = buildFilter(cursor, **filt)
      condits.append(w)
      values = values + v

  return condits, values


def buildKwargsConditions(**kwargs):
  condits = [f"{key}=%s" for key in kwargs.keys()]
  values = tuple(kwargs.values())
  return condits, values


def parseReadFilters(func):
  def decorator(cursor, filters, op, **kwargs):
    c1, v1 = buildFiltersConditions(cursor, filters)
    c2, v2 = buildKwargsConditions(**kwargs)
    return func(cursor, f" {op} ".join(c1 + c2), *(v1+v2))

  return decorator
'''

'''
def prepareEmployeeSqlreadQueries(func):
  def decorator(cursor, filters, op, user, survey=None, question=None, control=None, controlType=None, **kwargs):
    sqlSelect = ""
    return func(...)
  
  return decorator
'''
