
import signal
import os
from casper_db import Database
from api import app
from api import shutdown
from pool_schedulers import PoolSchedulers
from casper_logger import Logger
# from OpenSSL import SSL
from flask.cli import FlaskGroup
from configuration import glob as settings
import traceback

# db = None
db = Database()

def sigHandle(signum, frame):
  if db:
    db.close()

  shutdown()
  quit()

signal.signal(signal.SIGINT, sigHandle)

# try:
# context = ('cert/server.crt', 'cert/server.key')

db.connect()
db.init()
  # PoolSchedulers().restore(db)
  # app.run(use_reloader=False, ssl_context=context)

# except Exception as err:
#   traceback.print_exc()
#   Logger.error(err)

# finally:
#   if db is not None:
#     db.close()


cli = FlaskGroup(app)
if __name__  == "__main__":
  # app.config.update(settings.flask())
  cli()
