#!/usr/bin/python3

import argparse
import smtplib
from email.message import EmailMessage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from string import Template
# TODO: use Notification.Email

def getEmailSubject(loader):
  if loader == 'morning':
    return "Good morning from CASPER! Ready to start the day?"
  
  if loader == 'afternoon':
    return "Complete your next CASPER diary"

  if loader == 'evening':
    return "Another day done and dusted, time to unwind"

  return f"CASPER {loader} survey"

def getEmailBody(link, loader):
  #tmp = "" if loader == "singleevening" else loader
  tmp = f"Hi! You survey is available <a href='{link}'>here</a>"
  if loader == "morning":
    tmp = f"Good morning from CASPER. Let’s help you get off to a smooth start! Complete you morning diary <a href='{link}'>here</a>"
  if loader == "afternoon":
    tmp = f"Good afternoon from CASPER. How is your day going so far? Give yourself some headspace by completing your afternoon diary <a href='{link}'>here</a>"
  if loader == "evening":
    tmp = f"Good evening from CASPER. Let’s take a minute just for you and reflect on your experiences. Complete your last diary of the day <a href='{link}'>here</a>"

  return f"""
    <html>
      <body>
        {tmp}. 
      </body>
    </html>
  """
  # Hi! You {loader} survey is available <a href='{link}'>here</a>.

def makeEmailMessage(me, to, subject, body, kind):
  msg = EmailMessage()
  msg['From'] = me
  msg['To'] = to
  msg['Subject'] = subject
  msg.add_alternative(body, subtype=kind)
  return msg

def getEmailSurveyMessage(user, dest, surveylink, subj, loader, body=None):
  message = getEmailBody(surveylink, loader) if not body else body
  msg = EmailMessage()
  msg['From'] = user
  msg['To'] = dest
  msg['Subject'] = subj if subj else getEmailSubject(loader)
  msg.add_alternative(message, subtype='html')
  return msg

def connectToEmailServer(host, port, user, passwd):
  server = smtplib.SMTP(host=host, port=port)
  server.starttls()
  server.login(user, passwd)
  return server

def sendEmail(host, port, user, passwd, dest, surveylink, subj, loader, body=None):
  server = connectToEmailServer(host, port, user, passwd)
  message = getEmailSurveyMessage(user, dest, surveylink, subj, loader, body)
  server.send_message(message)

def sendEmails(host, port, user, passwd, dests, surveylink, subj, loader):
  server = connectToEmailServer(host, port, user, passwd)
  for dest in dests:
    message = getEmailSurveyMessage(user, dest, surveylink, subj, loader)
    server.send_message(message)

def sendEmailsTemplate(host, port, me, passwd, dests, template, surveylink, loader):
  title, body, kind = template['title'], template['body'], template['type']
  title = Template(title).substitute(surveylink=surveylink, loader=loader)
  body = Template(body).substitute(surveylink=surveylink, loader=loader)
  server = connectToEmailServer(host, port, me, passwd)
  for to in dests:
    msg = makeEmailMessage(me, to, title, body, kind)
    server.send_message(msg)


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-H", "--host", help="smtp server host")
  parser.add_argument("-p", "--port", help="smtp server port")
  parser.add_argument("-u", "--user", help="smtp user")
  parser.add_argument("-P", "--password", help="smtp password")
  parser.add_argument("-d", "--destination", help="destination email")
  parser.add_argument("-D", "--destinations", help="destination emails divided by comma")
  parser.add_argument("-l", "--link", help="survey link")
  parser.add_argument("-s", "--subject", help="email subject")
  parser.add_argument("-L", "--loader", help="loader")

  args = parser.parse_args()

  host = args.host
  port = int(args.port)
  user = args.user
  passwd = args.password
  dest = args.destination if args.destination else None
  dests = args.destinations.split(',') if args.destinations else None
  link = args.link
  subj = args.subject
  loader = args.loader

  if dest:
    sendEmail(host, port, user, passwd, dest, link, subj, loader)

  if dests:
    sendEmails(host, port, user, passwd, dests, link, subj, loader)
