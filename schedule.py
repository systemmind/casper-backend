#!/usr/bin/python3
import sys
import argparse
from datetime import datetime
from datetime import timedelta
sys.path.append('../.')
from configuration import Configuration
from casper_db import Database, NotFoundError
from survey_controller import SurveyController
from question_table_controller import QuestionTableController
from notification_controller import NotificationController
from question_controller import QuestionController

parser = argparse.ArgumentParser()
parser.add_argument("-m", "--measurement", help="measurement")
parser.add_argument("-d", "--delay", help="delay of scheduled surveys")
parser.add_argument("-x", "--expired", help="expired of notifications")
parser.add_argument("-w", "--when", help="morning, afternoon, evening")

args = parser.parse_args()

if args.measurement is None:
  print("--measurement argument requred")
  sys.exit(-1)

if args.delay is None:
  print("--delay argument requred")
  sys.exit(-1)

if args.expired is None:
  print("--expired argument requred")
  sys.exit(-1)

if args.when is None:
  print("--when argument requred")
  sys.exit(-1)

if args.when != "morning" and args.when != "afternoon" and args.when != "evening":
  print("--when argument is invalid")
  sys.exit(-1)

measurement = int(args.measurement)
delay = int(args.delay)
when = args.when
expired = int(args.expired)

connection = Database().getConnection()
cursor = connection.cursor()
client = Configuration().client()
questions = [
  "activity",
  "duration",
  "feeling",
  "reason",
  "agree",
  "todo"
]

questionController = QuestionController()
if (args.when == "evening"):
    questionTablesController = QuestionTableController()
    targetQuestionTables = questionTablesController.listByType(connection, 'target')
    questions.extend(map(lambda element: element['name'], targetQuestionTables))
    questions.append(questionController.getQuestionEmotionName())

def __scheduleConcreteQuestion(questionName, surveyId, controller):
  funcName = "insert_question_%s" % questionName
  if hasattr(controller, funcName):
    getattr(controller, funcName)(surveyId, connection)

cursor.execute("""SELECT * FROM users WHERE measurement = %s AND authorized > 0;""", (measurement,))
users = cursor.fetchall()
notificationController = NotificationController(connection)
now = datetime.now() + timedelta(seconds=delay)
expired = now + timedelta(seconds=expired)
for user in users:
  userId = user['id']
  userToken = user['token']
  surveyController = SurveyController()
  surveyId = surveyController.createSurvey(userId, measurement, connection)
  link = notificationController.createSurveyLink(client, surveyId, questions, userToken)
  notificationController.createStaticNotification(userId, link, when, now, expired)
  for q in questions:
    __scheduleConcreteQuestion(q, surveyId, questionController)
