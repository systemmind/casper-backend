It is `CASPER` backend project

To run the appliation you need to have both `python2.7` and `pip` installed.

First install dependencies
```shell
sudo apt-get install python-pip
sudo apt-get install mysql-server
sudo apt-get install libmysqlclient-dev
sudo apt-get install python-flask
pip install flask-restful
pip install flask-cors
pip install mysqlclient
```

Next run the application inside the project dir:
```shell
FLASK_APP=main:app flask run --cert cert/server.crt --key cert/server.key
```

The flask listens `localhost:5000` by default.

API examples:
---
```shell
# list of measuremnts request
curl localhost:5000/api/measurements

#response
[
  {
    "company_url": "http://hello.com",
    "id": 5,
    "manager_email": "manager@mail.com",
    "name": "first",
    "start_date": "2001-01-20",
    "stop_date": "2014-01-20",
    "welcome_email": "welcome2@email.com",
    "welcome_email_id": null
  },
  {
    "company_url": "http://hello.com",
    "id": 6,
    "manager_email": "manager@mail.com",
    "name": "first",
    "start_date": "2001-01-20",
    "stop_date": "2014-01-20",
    "welcome_email": "welcome2@email.com",
    "welcome_email_id": null
  }
]

#get single measurement
curl -X GET  localhost:5000/api/measurements/1

#possible response
{
  "company_url": "firstCompany.com",
  "id": 1,
  "manager_email": "info@abc.com",
  "name": "\"firstCompany\"",
  "start_date": "2019-05-01",
  "started": 0,
  "stop_date": "2019-05-30",
  "welcome_email": "welcome2@email.com",
  "welcome_email_id": null
}

# create new measurement request
curl --header "Content-Type: application/json" --request POST \
    --data '{ \
        "start_date":"2019-05-19",\
        "stop_date":"2019-05-30",\
        "name":"first",\
        "company_url":"http://hello.com",\
        "manager_email":"manager@mail.com",\
        "welcome_email":"welcome2@email.com"}' \
    http://localhost:5000/api/measurements

# response
{
  "company_url": "http://hello.com",
  "id": 7,
  "manager_email": "manager@mail.com",
  "name": "first",
  'start_date': 'Sun, 19 May 2019 00:00:00 GMT',
  'stop_date': 'Thu, 30 May 2019 00:00:00 GMT'
  "welcome_email": "welcome2@email.com",
  "welcome_email_id": null
}

# modify measurement request
curl --header "Content-Type: application/json" --request PUT \
    --data '{ \
        "start_date":"02.02.2019",\
        "stop_date":"16.02.2019",\
        "name":"second",\
        "company_url":"http://hello-world.com",\
        "manager_email":"manager@mail.com",\
        "welcome_email":"welcome777@email.com"}' \
    http://localhost:5000/api/measurements/7

#response
{
  "company_url": "http://hello-world.com",
  "id": 7,
  "manager_email": "manager@mail.com",
  "name": "second",
  "start_date":"02.02.2019",
  "stop_date":"16.02.2019",
  "welcome_email": "welcome777@email.com",
  "welcome_email_id": null
}

#delete measurement request
curl localhost:5000/api/measurements/7 -X DELETE

#response
{
  "company_url": "http://hello-world.com",
  "id": 7,
  "manager_email": "manager@mail.com",
  "name": "second",
  "start_date":"02.02.2019",
  "stop_date":"16.02.2019",
  "welcome_email": "welcome777@email.com",
  "welcome_email_id": null
}


#create new user
curl -H "Content-Type: application/json"  -X  POST   --data '{"measurement":1}'   http://localhost:5000/api/users

#possible response
{
  "authorized": false,
  "date": "2019-01-26",
  "id": 1,
  "measurement": 1,
  "token": "Z2XBYR6TBN7BRKFRQ6JMXQFTTVQEP219U0EPNATOVESWKV1H215GZ9B9TIBK6GR791ZNAQBKRJ3P5C8PNY3ZQ5RAVU6F82IVTS9QE7WXFJ7FNEI5GW9Z1BLMJGB7LLMZ"
}

#authorize user
curl -H "token-Authorization: Z2XBYR6TBN7BRKFRQ6JMXQFTTVQEP219U0EPNATOVESWKV1H215GZ9B9TIBK6GR791ZNAQBKRJ3P5C8PNY3ZQ5RAVU6F82IVTS9QE7WXFJ7FNEI5GW9Z1BLMJGB7LLMZ" -H "Content-Type: application/json" --data '{"authorized": true}'  -X PUT  http://localhost:5000/api/users/1

#possible response
{
  "authorized": true,
  "date": "2019-01-26",
  "id": 1,
  "measurement": 1,
  "token": "Z2XBYR6TBN7BRKFRQ6JMXQFTTVQEP219U0EPNATOVESWKV1H215GZ9B9TIBK6GR791ZNAQBKRJ3P5C8PNY3ZQ5RAVU6F82IVTS9QE7WXFJ7FNEI5GW9Z1BLMJGB7LLMZ"
}

#get single user
curl --header "token-Authorization: H8WUVOWQMADFI251VZR0LKISOP0SFWFG51TRRSQTFT5E9Z10162DI65ZQ5N3ELAO8S8L1ZMG5UFKV6HZUQ5B8ES9PXXEHMANRZLUAZ63GIYGRRBB2WEN3L7XK00XX74C"  -X GET  http://localhost:5000/api/users/1

#possible response
{
  "authorized": true,
  "date": "2019-01-26",
  "id": 1,
  "measurement": 1,
  "token": "Z2XBYR6TBN7BRKFRQ6JMXQFTTVQEP219U0EPNATOVESWKV1H215GZ9B9TIBK6GR791ZNAQBKRJ3P5C8PNY3ZQ5RAVU6F82IVTS9QE7WXFJ7FNEI5GW9Z1BLMJGB7LLMZ"
}

#get list of users
curl  -X GET  http://localhost:5000/api/users --header 'token-Authorization: YA4ENIZJ8AO51C0NWOYNN12UI7LH5ZUYOQZQE6GMGMTMBQ9L2IE77OFNWKM0O3PXU42S5GHYH77L5ZKUXUVDCETNSEI6S0XPP2JZ04W00GKX37N4ISCUYWYN4OPQR4K8'

#possible response
[
  {
    "authorized": true,
    "date": "2019-01-26",
    "id": 1
  },
  {
    "authorized": false,
    "date": "2019-01-26",
    "id": 2
  }
]

#delete user
curl -H "token-Authorization: QBTWNTGZ0572HI59I6A1AJSPJCV431QXCVVG8BG24FM9A0XDQBTL6G4VWG0L50AP9ECI8HK3O22GU7J4ZJ1T6ZIP4AB9FGS0IF3SVXNY9VZWKKJ2134VMPFD1FFW6KD3"  -X DELETE  http://localhost:5000/api/users/21

#possible response
{
  "authorized": false,
  "date": "2019-01-27",
  "id": 21,
  "measurement": 1,
  "token": "QBTWNTGZ0572HI59I6A1AJSPJCV431QXCVVG8BG24FM9A0XDQBTL6G4VWG0L50AP9ECI8HK3O22GU7J4ZJ1T6ZIP4AB9FGS0IF3SVXNY9VZWKKJ2134VMPFD1FFW6KD3"
}

#important before send answer you should send 'curl --header "Content-Type: application/json" --request POST  http://localhost:5000/api/scheduler/measurement/1'
#with conforming values measurement !

#send answer question_feeling
curl --header "Content-Type: application/json" --request POST --data '{"emotion": "challenged", "value":2}' http://localhost:5000/api/survey/3/question/question_feeling

#possible response
{
  "date": "2019-01-31",
  "value": 2,
  "emotion": "challenged",
  "id": 4,
  "measurement": 2,
  "time": "00:00:43",
  "user": 2
}

#send answer question_duration
curl --header "Content-Type: application/json" --request PUT  --data '{"duration":250}'  http://localhost:5000/api/survey/3/question/question_time

#possible response
{
  "date": "2019-01-31",
  "duration": 250,
  "id": 2,
  "measurement": 2,
  "time": "00:35:40",
  "user": 2
}

#send answer question_activity
curl --header "Content-Type: application/json" --request PUT  --data '{"activity":"drink cofe"}'  http://localhost:5000/api/survey/3/question/question_activity

#pssible response
{
  "activity": "drink cofe",
  "date": "2019-01-31",
  "id": 2,
  "measurement": 2,
  "time": "00:47:50",
  "user": 2
}

#send measurement start
curl --header "Content-Type: application/json" --request POST  http://localhost:5000/api/measurement/1/start

#send measurement stop
curl --header "Content-Type: application/json" --request POST  http://localhost:5000/api/measurement/1/stop

#send answer question_agree
curl --header "Content-Type: application/json" --request PUT --data '{"meaning":7,"useful":7,"achievement": 7}' \
    http://localhost:5000/api/survey/3/question/question_agree

#possible response
{
  "achievement": 7,
  "date": "2019-02-05",
  "id": 6,
  "meaning": 7,
  "measurement": 1,
  "survey": 27,
  "time": "13:49:11",
  "useful": 7,
  "user": 25
}

#send answer question_todo
curl --header "Content-Type: application/json" --request PUT --data '{"have": 2, "want": 2, "should": 3}' \
    http://localhost:5000/api/survey/3/question/question_todo

#possible response
{
  "date": "2019-02-07",
  "have": 2,
  "id": 6,
  "measurement": 1,
  "should": 3,
  "survey": 6,
  "time": "14:49:30",
  "user": 25,
  "want": 2
}

#send answer question_age
curl --header "Content-Type: application/json" --request PUT --data '{"age": 25}' \
    http://localhost:5000/api/survey/2/question/question_age

#possible response
{
  "age": "25",
  "date": "2019-02-20",
  "id": 2,
  "survey": 2,
  "time": "19:45:46"
}

#send answer question_experience
curl --header "Content-Type: application/json" --request PUT --data '{"experience": "5-7 ears"}' \
    http://localhost:5000/api/survey/2/question/question_experience

#possible response
{
  "date": "2019-02-20",
  "experience": "5-7 ears",
  "id": 2,
  "survey": 2,
  "time": "19:59:51"
}

#send answer question_gender
curl --header "Content-Type: application/json" --request PUT --data '{"gender": "male"}' \
    http://localhost:5000/api/survey/2/question/question_gender

#possible response
{
  "date": "2019-02-20",
  "gender": "male",
  "id": 2,
  "survey": 2,
  "time": "19:47:37"
}

#get to get list notifications
curl -H "token-Authorization: NWPRB61HOY8VYPAK8EZ4QSGW0VD1IWW2WNSUULXAYHKR6JC93E1BRZ9KW3Q2FSJBPK8P6NNU7SJZ1BDF2A9C3ZS4CHU7E4J6T6733M2N050DQH09HWMFI6ZZXCJ41Z6J" -X  GET    http://localhost:5000/api/user/2/notifications

#possible response
[
  {
    "body": "{\n  \"message\": \"<p>Hi!</p><p>Morning survey is available <a href='http://casper.com/'>here</a></p>\"\n}\n",
    "id": 3
  },
  {
    "body": "{\n  \"message\": \"<p>Hi!</p><p>Evening survey is available <a href='http://casper.com/'>here</a></p>\"\n}\n",
    "id": 4
  },
  {
    "body": "{\"message\": \"What do you think about us?\", \"response\": {\"url\": \"http://casper.com/api/questions/think_table_name/id/45\", \"data\": {\"answer\": \"$answer\"}}}",
    "id": 6
  }
]
#mark notification as delivered
curl -X  PUT -H "token-Authorization: NWPRB61HOY8VYPAK8EZ4QSGW0VD1IWW2WNSUULXAYHKR6JC93E1BRZ9KW3Q2FSJBPK8P6NNU7SJZ1BDF2A9C3ZS4CHU7E4J6T6733M2N050DQH09HWMFI6ZZXCJ41Z6J"
-H "Content-Type: application/json" --data '{"notifications": [601]}' http://localhost:5000/api/user/2/notifications

#possible response
[
  3,
  4,
  6,
  7,
  9,
  10
]

#put fedback response
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87" -H "Content-Type: application/json" --data '{"message": "good feedback"}'  -X PUT  http://localhost:5000/api/feedback/110/answer

#possible response
{
  "date": "2019-03-19",
  "fact": 1,
  "id": 110,
  "picture": "bored1.jpg",
  "response": "good feedback",
  "tip": 1,
  "user": 12
}

#get fact
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87" -X GET http://localhost:5000/api/facts/1

#possible response
{
  "body": "bla bla bla",
  "id": 1,
  "type": "fact"
}

#create fact
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87" -H "Content-Type: application/json" --data '{"body": "bla bla bla", "emotion": 4}' -X POST http://localhost:5000/api/facts

#possible response
{
  "body": "bla bla bla",
  "id": 2,
}

#delete fact
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87" -X DELETE  http://localhost:5000/api/facts/2

#possible response
{
  "body": "bla bla bla",
  "emotion": 4,
  "id": 7
}


#edit fact
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87"  -H "Content-Type: application/json" --data '{ "body": "tru la la"}'  http://localhost:5000/api/facts/3 -X PUT

#possible response
{
  "body": "tru la la",
  "id": 3
}

#create tip
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87" -H "Content-Type: application/json" --data '{"body": "dont bla bla bla", "emotion": 1}' -X POST http://localhost:5000/api/tips

#possible response
{
  "body": "dont bla bla bla",
  "fact": 1,
  "id": 2
}

#edit tip
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87" -H "Content-Type: application/json" --data '{"body": "dont bla bla bla and begin trulala"}' -X PUT http://localhost:5000/api/tips/1

#possible response
{
  "body": "dont bla bla bla and begin trulala",
  "fact": 1,
  "id": 1
}

#get tip
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87" -X GET http://localhost:5000/api/tips/2

#possible response
{
  "body": "dont bla bla bla",
  "fact": 1,
  "id": 2
}

#delete tip
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87" -X DELETE http://localhost:5000/api/tips/14

#possible response
{
  "body": "dont bla bla bla and begin trulala",
  "emotion": 4,
  "id": 14
}


#create emotion
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87" -H "Content-Type: application/json" --data '{"emotion": "sad"}'  -X POST  http://localhost:5000/api/emotions

#possible response
{
  "emotion": "sad",
  "id": 1
}

#get all emotions
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87" -X GET  http://localhost:5000/api/emotions

#possible response
[
  {
    "emotion": "Greatful",
    "id": 4
  },
  {
    "emotion": "Productive",
    "id": 5
  },
  {
    "emotion": "Stressed",
    "id": 6
  },
  {
    "emotion": "Bored",
    "id": 7
  },
  {
    "emotion": "sad",
    "id": 8
  }
]

#edit emotion
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87" -H "Content-Type: application/json" --data '{"emotion": "jjjnjj"}'  -X PUT  http://localhost:5000/api/emotions/1

#possible response
{
  "emotion": "jjjnjj",
  "id": 8
}


#delete emotion
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87" -X DELETE  http://localhost:5000/api/emotions/8

#possible response
{
  "emotion": {
    "emotion": "jjjnjj",
    "id": 8
  },
  "facts": [
    {
      "body": "bla bla bla",
      "emotion": 8,
      "id": 9,
      "type": "fact"
    }
  ],
  "tips": [
    {
      "body": "dont bla bla bla",
      "emotion": 8,
      "id": 15,
      "type": "tip"
    }
  ]
}

#create feedback
curl -H "token-Authorization: WZVYAA2IX0LW7WGWWPTU8GE5NGMUW8ICNR6TH7D6TON79FBEKKRMTDHUPM1A1FXZYR1DQLEJ8BVAK75NQ5744GP7M1XDITDZ10AJXUN1RXMGMTSERY82OXH19Y63LF6F" -H "Content-Type: application/json" --data '{"emotion": "stressed"}'  -X POST  http://localhost:5000/api/survey/60/feedback/emotion

#get surveys
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87" -H "Content-Type: application/json"  -X GET http://localhost:5000/api/survey/90

#possible response
[
  {
    "begin": "15:24:57",
    "date": "2019-03-27",
    "end": "16:24:57",
    "id": 1,
    "measurement": 3,
    "user": 1
  },
  {
    "begin": "15:25:43",
    "date": "2019-03-27",
    "end": "16:25:43",
    "id": 2,
    "measurement": 3,
    "user": 1
  }
]

#get data
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87" -H "Content-Type: application/json" -X GET http://localhost:5000/api/data/table/question_feeling/survey/2

#possible response
[
  {
    "emotion": "bored",
    "value": 2
  },
  {
    "emotion": "sad",
    "value": 3
  }
]

#get picture
curl -X GET http://localhost:5000/api/pictures/bored1.jpg

#get feedback
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87" -X GET http://localhost:5000/api/feedback/13

#get surveys
curl -H "token-Authorization: TIB327DLMBB3AVXDF0GT1SOM3MKD8GDV7LWOOCUJ7ATFFABT07E42UPJ0WXVZ2RA34TTF97TP189LQE4ZNBYF1U7PMLP45LSOTSCWKWMLLMOU399FDIY0OTOWWHTYX87"  -X GET http://localhost:5000/api/feedback/14

To change the administrator password, run
python bcrypt_pwd.py

#possible response
Enter your password:
test
Enter the rounds number (12 by default): #hash iteration number, the more the slower
13
Your bcrypt-hashed password is:
$2b$13$kVx9xTvPXGVjZ4XkzNih0ObMtlxQSCexfHBQdnc9YsIN6/DNy7jjG #copy-past this hash to hardcore your new password in the database
Enter your password again:
test
Passwords match

#create admin token
curl -H "Content-Type: application/json" --data '{"login": "admin", "password": "password"}' -X POST http://localhost:5000/api/login

#echo test the server state
curl -X POST https://localhost:5000/api/echo -k -H "Content-Type: text" --data "hello!" 

```
