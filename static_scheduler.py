from datetime import datetime
from datetime import time
from datetime import timedelta
import threading
from survey_controller import SurveyController
from question_table_controller import QuestionTableController
from notification_controller import NotificationController
from question_controller import QuestionController
from casper_logger import Logger
from scheduler import Scheduler
from survey_reminder import SurveyReminder
from custom_exceptions import MeasurementExpiredError, MeasurementNotFoundError

class StaticScheduler(Scheduler):
  def __init__(self, database, measurement, stopHook):
    super(StaticScheduler, self).__init__(database, measurement)
    self._first_time = time(hour = 6, minute = 0)
    self._middle_time = time(hour = 10, minute = 0)
    self._last_time = time(hour = 14, minute = 0)
    self._time_of_day = ""
    self.questionController = QuestionController()
    self._reminder = SurveyReminder(self._db)
    self.__hoursDelay = 2
    self.__secondsBeforeExpired = 900
    self.__hoursBeforeExpired = 1
    self.__timer = None
    self.__lock = threading.Lock()
    self._connection = database.getConnection()
    self._cursor = self._connection.cursor(dictionary=True)
    self.__stopHook = stopHook

  def stop(self):
    with self.__lock:
      if self.__timer:
        self.__timer.cancel()

  def start(self):
    now = datetime.now()
    now_time = now.time()
    now_date = now.date()
    timeoutFunc = self._scheduleSurveyTimeout

    if now_time < self._first_time:
      delta = datetime.combine(now, self._first_time) - now
      self._time_of_day = "morning"
    elif now_time < self._middle_time and now_time >= self._first_time:
      delta = datetime.combine(now, self._middle_time) - now
      self._time_of_day = "afternoon"
    elif now_time < self._last_time and now_time >= self._middle_time:
      delta = datetime.combine(now, self._last_time) - now
      timeoutFunc = self._scheduleLastSurveyTimeout
      self._time_of_day = "evening"
    else:
      delta = datetime.combine(now, self._first_time) + timedelta(days=1) - now
      self._time_of_day = "morning"

    def func_wrapper():
      try:
        self._connection = self._db.getConnection()
        self._cursor = self._connection.cursor(dictionary=True)
        if datetime.strptime(self._measurement['stop_date'], "%Y-%m-%d").date() >= now_date:
          timeoutFunc()
          with self.__lock:
            self.start()
        else:
          if self.__stopHook:
            self.__stopHook(self)
      except Exception as err:
        Logger.error(str(err))

    self.__timer = threading.Timer(delta.seconds + 1, func_wrapper, ())
    self.__timer.start()

  def _scheduleSurveyTimeout(self):
    questions = (
      "activity",
      "duration",
      "feeling",
      "reason",
      "agree",
      "todo"
    )
    self.__scheduleQuestions(questions)

  def _scheduleLastSurveyTimeout(self):
    questionTablesController = QuestionTableController()
    targetQuestionTables = questionTablesController.listByType(self._connection, 'target')
    questions = [
      "activity",
      "duration",
      "feeling",
      "reason",
      "agree",
      "todo"
    ]
    questions.extend(list(map(lambda element: element['name'], targetQuestionTables)))
    questions.append(self.questionController.getQuestionEmotionName())
    self.__scheduleQuestions(questions)

  def __scheduleQuestions(self, questions):
    self._cursor.execute("""SELECT * FROM users WHERE measurement = %s AND authorized > 0;""", (self._measurement["id"],))
    users = self._cursor.fetchall()
    notificationController = NotificationController(self._connection)
    timeDelay = datetime.now() + timedelta(hours=self.__hoursDelay)
    expired = timeDelay + timedelta(hours=self.__hoursBeforeExpired, seconds=self.__secondsBeforeExpired)
    remindExpired = timeDelay + timedelta(hours=self.__hoursBeforeExpired * 2, seconds=self.__secondsBeforeExpired * 2)
    remindDelay = expired - datetime.now()
    self._reminder.initQuestions(questions)
    for user in users:
      userId = user['id']
      userToken = user['token']
      surveyController = SurveyController()
      surveyId = surveyController.createSurvey(userId, self._measurement["id"], self._connection)
      link = notificationController.createSurveyLink(self.host_port, surveyId, questions, userToken)
      self._reminder.appendRemind(userId, surveyId, link)
      notificationController.createStaticNotification(userId, link, self._time_of_day, timeDelay, expired)
      for q in questions:
        self.__scheduleConcreteQuestion(q, surveyId, self.questionController)

    self._reminder.start(remindDelay.seconds, remindExpired)

  def __scheduleConcreteQuestion(self, questionName, surveyId, controller):
    funcName = "insert_question_%s" % questionName
    if hasattr(controller, funcName):
      getattr(controller, funcName)(surveyId, self._connection)

  @classmethod
  def create(cls, database, measurement_id, stopHook):
    connection = database.getConnection()
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM measurements WHERE id = %s;""", (measurement_id,))
    measurement = cursor.fetchone()
    if measurement is None:
      raise MeasurementNotFoundError(measurement_id)
    elif measurement['stop_date'] < datetime.now().date():
      raise MeasurementExpiredError(measurement_id)
    else:
      return cls(database, measurement, stopHook)
