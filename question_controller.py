from casper_db import Database, NotFoundError
import mysql.connector
from datetime import datetime
from datetime import time
from casper_logger import Logger

class QuestionController(object):
  def updateQuestionShown(self, connection, survey, name, indent):
    cursor = connection.cursor(dictionary=True)
    survey = int(survey)
    name = connection._cmysql.escape_string(name).decode("utf-8")
    values = (survey, indent) if indent else (survey,)
    query = " and indent=%s " if indent else ""
    cursor.execute(f"UPDATE question_{name} SET shown=CURTIME() WHERE survey=%s {query};", values)
    connection.commit()
    cursor.execute(f"SELECT shown FROM question_{name} WHERE survey=%s {query};", values)
    result = cursor.fetchone()
    return result != None

  def questionAnswered(self, survey, name, indent, connection):
    cursor = connection.cursor(dictionary=True)
    funcName = f"isAnswered_question_{name}"
    Logger.debug(f"questionAnswered(): check {funcName} => {hasattr(self, funcName)}")
    if hasattr(self, funcName):
      return getattr(self, funcName)(survey, indent, cursor)

    name = connection._cmysql.escape_string(name).decode("utf-8")
    if indent:
      cursor.execute(
        f"SELECT * FROM question_{name} WHERE survey = %s AND date IS NOT NULL AND indent=%s;",
        (survey, indent)
      )
    else:
      cursor.execute(
        f"SELECT * FROM question_{name} WHERE survey = %s AND date IS NOT NULL;",
        (survey,)
      )

    result = cursor.fetchall()
    return len(result) > 0

  def update_question_age(self, id, json, connection):
    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute(
        """UPDATE question_age SET age=%s, date=CURDATE(), time=CURTIME()
           WHERE id=%s;""",
           (json['age'], id)
      )

      connection.commit()
      return self.getAnswerDataById(id, "age", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def update_question_gender(self, id, json, connection):
    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute(
        """UPDATE question_gender SET gender=%s, date=CURDATE(), time=CURTIME()
           WHERE id=%s;""",
           (json['gender'], id)
      )

      connection.commit()
      return self.getAnswerDataById(id, "gender", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def update_question_experience(self, id, json, connection):
    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute(
        """UPDATE question_experience SET experience=%s, date=CURDATE(), time=CURTIME()
           WHERE id=%s;""",
           (json['experience'], id)
      )

      connection.commit()
      return self.getAnswerDataById(id, "experience", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def update_question_feeling(self, id, json, connection):
    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""
        UPDATE question_feeling
           SET emotion=%s, value=%s, date=CURDATE(), time=CURTIME()
         WHERE id=%s;
      """, (json['emotion'], json['value'], id))
      connection.commit()
      return self.getAnswerDataById(id, "feeling", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def update_question_reason(self, id, json, connection):
    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""UPDATE question_reason SET reason=%s, date=CURDATE(), time=CURTIME()
                          WHERE id=%s;""", (json['reason'], id))
      connection.commit()
      return self.getAnswerDataById(id, "reason", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def update_question_duration(self, id, json, connection):
    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""UPDATE question_duration SET duration=%s, date=CURDATE(), time=CURTIME()
                          WHERE id=%s;""", (json['duration'], id))
      connection.commit()
      return self.getAnswerDataById(id, "duration", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def update_question_activity(self, id, json, connection):
    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""UPDATE question_activity SET activity=%s, date=CURDATE(), time=CURTIME()
                          WHERE id=%s;""", (json['activity'], id))
      connection.commit()
      return self.getAnswerDataById(id, "activity", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def update_question_agree(self, id, json, connection):
    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""UPDATE question_agree SET meaning=%s, useful=%s, achievement=%s, date=CURDATE(), time=CURTIME()
                                WHERE id=%s;""",
                                    (json['useful'],
                                      json['meaning'],
                                      json['achievement'],
                                      id))
      connection.commit()
      return self.getAnswerDataById(id, "agree", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def update_question_todo(self, id, json, connection):
    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute(
        """UPDATE question_todo SET have=%s, want=%s, should=%s, date=CURDATE(), time=CURTIME()
           WHERE id=%s;""",
              (json['have'],
                json['want'],
                json['should'],
                id)
      )

      connection.commit()
      return self.getAnswerDataById(id, "todo", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def update_question_happiness(self, id, json, connection):
    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute(""" UPDATE question_happiness SET happiness=%s, date=CURDATE(), time=CURTIME()
                                  WHERE id=%s;""",
                                    (json['happiness'], id))
      connection.commit()
      return self.getAnswerDataById(id, "happiness", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def update_question_productivity(self, id, json, connection):
    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""UPDATE question_productivity SET productivity=%s, date=CURDATE(), time=CURTIME()
                                  WHERE id=%s;""",
                                    (json['productivity'], id))

      connection.commit()
      return self.getAnswerDataById(id, "productivity", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def update_question_sleepduration(self, id, obj, connection):
    self._updateQuestionValue(connection, 'sleepduration', id, obj)
    return self.getAnswerDataById(id, "sleepduration", connection)

  def update_question_overalldayevaluation(self, id, obj, connection):
    self._updateQuestionValue(connection, 'overalldayevaluation', id, obj)
    return self.getAnswerDataById(id, "overalldayevaluation", connection)

  def update_question_reasonfear(self, id, obj, connection):
    self._updateQuestionValue(connection, 'reasonfear', id, obj)
    return self.getAnswerDataById(id, "reasonfear", connection)

  def update_question_experiencefears(self, id, obj, connection):
    self._updateQuestionValue(connection, 'experiencefears', id, obj)
    return self.getAnswerDataById(id, "experiencefears", connection)

  def update_question_energylevel(self, id, obj, connection):
    self._updateQuestionValue(connection, 'energylevel', id, obj)
    return self.getAnswerDataById(id, "energylevel", connection)

  def update_question_tensionrate(self, id, obj, connection):
    self._updateQuestionValue(connection, 'tensionrate', id, obj)
    return self.getAnswerDataById(id, "tensionrate", connection)

  def update_question_sleepquality(self, id, obj, connection):
    self._updateQuestionValue(connection, 'sleepquality', id, obj)
    return self.getAnswerDataById(id, "sleepquality", connection)

  def _updateQuestionValue(self, connection, question, id, obj):
    cursor = connection.cursor(dictionary=True)
    values = ()
    query = ['modified=NOW()']
    if 'value' in obj:
      query.append('value=%s')
      values = values + (obj['value'],)

    if 'indent' in obj:
      query.append('indent=%s')
      values = values + (obj['indent'],)

    cursor.execute(
      f"""update question_{question} set {", ".join(query)} where id=%s;""",
      values + (id,)
    )

    connection.commit()

  def insert_question_rubbishbin(self, survey, obj, connection):
    try:
      if not 'activity' in obj:
        raise Exception("invalid argument: activity")

      activity = obj['activity']
      indent = obj['indent'] if 'indent' in obj else None

      cursor = connection.cursor(dictionary=True)
      cursor.execute(
        "INSERT INTO question_rubbishbin(activity, date, time, survey, indent) VALUES (%s, CURDATE(), CURTIME(), %s, %s);",
        (activity, survey, indent)
      )

      id = cursor.lastrowid
      assert(id != 0)
      connection.commit()
      return self.getAnswerDataById(id, "rubbishbin", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def insert_question_feeling(self, survey, obj, connection):
    if not 'indent' in obj:
      raise Exception('invalid argument: indent')

    emotion = obj['emotion'] if 'emotion' in obj else None
    value = obj['value'] if 'value' in obj else None
    indent = obj['indent'] if 'indent' in obj else None

    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute(
        """ INSERT INTO question_feeling(emotion, value, date, time, shown, survey, indent)
            VALUES (%s, %s, CURDATE(), CURTIME(), NULL, %s, %s); """,
        (emotion, value, survey, indent))

      id = cursor.lastrowid
      assert(id != 0)
      connection.commit()
      return self.getAnswerDataById(id, "feeling", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def insert_question_reason(self, survey, obj, connection):
    if not 'indent' in obj:
      raise Exception('invalid argument: indent')

    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""
          INSERT INTO question_reason(reason, date, time, shown, survey, indent)
            VALUES (NULL, NULL, NULL, NULL, %s, %s);""",
              (survey, obj["indent"]))

      id = cursor.lastrowid
      assert(id != 0)
      connection.commit()
      return self.getAnswerDataById(id, "reason", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def insert_question_duration(self, survey, obj, connection):
    if not 'indent' in obj:
      raise Exception('invalid argument: indent')

    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""
          INSERT INTO question_duration(duration, date, time, shown, survey, indent)
            VALUES (NULL, NULL, NULL, NULL, %s, %s);""",
              (survey, obj["indent"]))

      id = cursor.lastrowid
      assert(id != 0)
      connection.commit()
      return self.getAnswerDataById(id, "duration", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def insert_question_activity(self, survey, obj, connection):
    if not 'indent' in obj:
      raise Exception('invalid argument: indent')

    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""
          INSERT INTO question_activity(activity, date, time, shown, survey, indent)
            VALUES (NULL, NULL, NULL, NULL, %s, %s);""",
              (survey, obj["indent"]))

      id = cursor.lastrowid
      assert(id != 0)
      connection.commit()
      return self.getAnswerDataById(id, "activity", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def insert_question_agree(self, survey, obj, connection):
    if not 'indent' in obj:
      raise Exception('invalid argument: indent')

    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""
          INSERT INTO question_agree(meaning, useful, achievement, date, time, shown, survey, indent)
            VALUES (-1, -1, -1, NULL, NULL, NULL, %s, %s);""",
              (survey, obj["indent"]))

      id = cursor.lastrowid
      assert(id != 0)
      connection.commit()
      return self.getAnswerDataById(id, "agree", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def insert_question_todo(self, survey, obj, connection):
    if not 'indent' in obj:
      raise Exception('invalid argument: indent')

    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""
          INSERT INTO question_todo(have, want, should, date, time, shown, survey, indent)
            VALUES (-1, -1, -1, NULL, NULL, NULL, %s, %s);""",
              (survey, obj["indent"]))

      id = cursor.lastrowid
      assert(id != 0)
      connection.commit()
      return self.getAnswerDataById(id, "todo", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def insert_question_happiness(self, survey, obj, connection):
    if not 'indent' in obj:
      raise Exception('invalid argument: indent')

    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""
          INSERT INTO question_happiness(happiness, date, time, shown, survey, indent)
            VALUES (NULL, NULL, NULL, NULL, %s, %s);""",
              (survey, obj["indent"]))

      id = cursor.lastrowid
      assert(id != 0)
      connection.commit()
      return self.getAnswerDataById(id, "happiness", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def insert_question_productivity(self, survey, obj, connection):
    if not 'indent' in obj:
      raise Exception('invalid argument: indent')

    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""
          INSERT INTO question_productivity(productivity, date, time, shown, survey, indent)
            VALUES (NULL, NULL, NULL, NULL, %s, %s);""",
              (survey, obj["indent"]))

      id = cursor.lastrowid
      connection.commit()
      return self.getAnswerDataById(id, "productivity", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def insert_question_age(self, survey, obj, connection):
    if not 'indent' in obj:
      raise Exception('invalid argument: indent')

    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""
          INSERT INTO question_age(age, date, time, shown, survey, indent)
            VALUES (NULL,NULL,NULL,NULL,%s,%s);""",
              (survey, obj["indent"]))

      id = cursor.lastrowid
      assert(id != 0)
      connection.commit()
      return self.getAnswerDataById(id, "age", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def insert_question_experience(self, survey, obj, connection):
    if not 'indent' in obj:
      raise Exception('invalid argument: indent')

    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""
          INSERT INTO question_experience(experience, date, time, shown, survey, indent)
            VALUES (NULL,NULL,NULL,NULL,%s,%s);""",
              (survey, obj["indent"]))

      id = cursor.lastrowid
      assert(id != 0)
      connection.commit()
      return self.getAnswerDataById(id, "experience", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def insert_question_gender(self, survey, obj, connection):
    if not 'indent' in obj:
      raise Exception('invalid argument: indent')

    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""
          INSERT INTO question_gender(gender, date, time, shown, survey, indent)
            VALUES (NULL,NULL,NULL,NULL,%s,%s);""",
              (survey, obj["indent"]))
      id = cursor.lastrowid
      assert(id != 0)
      connection.commit()
      return self.getAnswerDataById(id, "gender", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def insert_question_happiness(self, survey, obj, connection):
    if not 'indent' in obj:
      raise Exception('invalid argument: indent')

    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""
          INSERT INTO question_happiness(happiness, date, time, shown, survey, indent)
            VALUES (NULL, NULL, NULL, NULL, %s, %s);""",
              (survey, obj["indent"]))

      id = cursor.lastrowid
      assert(id != 0)
      connection.commit()
      return self.getAnswerDataById(id, "happiness", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def insert_question_productivity(self, survey, obj, connection):
    if not 'indent' in obj:
      raise Exception('invalid argument: indent')

    try:
      cursor = connection.cursor(dictionary=True)
      cursor.execute("""
          INSERT INTO question_productivity(productivity, date, time, shown, survey, indent)
            VALUES (NULL, NULL, NULL, NULL, %s, %s);""",
              (survey, obj["indent"]))

      id = cursor.lastrowid
      assert(id != 0)
      connection.commit()
      return self.getAnswerDataById(id, "productivity", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def insert_question_goodactivities(self, survey, obj, connection):
    if not 'activity' in obj:
        raise Exception("invalid argument: activity")

    try:
      activity = obj['activity']
      indent = obj['indent'] if 'indent' in obj else None;

      cursor = connection.cursor(dictionary=True)
      cursor.execute(
        "INSERT INTO question_goodactivities(activity, date, time, survey, indent) VALUES (%s, CURDATE(), CURTIME(), %s, %s);",
        (activity, survey, indent)
      )

      id = cursor.lastrowid
      assert(id != 0)
      connection.commit()
      return self.getAnswerDataById(id, "goodactivities", connection)
    except mysql.connector.IntegrityError as err:
      raise NotFoundError(str(err))

  def insert_question_sleepduration(self, survey, obj, connection):
    id = self._insertQuestionValue(connection, 'sleepduration', survey, obj)
    return self.getAnswerDataById(id, 'sleepduration', connection)

  def insert_question_overalldayevaluation(self, survey, obj, connection):
    id = self._insertQuestionValue(connection, 'overalldayevaluation', survey, obj)
    return self.getAnswerDataById(id, 'overalldayevaluation', connection)

  def insert_question_reasonfear(self, survey, obj, connection):
    id = self._insertQuestionValue(connection, 'reasonfear', survey, obj)
    return self.getAnswerDataById(id, 'reasonfear', connection)

  def insert_question_experiencefears(self, survey, obj, connection):
    id = self._insertQuestionValue(connection, 'experiencefears', survey, obj)
    return self.getAnswerDataById(id, 'experiencefears', connection)

  def insert_question_energylevel(self, survey, obj, connection):
    id = self._insertQuestionValue(connection, 'energylevel', survey, obj)
    return self.getAnswerDataById(id, 'energylevel', connection)

  def insert_question_tensionrate(self, survey, obj, connection):
    id = self._insertQuestionValue(connection, 'tensionrate', survey, obj)
    return self.getAnswerDataById(id, 'tensionrate', connection)

  def insert_question_sleepquality(self, survey, obj, connection):
    id = self._insertQuestionValue(connection, 'sleepquality', survey, obj)
    return self.getAnswerDataById(id, 'sleepquality', connection)

  def _insertQuestionValue(self, connection, question, survey, obj):
    cursor = connection.cursor(dictionary=True)
    value = obj['value'] if 'value' in obj else None
    shown = int(obj['shown']) if 'shown' in obj else None
    indent = obj['indent'] if 'indent' in obj else None
    cursor.execute(f"""
      insert into question_{question}(value, modified, shown, survey, indent)
      values(%s,NOW(),%s,%s,%s)
    """, (value, shown, survey, indent))
    id = cursor.lastrowid
    connection.commit()
    return id

  def deleteAnswerById(self, id, name, connection):
    record = self.getAnswerDataById(id, name, connection)
    cursor = connection.cursor(dictionary=True)
    query = connection._cmysql.escape_string(
      f"DELETE FROM question_{name} WHERE id={id};"
    ).decode("utf-8")

    cursor.execute(query)
    connection.commit()
    return record

  def getAnswerDataById(self, id, name, connection):
    cursor = connection.cursor(dictionary=True)
    query = connection._cmysql.escape_string(f"select * from question_{name} where id=%s;")
    cursor.execute(query, (id,))
    result = cursor.fetchone()
    if not result:
      raise NotFoundError(f"id #{id} not found")

    return result

  def getAnswerDataBySurvey(self, surveyId, name, connection):
    cursor = connection.cursor(dictionary=True)
    name = connection._cmysql.escape_string(name).decode("utf-8")
    cursor.execute(f"SELECT * FROM question_{name} WHERE survey = %s;", (surveyId,))
    result = cursor.fetchall()
    if not result:
      raise NotFoundError(f"{surveyId} survey not found")

    return result

  def getAnswerDataBySurveyAndIndent(self, survey, name, indent, connection):
    cursor = connection.cursor(dictionary=True)
    name = connection._cmysql.escape_string(name).decode("utf-8")
    cursor.execute(f"SELECT * FROM question_{name} WHERE survey=%s AND indent=%s;", (survey, indent))
    result = cursor.fetchall()
    if not result:
      raise NotFoundError(f"{survey} survey and {indent} indent not found")

    return result

  def getQuestionEmotionName(self):
    return "emotion"

  def isAnswered_question_sleepduration(self, survey, indent, cursor):
    return self._isAnsweredByValue(cursor, 'sleepduration', survey, indent)

  def isAnswered_question_overalldayevaluation(self, survey, indent, cursor):
    return self._isAnsweredByValue(cursor, 'overalldayevaluation', survey, indent)

  def isAnswered_question_reasonfear(self, survey, indent, cursor):
    return self._isAnsweredByValue(cursor, 'reasonfear', survey, indent)

  def isAnswered_question_experiencefears(self, survey, indent, cursor):
    return self._isAnsweredByValue(cursor, 'experiencefears', survey, indent)

  def isAnswered_question_energylevel(self, survey, indent, cursor):
    return self._isAnsweredByValue(cursor, 'energylevel', survey, indent)

  def isAnswered_question_tensionrate(self, survey, indent, cursor):
    return self._isAnsweredByValue(cursor, 'tensionrate', survey, indent)

  def isAnswered_question_sleepquality(self, survey, indent, cursor):
    return self._isAnsweredByValue(cursor, 'sleepquality', survey, indent)

  def _isAnsweredByValue(self, cursor, question, survey, indent):
    values = (survey, indent) if indent else (survey,)
    query = " and indent=%s " if indent else ""
    cursor.execute(f"""
      select *
        from question_{question}
       where survey=%s
         and value is not null {query};
    """, values)
    return len(cursor.fetchall()) > 0

  def isAnswered_question_feeling(self, survey, indent, cursor):
    values = (survey, indent) if indent else (survey,)
    query = " AND indent=%s " if indent else ""
    cursor.execute(f"""
      select * from question_feeling
      where survey=%s and emotion is not null and value is not null {query};""",
      values
    )

    return len(cursor.fetchall()) > 0

  def isAnswered_question_rubbishbin(self, survey, indent, cursor):
    return self._isAnsweredByRecords(cursor, 'rubbishbin', survey, indent)

  def isAnswered_question_goodactivities(self, survey, indent, cursor):
    return self._isAnsweredByRecords(cursor, 'goodactivities', survey, indent)

  def _isAnsweredByRecords(self, cursor, question, survey, indent):
    values = (survey, indent) if indent else (survey,)
    query = " AND indent=%s " if indent else ""
    cursor.execute(f"""SELECT * FROM question_{question} WHERE survey=%s {query};""", values)
    return len(cursor.fetchall()) > 0

  def isAnswered_question_emotion(self, survey, indent, cursor):
    # TODO: either use feedback controller or create addition table for emotion question
    # or remove all controller and combine all required database request sets together
    cursor.execute("""
       SELECT survey.*
       FROM survey, feedback
       WHERE feedback.user = survey.user
         AND survey.id = %s
         AND feedback.date = NOW();
    """, (survey,))
    result = cursor.fetchall()
    return len(result) > 0
