import mysql.connector

class QuestionTableController(object):
  def listByType(self, connection, type):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT name FROM questions where type = %s;""", (type, ))
    return cursor.fetchall()

  def all(self, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM questions;""")
    return cursor.fetchall()
