#!/usr/bin/python3
import sys
import math
import argparse
import json
import random
from datetime import datetime
from datetime import timedelta
from configuration import glob as settings
from casper_db import Database, NotFoundError
from question_table_controller import QuestionTableController
from notification_controller import NotificationController
from question_controller import QuestionController
from survey_controller import SurveyController
from notification_controller import NotificationController
from notification_email import sendEmailsTemplate


connection = Database().getConnection()
cursor = connection.cursor(dictionary=True)
client = settings.client()

notificationController = NotificationController(connection)
surveyController = SurveyController()

usernotificationsset = []

cursor.execute("""
  select schedulers.*
    from schedulers
left join measurements on schedulers.measurement = measurements.id
   where schedulers.begin <= now()
     and schedulers.end > now()
     and measurements.started = 1
     and measurements.stop_date >= now();
""")

schedulers = cursor.fetchall()
print(f"schedulers: {list(map(lambda x: x['id'], schedulers))}")

for scheduler in schedulers:
  measurement = scheduler["measurement"]
  indent = scheduler["id"]
  #begin = datetime.strptime(scheduler["begin"],"%H:%M:%S")
  #end = datetime.strptime(scheduler["end"],"%H:%M:%S")
  begin = scheduler["begin"]
  end = scheduler["end"]
  scheduleDiscrete = scheduler["schedule_discrete"]
  surveyExpiration = scheduler["survey_expiration"]
  loader = scheduler["loader"]
  type = scheduler["type"]

  print(f"scheduling {indent} scheduler")

  where = """
    date between date_add(curdate(), interval time_to_sec(%s) second)
             and date_add(curdate(), interval time_to_sec(%s) second)
  """ if type == 'cyclic' else """
    time(date) between %s and %s
  """ if type == 'onetime' else None #"error = bad schedule type"

  if not where:
    continue 

  selectUserNotifications = f"""
    select *
      from usernotifications
     where {where}
       and indent = %s
  """

  cursor.execute(f"""
    select users.id, users.token as usertoken, usersdevices.token as devicetoken
      from users
 left join usersdevices
        on usersdevices.user = users.id
 left join ({selectUserNotifications}) as usernotif
        on users.id = usernotif.user
     where users.measurement = %s
       and users.authorized > 0
       and usernotif.id IS NULL;
  """, (begin,end,indent,measurement))

  users = cursor.fetchall()

  print(f"Users: {str(users)}")

  userlen = len(users)
  if userlen < 1:
    continue

  cursor.execute("""
    select COUNT(*)
      from users
     where measurement = %s
       and authorized > 0;
  """, (measurement,))

  result = cursor.fetchone()
  totalUsers = result["COUNT(*)"]

  usersBunch = min(
    math.ceil(totalUsers / ((end - begin).total_seconds() / scheduleDiscrete)),
    userlen
  )

  print(f"Users bunch {usersBunch}")

  '''
  cursor.execute(
    "select * from scheduler_questions where scheduler=%s;",
    (indent,)
  )

  questions = list(map(lambda x: x["question"], cursor.fetchall()))
  '''

  cursor.execute("""
    select title, body, type
      from scheduler_emails
 left join emails_templates
        on scheduler_emails.email = emails_templates.id
     where scheduler_emails.scheduler=%s
  """, (indent,))

  emailsTemplates = cursor.fetchall()

  for i in random.sample(range(0, userlen), usersBunch):
    user = users[i]
    userId = user['id']
    userToken = user['usertoken']
    devicetoken = user["devicetoken"]

    cursor.execute("""select * from useremails where user=%s""", (userId,))
    emails = list(map(lambda x: x['email'], cursor.fetchall()))

    surveyId = surveyController.createSurvey(
      userId,
      measurement,
      indent,
      connection,
      datetime.now() + timedelta(minutes=surveyExpiration)
    )

    # surveyController.addQuestions(surveyId, questions, connection)
    link = notificationController.createSurveyLink(client, userId, surveyId, loader, userToken)
    notificationController.createStaticNotification(
      userId,
      link,
      loader,
      datetime.now(),
      datetime.now() + timedelta(minutes=surveyExpiration)
    )

    # notificationController.sendSurveyXmppNotification(userId, link, loader)
    if devicetoken:
      notificationController.sendSurveyPushNotification(devicetoken, link, loader)

    emailTemplate = random.choice(emailsTemplates)

    sendEmailsTemplate(
      settings.smtpHost(),
      settings.smtpPort(),
      settings.smtpUser(),
      settings.smtpPassword(),
      emails, emailTemplate,
      link, loader
    )

    usernotificationsset.append((measurement, userId, surveyId, indent))

cursor.executemany("""
  INSERT INTO
    usernotifications(measurement, user, survey, indent, date, body)
  VALUES (%s, %s, %s, %s, NOW(), NULL);
""", usernotificationsset)

connection.commit()
