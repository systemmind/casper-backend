from pyfcm import FCMNotification
from configuration import glob as settings

push_service = FCMNotification(api_key=settings.fcmApiKey())

def sendMessage(device, title, body, link):
  push_service.notify_single_device(
    registration_id=device,
    message_title=title,
    message_body=body,
    click_action=link
  )
