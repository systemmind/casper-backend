import os
import logging
from logging.handlers import TimedRotatingFileHandler
import json
from configuration import glob as settings

#with open('config.json', "r", encoding="utf-8") as f:
#  config = json.load(f)

config = settings.config

_levels = {
  logging.DEBUG : "DEBUG",
  logging.INFO  : "INFO",
  logging.WARNING  : "WARNING",
  logging.ERROR : "ERROR",
  logging.CRITICAL : "CRITICAL"
}

def getLevel():
  for key in _levels:
    if _levels[key] == config['log']['level']:
      return key;
  return logging.DEBUG;


_formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(filename)s:%(lineno)d %(message)s')
_fileHandler = TimedRotatingFileHandler(os.path.join(config['log']['path'], 'casper.log'),
                                     when='midnight',
                                     backupCount=0)
_fileHandler.setFormatter(_formatter)
_coutHandler = logging.StreamHandler()
_coutHandler.setFormatter(_formatter)

Logger = logging.getLogger()
Logger.addHandler(_fileHandler)
Logger.addHandler(_coutHandler)

def setLevel():
    global Logger
    Logger.setLevel(getLevel())
    Logger.info("[LOGGER] level: " + config['log']['level'])

setLevel()
