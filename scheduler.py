from configuration import glob as settings

class Scheduler(object):
  def __init__(self, database, measurement):
    self._db = database
    self._measurement = measurement
    self.host_port = settings.client()
    assert(self.host_port is not None)

  def stop(self):
    pass

  def start(self):
    pass
