from casper_db import Database
from question_controller import QuestionController
from notification_controller import NotificationController
from question_table_controller import QuestionTableController
from survey_controller import SurveyController
from scheduler import Scheduler
from casper_logger import Logger
import fcm

class DemographicScheduler(Scheduler):
  def __init__(self, database, measurement_id, user_id):
    super(DemographicScheduler, self).__init__(database, measurement_id)
    self.userId = user_id

  def start(self):
    self._connection = self._db.getConnection()
    cursor = self._connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM users WHERE id = %s;""", (self.userId, ))
    user = cursor.fetchone()
    userId = user.get("id")
    assert(userId is not None)
    userToken = user.get('token')
    assert(userToken is not None)
    cursor.execute("""SELECT * FROM usersdevices WHERE user = %s;""", (self.userId, ))
    surveyController = SurveyController()
    device = cursor.fetchone()
    devicetoken = device['token'] if device else None
    survey = surveyController.createSurvey(self.userId, self._measurement, self._connection)
    assert(survey is not None)
    questionController = QuestionController()
    questionTablesController = QuestionTableController()
    questionTables = questionTablesController.listByType(self._connection, 'demographic')
    questions = list(map(lambda element: element['name'], questionTables))
    assert(len(questions) > 0)
    surveyController.addQuestions(survey, questions, self._connection)
    notificationController = NotificationController(self._connection)
    #link = notificationController.createSurveyLink(self.host_port, survey, questions, userToken)
    loader = "demographic"
    link = notificationController.createSurveyLink(self.host_port, survey, loader, userToken)
    notificationController.createDemographicNotification(self.userId, link, self._measurement)
    notificationController.sendSurveyXmppNotification(self.userId, link, loader)
    if devicetoken:
      notificationController.sendDemographicNotification(devicetoken, link);

    """
    for q in questions:
      self.__scheduleConcreteQuestion(q, survey, questionController)
    """

    self._connection.close()

  """
  def __scheduleConcreteQuestion(self, questionName, surveyId, controller):
    funcName = "insert_question_%s" % questionName
    getattr(controller, funcName)(surveyId, self._connection)
  """
