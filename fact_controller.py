from casper_db import NotFoundError


class FactsController(object):
  def getFactById(self, factId, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM facts WHERE id = %s;""", (factId,))
    fact = cursor.fetchone()
    if not fact:
      raise NotFoundError("fact not found")

    return fact

  def getFactsByEmotion(self, emotionId, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM facts WHERE emotion = %s;""", (emotionId,))
    facts = cursor.fetchall()
    if not len(facts):
      raise NotFoundError("fact not found")

    return facts

  def insertFact(self, body, emotion, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""INSERT INTO facts (body, emotion)
                      VALUES(%s, %s);""", (body, emotion))

    id = cursor.lastrowid
    connection.commit()
    return self.getFactById(id, connection)

  def editFact(self, factId, body, connection):
    self.getFactById(factId, connection)
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""UPDATE facts SET body = %s WHERE id = %s""", (body, factId))
    connection.commit()
    return self.getFactById(factId, connection)

  def deleteFact(self, factId, connection):
    cursor = connection.cursor(dictionary=True)
    fact = self.getFactById(factId, connection)
    cursor.execute("""DELETE from facts WHERE id = %s;""", (factId, ))
    connection.commit()
    return fact
