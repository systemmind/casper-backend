import json
import os

class Configuration(object):
  def load(self, path='settings.json'):
    with open(path, "r") as f:
      self.config = json.load(f)

  def flask(self):
    appConfig = {}
    serverName = "%s:%s" % (self.config['server']['domain'], self.config['server']['port'],)
    appConfig.update({"SERVER_NAME": serverName})
    appConfig.update({'DEBUG': self.config['server']['DEBUG']})
    #appConfig.update({'CORS_ENABLED': False})
    return appConfig

  def client(self):
    client = "%s:%s" % (self.config['client']['domain'], self.config['client']['port'],)
    return client

  def api(self):
    return self.config['api']

  def pictures(self):
    return self.config["paths"]["pictures"]

  def server(self):
    return self.flask()["SERVER_NAME"]

  def serverDomain(self):
    return self.config['server']['domain']
  
  def databaseHost(self):
    return self.config["database"]["host"]

  def databaseUser(self):
    return self.config["database"]["user"]

  def databasePassword(self):
    return self.config["database"]["password"]

  def databaseName(self):
    return self.config["database"]["name"]

  def dbDumps(self):
    return self.config["database"]["dumps"]

  def ejabberdctl(self):
    return self.config["xmpp"]["ejabberdctl"]

  def xmppServer(self):
    return self.config["xmpp"]["server"]
  
  def xmppAdminUser(self):
    return self.config["xmpp"]["admin"]["user"]
  
  def xmppAdminToken(self):
    return self.config["xmpp"]["admin"]["token"]
  
  def xmppAdminHost(self):
    return self.config["xmpp"]["admin"]["host"]
  
  def xmppAdminPort(self):
    return self.config["xmpp"]["admin"]["port"]

  def fcmApiKey(self):
    return self.config["fcm"]["apiKey"]

  def smtpHost(self):
    return self.config["smtp"]["host"]

  def smtpPort(self):
    return self.config["smtp"]["port"]

  def smtpUser(self):
    return self.config["smtp"]["user"]

  def smtpPassword(self):
    return self.config["smtp"]["password"]

glob = Configuration()
glob.load()
