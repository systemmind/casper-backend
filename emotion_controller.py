from casper_db import NotFoundError


class EmotionsController(object):
  def getEmotionById(self, emotionId, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM emotions WHERE id = %s;""", (emotionId, ))
    emotion = cursor.fetchone()
    if not emotion:
      raise NotFoundError("emotion not found")

    return emotion

  def getEmotionByName(self, emotion, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM emotions WHERE emotion = %s;""", (emotion, ))
    emotion = cursor.fetchone()
    if not emotion:
      raise NotFoundError("emotion not found")

    return emotion

  def getEmotions(self, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM emotions;""")
    emotions = cursor.fetchall()
    if not len(emotions):
      raise NotFoundError("emotions not found")

    return emotions

  def insertEmotion(self, emotion, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""INSERT INTO emotions (emotion)
                      VALUES(%s);""", (emotion, ))
    id = cursor.lastrowid
    connection.commit()
    return self.getEmotionById(id, connection)

  def deleteEmotion(self, emotionId, connection):
    result = self.getEmotionById(emotionId, connection)
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM facts WHERE emotion = %s;""", (emotionId, ))
    facts = cursor.fetchall()
    cursor.execute("""SELECT * FROM tips WHERE emotion = %s;""", (emotionId, ))
    tips = cursor.fetchall()
    if len(facts):
      cursor.execute("""DELETE FROM facts WHERE emotion = %s;""", (emotionId, ))

    if len(tips):
      cursor.execute("""DELETE FROM tips WHERE emotion = %s;""", (emotionId, ))

    cursor.execute("""DELETE FROM emotions WHERE id = %s;""", (emotionId, ))
    connection.commit()
    response = {
      "emotion": result,
      "facts": facts,
      "tips": tips
    }
    return response

  def editEmotion(self, emotionId, emotion, connection):
    self.getEmotionById(emotionId, connection)
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""UPDATE emotions SET emotion = %s WHERE id = %s;""", (emotion, emotionId))
    connection.commit()
    return self.getEmotionById(emotionId, connection)
