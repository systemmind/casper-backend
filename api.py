#! /usr/bin/env python
from flask import Flask, abort, request, jsonify, send_file, Blueprint
from flask_restful import Resource, Api
from flask_cors import CORS, cross_origin
import mysql.connector
from casper_db import Database, NotFoundError
from question_controller import QuestionController
import json
from casper_logger import Logger
from notification_controller import NotificationController
from feedback_controler import FeedbackController
from survey_controller import SurveyController
from configuration import glob as settings
import os
import base64
from pool_schedulers import PoolSchedulers
from admin_controller import AdminController
from custom_exceptions import PasswordMismatchError, UserNotFoundError
from fact_controller import FactsController
from emotion_controller import EmotionsController
from tip_controller import TipsController
from picture_controller import PicturesController
from xmppcontroller import XmppControllerXmlrpc as XmppController
import traceback
#from user_delete import deleteUserById
#from route_measurement import *
from route_user_xmpp import *

dateformat = '%Y-%m-%d'
app = Flask(__name__)
bp = Blueprint('bp', __name__)
CORS(bp)
api = Api(bp)
#settings = Configuration()
# app.config.update(settings.flask())

def shutdown():
  request.environ.get('werkzeug.server.shutdown')

@bp.route('/echo', methods=['POST'])
@cross_origin()
def echo():
  try:
    return (request.data, 200)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

'''
@bp.route('/login', methods=['POST'])
@cross_origin()
def createAuthToken():
  try:
    login = request.get_json()['login']
    password = request.get_json()['password']
    connection = Database().getConnection()
    result = AdminController().createAuthToken(login, password, connection)
    return(jsonify({'token':result}), 200)
  except PasswordMismatchError:
    return ('password mismatch', 401)
  except UserNotFoundError:
    return ('user not found', 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/updatetoken', methods=['PUT'])
@cross_origin()
def update_token():
  try:
    connection = Database().getConnection()
    control = AdminController()
    token = request.headers.get('token-Authorization')
    if (control.verifyAuthToken(token, connection)):
      result = control.updateAuthToken(token, connection)
      if result:
        return ('token updated till ' + result, 200)
      else:
        return ('token expired', 401)
    else:
      return ('access denied', 401)

  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)
'''

"""
@bp.route('/measurements', methods=['GET'])
@cross_origin()
def getMeasurements():
  return routeGetMeasurements()

@bp.route('/measurements/<id>', methods=['GET'])
@cross_origin()
def getMeasurement(id):
  return routeGetMeasurement(int(id))

@bp.route('/measurements', methods=['POST'])
@cross_origin()
def createMeasurement():
  return routeCreateMeasurement()

@bp.route('/measurements/<id>', methods=['PUT'])
@cross_origin()
def editMeasurement(id):
  return routeEditMeasurement(int(id))

@bp.route('/measurements/<id>', methods=['DELETE'])
@cross_origin()
def deleteMeasurement(id):
  return routeDeleteMeasurement(int(id))

"""

#from route_user import *

# router to create admin users
# change it later
'''
@bp.route('/users', methods=['POST'])
@cross_origin()
def createUser():
  try:
    json = request.get_json()
    measurement = json['measurement']
    connection = Database().getConnection()
    result = UserController().createUser(measurement, connection)
    return (jsonify(result), 201)

  except AuthorizationError as err:
    Logger.error(str(err))
    return (str(err), 401)
  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)
'''
'''
# router to create users
@bp.route('/measurements/<id>/users', methods=['POST'])
@cross_origin()
def createUsers(id):
  return routeCreateUsers(int(id))

@bp.route('/measurements/<measurement>/users/<user>', methods=['PUT'])
@cross_origin()
def editUser(measurement, user):
  return routeEditUser(int(measurement), int(user))

@bp.route('/measurements/<id>/users', methods=['GET'])
@cross_origin()
def readUsers(id):
  return routeReadUsers(int(id))

'''  

'''
  try:
    token = request.headers.get('token-Authorization')
    connection = Database().getConnection()
    admin = AdminController()
    if (admin.verifyAuthToken(token, connection)):
      measurement = int(id)
      result = UserController().getUsers(connection, measurement)
      return (jsonify(result), 201)
    else:
      return ('access denied', 401)
  except Exception as err:
    Logger.exception(err)
    return (str(err), 500)
  '''

@bp.route('/measurements/<measurement>/users/<user>', methods=['GET'])
@cross_origin()
def readUser(measurement, user):
  return routeReadUser(int(measurement), int(user))

@bp.route('/measurements/<measurement>/users/<user>/xmpp', methods=['POST'])
@cross_origin()
def createXmppUsers(measurement, user):
  return createXmppUser(int(measurement), int(user))

'''
# TODO: deprecated method, use user put request and xmpp user post request instead
@bp.route('/users/<user>', methods=['PUT'])
@cross_origin()
def authorizeToken(user):
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('bad token', 412)

    json = request.get_json()
    db = Database()
    connection = db.getConnection()
    xmppUser = f"user{user}"
    result = UserController().authorizeUserToken(user, token, json, connection)
    measurement = result['measurement']
    xmppPassword = token[:6]
    xmppController = XmppController()
    osResult = xmppController.createUser(user, xmppPassword)
    if not osResult:
      raise Exception(f"Failed to create XMPP user")

    #
    osResult = xmppController.addUserToRoom(user, measurement)
    if not osResult:
      raise Exception(f"Failed to add XMPP user to room")
    #

    osResult = xmppController.addToRoster(user, "casper")
    if not osResult:
      raise Exception(f"Failed to add casper to XMPP user roster")

    osResult = xmppController.addToRoster(user, "support")
    if not osResult:
      raise Exception(f"Failed to add support to XMPP user roster")

    result["xmpp"] = {
      "server": settings.xmppServer(),
      "user": xmppUser,
      "password": xmppPassword
    }

    notificationController = NotificationController(connection)
    notificationController.createSupportNotification(user, result['measurement'])
    return (jsonify(result), 200)

  except AuthorizationError as err:
    Logger.error(err, print_exc=True)
    return (str(err), 401)
  except NotFoundError as err:
    Logger.error(err, print_exc=True)
    return (str(err), 404)
  except Exception as err:
    Logger.error(err, print_exc=True)
    return (str(err), 500)
'''

'''
@bp.route('/users/<id>', methods=['GET'])
@cross_origin()
def get_single_user(id):
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('bad token', 412)

    connection = Database().getConnection()
    cursor = connection.cursor(dictionary=True)
    user = UserController().findUserById(id, connection)
    if token != user['token']:
      return ('bad token', 401)

    user['authorized'] = True if user['authorized'] > 0 else False
    return jsonify(user)

  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/users', methods=['GET'])
@cross_origin()
def get_all_users():
  try:
    connection = Database().getConnection()
    cursor = connection.cursor(dictionary=True)
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    control = AdminController()
    if (control.verifyAuthToken(token, connection)):
      cursor.execute("SELECT id, date, authorized FROM users")
      result = cursor.fetchall()
      if not result:
        return ([],200)
    else:
      return ('access denied', 401)

    for user in result:
      user['authorized'] = True if user['authorized'] > 0 else False

    return (jsonify(result), 201)

  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

'''
'''
@bp.route('/users/<id>', methods=['DELETE'])
@cross_origin()
def delete_user(id):
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('bad token', 412)

    connection = Database().getConnection()
    admin = AdminController()
    if (admin.verifyAuthToken(token, connection)):
      user = int(id)
      result = deleteUserById(connection.cursor(dictionary=True), user)
      connection.commit()
      return (jsonify(result), 201)
    else:
      return ('access denied', 401)

    
    #TODO: use UserController for all manipulations with users
    connection = Database().getConnection()
    cursor = connection.cursor(dictionary=True)
    cursor.execute("SELECT * FROM users WHERE id = %s;", (id,))
    result = cursor.fetchone()
    if not result:
      return ('user not found', 404)

    if result['token'] != token:
      return ('bad token', 401)

    cursor.execute("DELETE FROM users WHERE id = %s;", (id,))
    connection.commit()
    result['authorized'] = True if result['authorized'] > 0 else False
    return (jsonify(result), 200)
    
  except Exception as err:
    Logger.exception(err)
    return (str(err), 500)
'''

# creates new record for given question
@bp.route('/survey/<surveyId>/question/<name>', methods=['POST'])
@cross_origin()
def postQuestionData(surveyId, name):
  try:
    obj = request.get_json()
    token = request.headers.get("token-Authorization")
    if not token:
      return ('missed token', 412)

    updater = QuestionController()
    connection = Database().getConnection()
    UserController().findUserBySurveyAndToken(surveyId, token, connection)
    cursor = connection.cursor(dictionary=True)
    funcName = "insert_question_{0}".format(name)
    if not hasattr(updater, funcName):
      return ('function not found', 404)

    result = getattr(updater, funcName)(surveyId, obj, connection)
    return (json.dumps(result, default=str), 200)
  except AuthorizationError as err:
    traceback.print_exc()
    Logger.error(str(err))
    return (str(err), 401)
  except NotFoundError as err:
    traceback.print_exc()
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    traceback.print_exc()
    Logger.error(str(err))
    return (str(err), 500)

"""
@bp.route('/survey/<surveyId>/question/<name>', methods=['PUT'])
@cross_origin()
def putQuestionData(surveyId, name):
  try:
    obj = request.get_json()
    token = request.headers.get("token-Authorization")
    if not token:
      return ('missed token', 412)

    updater = QuestionController()
    connection = Database().getConnection()
    UserController().findUserBySurveyAndToken(surveyId, token, connection)
    cursor = connection.cursor(dictionary=True)
    #funcName = "update_question_%s" % cursor.connection.converter.escape(name)
    funcName = "update_question_{0}".format(name)
    if not hasattr(updater, funcName):
      return ('function not found', 404)

    if updater.questionAnswered(surveyId, name, connection):
      return ('question is answered already', 403)

    result = getattr(updater, funcName)(surveyId, obj, connection)
    return (json.dumps(result, default=str), 200)
  except AuthorizationError as err:
    traceback.print_exc()
    Logger.error(str(err))
    return (str(err), 401)
  except NotFoundError as err:
    traceback.print_exc()
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    traceback.print_exc()
    Logger.error(str(err))
    return (str(err), 500)
"""

# updates question record by given id
@bp.route('/survey/<surveyId>/question/<name>/<id>', methods=['PUT'])
@cross_origin()
def putQuestionData(surveyId, name, id):
  try:
    obj = request.get_json()
    indent = obj['indent'] if 'indent' in obj else None
    token = request.headers.get("token-Authorization")
    if not token:
      return ('missed token', 412)

    updater = QuestionController()
    connection = Database().getConnection()
    UserController().findUserBySurveyAndToken(surveyId, token, connection)
    cursor = connection.cursor(dictionary=True)
    funcName = connection._cmysql.escape_string(f"update_question_{name}").decode("utf-8")
    if not hasattr(updater, funcName):
      return ('function not found', 404)

    """
    if updater.questionAnswered(surveyId, name, indent, connection):
      return ('question is answered already', 403)
    """

    result = getattr(updater, funcName)(id, obj, connection)
    return (json.dumps(result, default=str), 200)
  except AuthorizationError as err:
    traceback.print_exc()
    Logger.error(str(err))
    return (str(err), 401)
  except NotFoundError as err:
    traceback.print_exc()
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    traceback.print_exc()
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/survey/<surveyId>/question/<name>', methods=['GET'])
@cross_origin()
def getQuestionData(name, surveyId):
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('empty token', 412)

    indent = request.args.get('indent')
    connection = Database().getConnection()
    UserController().findUserBySurveyAndToken(surveyId, token, connection)
    data = None
    if indent is None:
      data = QuestionController().getAnswerDataBySurvey(surveyId, name, connection)
    else:
      data = QuestionController().getAnswerDataBySurveyAndIndent(surveyId, name, indent, connection)

    return (json.dumps(data, default=str), 200)

  except AuthorizationError as err:
    Logger.error(str(err))
    return (str(err), 401)
  except mysql.connector.Error as err:
    Logger.error(str(err))
    return (str(err), 500)
  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

# delete question record by given id
@bp.route('/survey/<surveyId>/question/<name>/<id>', methods=['DELETE'])
@cross_origin()
def deleteQuestionData(surveyId, name, id):
  try:
    token = request.headers.get("token-Authorization")
    if not token:
      return ('missed token', 412)

    connection = Database().getConnection()
    UserController().findUserBySurveyAndToken(surveyId, token, connection)
    cursor = connection.cursor(dictionary=True)
    result = QuestionController().deleteAnswerById(id, name, connection)
    return (json.dumps(result, default=str), 200)
  except AuthorizationError as err:
    traceback.print_exc()
    Logger.error(str(err))
    return (str(err), 401)
  except NotFoundError as err:
    traceback.print_exc()
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    traceback.print_exc()
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/survey/<surveyId>/questions', methods=['GET'])
@cross_origin()
def getSurveyQuestions(surveyId):
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    """
    checkanswer = request.args.get('answered')
    if checkanswer is not None:
      checkanswer = (checkanswer == "true")
    """

    connection = Database().getConnection()
    result = SurveyController().getQuestions(surveyId, connection)
    result = [item["question"] for item in result]
    return (jsonify(result), 200)
    """
    if checkanswer is None:
      return (jsonify(result), 200)
    else:
      tmp = []
      ctrl = QuestionController()
      for name in result:
        if ctrl.questionAnswered(surveyId, name, connection) == checkanswer:
          tmp.append(name)

      return (jsonify(tmp), 200)
    """
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/survey/<surveyId>/question/<name>/answered', methods=['GET'])
@cross_origin()
def getQuestionAnswered(surveyId, name):
  try:
    indent = request.args.get('indent')
    connection = Database().getConnection()
    updater = QuestionController()
    result = updater.questionAnswered(surveyId, name, indent, connection)
    return (jsonify({'answered':result}), 200)
  except Exception as err:
    Logger.error(err, exc_info=True)
    traceback.print_exc()
    return (str(err), 500)

@bp.route('/survey/<surveyId>/question/<name>/shown', methods=['PUT'])
@cross_origin()
def updateQuestionShown(surveyId, name):
  try:
    body = request.get_json()
    connection = Database().getConnection()
    updater = QuestionController()
    indent = body['indent'] if 'indent' in body else None
    result = updater.updateQuestionShown(connection, surveyId, name, indent)
    return (jsonify({'shown':result}), 200)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

'''
TODO: remove this routes
@bp.route('/measurements/<measurementId>/start', methods=['POST'])
@cross_origin()
def measurementStart(measurementId):
  try:
    result = PoolSchedulers().start(measurementId)
    result['start_date'] = result['start_date'].strftime(dateformat)
    result['stop_date'] = result['stop_date'].strftime(dateformat)
    return jsonify(result)
  except Exception as err:
    traceback.print_exc()
    Logger.error(str(err))
    return (str(err), 412)

@bp.route('/measurements/<measurementId>/stop', methods=['POST'])
@cross_origin()
def measurementStop(measurementId):
  try:
    connection = Database().getConnection()
    notificationController = NotificationController(connection)
    notificationController.createMeasurementStopNotification(measurementId)

    result = PoolSchedulers().stop(measurementId)
    result['start_date'] = result['start_date'].strftime(dateformat)
    result['stop_date'] = result['stop_date'].strftime(dateformat)
    return jsonify(result)

  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)
'''

@bp.route('/user/<id>/notifications', methods=['GET'])
@cross_origin()
def getNotifications(id):
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    connection = Database().getConnection()
    UserController()._findUserByIdAndToken(id, token, connection)
    controller = NotificationController(connection)
    result = controller.getNotifications(id)
    return jsonify(result)

  except AuthorizationError as err:
    Logger.error(str(err))
    return (str(err), 401)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/user/<id>/notifications', methods=['PUT'])
@cross_origin()
def delivered_notifications(id):
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    connection = Database().getConnection()
    UserController()._findUserByIdAndToken(id, token, connection)
    controller = NotificationController(connection)
    controller.mark_as_delivered(id, request.get_json())
    return ("OK", 200)

  except AuthorizationError as err:
    Logger.error(str(err))
    return (str(err), 401)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/facts', methods=['POST'])
@cross_origin()
def createFact():
  # TODO check admin credentials
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    json = request.get_json()
    body = json['body']
    emotion = json['emotion']
    db = Database()
    response = FactsController().insertFact(body, emotion, Database().getConnection())
    return jsonify(response)

  except KeyError as err:
    Logger.error(str(err))
    return ("bad request", 400)
  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/facts/<factId>', methods=['GET'])
@cross_origin()
def getFact(factId):
  # TODO check admin credentials
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    response = FactsController().getFactById(factId, Database().getConnection())
    return (jsonify(response), 200)

  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/facts/<factId>', methods=['PUT'])
@cross_origin()
def editFact(factId):
  # TODO check admin credentials
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    json = request.get_json()
    body = json['body']
    response = FactsController().editFact(factId, body, Database().getConnection())
    return (jsonify(response), 200)

  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/facts/<factId>', methods=['DELETE'])
@cross_origin()
def deleteFact(factId):
  # TODO check admin credentials
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    response = FactsController().deleteFact(factId, Database().getConnection())
    return (jsonify(response), 200)

  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/tips', methods=['POST'])
@cross_origin()
def createTip():
  # TODO check admin credentials
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    json = request.get_json()
    body = json['body']
    emotion = json['emotion']
    result = TipsController().insertTip(body, emotion, Database().getConnection())
    return (jsonify(result), 200)

  except (NotFoundError, KeyError) as err:
    Logger.error(str(err))
    return (str(err), 400)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/tips/<tipId>', methods=['PUT'])
@cross_origin()
def editTip(tipId):
  # TODO check admin credentials
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    json = request.get_json()
    body = json['body']

    connection = Database().getConnection()
    tip = TipsController().editTip(tipId, body, connection)
    return (jsonify(tip), 200)

  except NotFoundError as err:
    return (str(err), 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/tips/<tipId>', methods=['GET'])
@cross_origin()
def getTip(tipId):
  # TODO check admin credentirls
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    tip = TipsController().getTipById(tipId, Database().getConnection())
    return (jsonify(tip), 200)

  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 400)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/tips/<id>', methods=['DELETE'])
@cross_origin()
def deleteTip(id):
  # TODO check admin credentirls
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    connection = Database().getConnection()
    tip = TipsController().deleteTip(id, connection)
    return (jsonify(tip), 200)

  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/pictures/<name>', methods=['GET'])
@cross_origin()
def getImage(name):
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    if (AdminController().verifyAuthToken(token, Database().getConnection())):
      pictureObj = PicturesController().getPicture(name)
      return (jsonify(pictureObj), 200)
    else:
      return ('access denied', 401)

  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 404)

  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/emotions', methods=['POST'])
@cross_origin()
def createEmotion():
  # TODO check admin credentials
  try:
    json = request.get_json()
    emotion = json['emotion']
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    connection = Database().getConnection()
    UserController().findUserByToken(token, connection)
    response = EmotionsController().insertEmotion(emotion, connection)
    return (jsonify(response), 200)

  except AuthorizationError as err:
    Logger.error(str(err))
    return (str(err), 401)
  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/emotions/<emotionId>', methods=['PUT'])
@cross_origin()
def editEmotion(emotionId):
  # TODO check admin credentials
  try:
    json = request.get_json()
    emotion = json['emotion']
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    response = EmotionsController().editEmotion(emotionId, emotion, Database().getConnection())
    return (jsonify(response), 200)

  except AuthorizationError as err:
    Logger.error(str(err))
    return (str(err), 401)
  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/emotions/<emotionId>', methods=['DELETE'])
@cross_origin()
def deleteEmotion(emotionId):
  # TODO check admin credentials
  try:
    json = request.get_json()
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    response = EmotionsController().deleteEmotion(emotionId, Database().getConnection())
    return (jsonify(response), 200)

  except AuthorizationError as err:
    Logger.error(str(err))
    return (str(err), 401)
  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/emotions', methods=['GET'])
@cross_origin()
def getEmotions():
  try:
    json = request.get_json()
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    response = EmotionsController().getEmotions(Database().getConnection())
    return (jsonify(response), 200)

  except AuthorizationError as err:
    Logger.error(str(err))
    return (str(err), 401)
  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/survey/<surveyId>/feedback', methods=['POST'])
@cross_origin()
def createFeedback(surveyId):
  try:
    json = request.get_json()
    emotion = json.get('emotion')
    if not emotion:
      return ("body must have emotion", 400)

    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    connection = Database().getConnection()
    user = UserController().findUserBySurveyAndToken(surveyId, token, connection)
    devicetoken = None
    try:
      device = UserController().findDeviceByUser(user['id'], connection)
      devicetoken = device['token']
    except NotFoundError as error:
      pass

    ctrl = FeedbackController()
    feedback = ctrl.createFeedbackAndNotification(user['id'], token, devicetoken, emotion, connection)
    return (jsonify(feedback), 200)

  except AuthorizationError as err:
    traceback.print_exc()
    Logger.error(str(err))
    return (str(err), 401)
  except NotFoundError as err:
    traceback.print_exc()
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    traceback.print_exc()
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/feedback/<feedbackId>/answer', methods=['PUT'])
@cross_origin()
def putFeedbackAnswer(feedbackId):
  try:
    json = request.get_json()
    message = json.get("message")
    token = request.headers.get("token-Authorization")
    if not token:
      return ('missed token', 412)

    if not message :
      return ('missed message', 400)

    connection = Database().getConnection()
    user = UserController().findUserByFeedbackAndToken(feedbackId, token, connection)
    feedback = FeedbackController().updateResponse(feedbackId, message, connection)
    return jsonify(feedback)

  except AuthorizationError as err:
    Logger.error(str(err))
    return (str(err), 401)
  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/feedback/<feedbackId>', methods=['GET'])
@cross_origin()
def getFeedback(feedbackId):
  token = request.headers.get('token-Authorization')
  if not token:
    return ('bad token', 412)

  try:
    connection = Database().getConnection()
    UserController().findUserByFeedbackAndToken(feedbackId, token, connection)
    feedbackObj = FeedbackController().getFeedbackObject(feedbackId, connection)
    return (jsonify(feedbackObj), 200)

  except AuthorizationError as err:
    Logger.error(str(err))
    return (str(err), 401)
  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

# TODO: change this route to get only one survey
# Move controller to another route
@bp.route('/survey/<surveyId>', methods=['GET'])
@cross_origin()
def getUserSurveys(surveyId):
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('empty token', 412)

    connection = Database().getConnection()
    user = UserController().findUserBySurveyAndToken(surveyId, token, connection)
    surveys = SurveyController().getUserSurveys(user['id'], connection)
    return (jsonify(surveys), 200)

  except AuthorizationError as err:
    Logger.error(str(err))
    return (str(err), 401)
  except NotFoundError as err:
    Logger.error(str(err))
    return (str(err), 404)
  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/survey/<surveyId>/expired', methods=['GET'])
@cross_origin()
def getSurveyExpired(surveyId):
  try:
    connection = Database().getConnection()
    result = SurveyController().getSurveyExpired(surveyId, connection)
    return (jsonify({'expired': result}), 200)

  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)

@bp.route('/survey', methods=['GET'])
@cross_origin()
def getSurveys():
  try:
    token = request.headers.get('token-Authorization')
    if not token:
      return ('missed token', 412)

    completed = request.args.get('completed')
    user = request.args.get('user')
    measurement = request.args.get('measurement')
    connection = Database().getConnection()
    if completed is not None:
      surveys = SurveyController().getSurveysByStatus(completed, user, measurement, connection)
      return (jsonify(surveys), 200)
    else:
      surveys = SurveyController().getUserSurveys(user, connection)
      return (jsonify(surveys), 200)

  except Exception as err:
    Logger.error(str(err))
    return (str(err), 500)


from Resources.Auth import Login, Auth;
from Resources.EmailsTemplates import EmailsTemplates, EmailsTemplate
from Resources.SchedulerEmails import SchedulerEmails, SchedulerEmail
from Resources.Questions import Questions
from Resources.Notifications import Notifications
from Resources.EmployeeSurveys import EmployeeSurveys, EmployeeSurvey
from Resources.EmployeeQuestions import EmployeeQuestions, EmployeeQuestion
from Resources.EmployeeControls import EmployeeControls, EmployeeControlsExt, EmployeeControl, EmployeeControlExt
from Resources.Schedulers import Scheduler, Schedulers
from Resources.Surveys import Surveys, SurveyLinks
from Resources.Measurements import Measurement, Measurements, MeasurementAction
from Resources.Employees import Employee, Employees
from Resources.Users import Users, User

# admin api
api.add_resource(Login, '/login')
api.add_resource(Users, '/users')
api.add_resource(User, '/users/<int:user>')
api.add_resource(Auth, '/users/<int:user>/auth/<string:action>')
# api.add_resource(Questions, '/questions')
api.add_resource(Notifications, '/users/<int:user>/notifications')
api.add_resource(Measurements, '/users/<int:user>/measurements')
api.add_resource(Measurement, '/users/<int:user>/measurements/<int:measurement>')
api.add_resource(MeasurementAction, '/users/<int:user>/measurements/<int:measurement>/action/<string:action>')
api.add_resource(Employees, '/users/<int:user>/measurements/<int:measurement>/employees')
api.add_resource(Employee, '/users/<int:user>/measurements/<int:measurement>/employees/<int:employee>')
api.add_resource(Schedulers, '/users/<int:user>/measurements/<int:measurement>/schedulers')
api.add_resource(Scheduler, '/users/<int:user>/measurements/<int:measurement>/schedulers/<int:scheduler>')
api.add_resource(EmailsTemplates, '/users/<int:user>/notifications/emails')
api.add_resource(EmailsTemplate, '/users/<int:user>/notifications/emails/<int:email>')
api.add_resource(SchedulerEmails, '/users/<int:user>/measurements/<int:measurement>/schedulers/<int:scheduler>/emails')
api.add_resource(SchedulerEmail, '/users/<int:user>/measurements/<int:measurement>/schedulers/<int:scheduler>/emails/<int:email>')

api.add_resource(Surveys, '/users/<int:user>/measurements/<int:measurement>/employees/<int:employee>/schedulers/<int:scheduler>/surveys')
api.add_resource(SurveyLinks, '/users/<int:user>/measurements/<int:measurement>/surveys/<int:survey>/links')

# employees api
api.add_resource(EmployeeSurveys, '/employees/<int:user>/surveys')
api.add_resource(EmployeeSurvey, '/employees/<int:user>/surveys/<int:survey>')

api.add_resource(EmployeeQuestions, '/employees/<int:user>/surveys/<int:survey>/questions')
api.add_resource(EmployeeQuestion, '/employees/<int:user>/surveys/<int:survey>/questions/<int:question>')

api.add_resource(EmployeeControls, '/employees/<int:user>/controls')
api.add_resource(EmployeeControl, '/employees/<int:user>/controls/<int:control>')
api.add_resource(EmployeeControlsExt, '/employees/<int:user>/surveys/<int:survey>/questions/<int:question>/controls')
api.add_resource(EmployeeControlExt, '/employees/<int:user>/surveys/<int:survey>/questions/<int:question>/controls/<int:control>')

urlPrefix = settings.api()
app.register_blueprint(bp, url_prefix=urlPrefix)
