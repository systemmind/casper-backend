from casper_db import NotFoundError
import random
import string
#from user_delete import deleteUserById 
#from configuration import glob as settings

class AuthorizationError(Exception):
  def __init__(self, message):
    super(AuthorizationError, self).__init__(message)


class DuplicateUser(Exception):
  def __init__(self, message):
    super(Exception, self).__init__(message)


class UserController(object):
  def _findUserByIdAndToken(self, userId, token, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM users WHERE id=%s AND token=%s AND authorized > 0;""", (userId, token))
    user = cursor.fetchone()
    if not user:
      raise AuthorizationError("user not authorized")

    return user

  def findUserBySurveyAndToken(self, survey, token, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT user FROM survey WHERE id=%s;""", (survey, ))
    user = cursor.fetchone()
    if not user:
      raise NotFoundError("survey not found")

    return self._findUserByIdAndToken(user["user"], token, connection)

  def findDeviceByUser(self, user, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM usersdevices WHERE user = %s;""", (user, ))
    device = cursor.fetchone()
    if not device:
      raise NotFoundError("device not found")

    return device

  def findUserByFeedbackAndToken(self, feedback, token, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT user FROM feedback WHERE id=%s;""", (feedback, ))
    user = cursor.fetchone()
    if not user:
      raise NotFoundError("feedback not found")

    return self._findUserByIdAndToken(user["user"], token, connection)

  def findUserByToken(self, token, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM users WHERE token=%s AND authorized > 0;""", (token, ))
    user = cursor.fetchone()
    if not user:
      raise AuthorizationError("user not authorized")

    return user

  def findUserById(self, id, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM users WHERE id=%s;""", (id, ))
    user = cursor.fetchone()
    if not user:
      raise NotFoundError("user not found")

    return user

  def getUsers(self, connection, measurement):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""
      SELECT users.*, useremails.email
        FROM users
   LEFT JOIN useremails
          ON users.id = useremails.user
       WHERE users.measurement=%s
    """, (measurement,))
    return cursor.fetchall()

  def createUser(self, measurement, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("SELECT * FROM measurements WHERE id = %s", (int(measurement),))
    if not cursor.fetchone():
      raise NotFoundError("measurement not found")

    token = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(128))
    cursor.execute("""INSERT INTO users (token, date, authorized, measurement)
      VALUES (%s, CURDATE(), 0, %s);""",
      (token, measurement))
    connection.commit()
    cursor.execute("""SELECT * FROM users WHERE token = %s;""", (token,))
    result = cursor.fetchone()
    result['authorized'] = (result['authorized'] > 0)
    return result

  def createUserAndDevice(self, measurement, device, connection):
    cursor = connection.cursor(dictionary=True)
    obj = self.createUser(measurement, connection);
    user = obj['id']
    cursor.execute("""INSERT INTO usersdevices (user, token)
      VALUES (%s, %s);""",
      (user, device))
    connection.commit()
    cursor.execute("""SELECT * FROM usersdevices WHERE user = %s;""", (user,))
    result = cursor.fetchone()
    obj['device'] = result
    return obj

  def createUserAndEmail(self, connection, measurement, email):
    cursor = connection.cursor(dictionary=True)
    cursor.execute(f"""
      SELECT *
        FROM users
   LEFT JOIN useremails
          ON users.id = useremails.user
       WHERE users.measurement={measurement}
         AND useremails.email={email}
    """)
    result = cursor.fetchall()
    if len(result) > 0:
      raise DuplicateUser(f"user already exists: {str(result)}")

    obj = self.createUser(measurement, connection)
    user = obj['id']

    cursor.execute(
      """INSERT INTO useremails (user, email) VALUES (%s, %s)""",
      (user, email)
    )

    result = cursor.fetchone()
    obj['email'] = result
    return obj

  def authorizeUserToken(self, userId, token, obj, connection):
    cursor = connection.cursor(dictionary=True)
    user = self.findUserById(userId, connection)
    athorized = 1 if obj['authorized'] is True else 0
    if user['token'] != token:
      raise AuthorizationError("invalid token")

    cursor.execute("""UPDATE users SET authorized = %s WHERE token = %s""", (athorized, token))
    connection.commit()
    cursor.execute("""SELECT * FROM users WHERE id = %s;""", (userId,))
    result = cursor.fetchone()
    result['authorized'] = (result['authorized'] > 0)
    return result

  '''
  def deleteUser(self, connection, user):
    cursor = connection.cursor(dictionary=True)
    deleteUserById(cursor, user)
  '''

