#!/usr/bin/python3
import sys
import argparse
from datetime import datetime
from casper_db import Database
from survey_controller import SurveyController

parser = argparse.ArgumentParser()
parser.add_argument("-m", "--measurement", help="measurement")
parser.add_argument("-u", "--user", help="user")
parser.add_argument("-d", "--date", help="start date of user registration, ex: 30 01 2020")
parser.add_argument("-t", "--till", help="end date of user registration, ex: 30 01 2020")
parser.add_argument("-b", "--begin", help="begin date surveys, ex: 30 01 2020")
parser.add_argument("-e", "--end", help="end date surveys, ex: 30 01 2020")

args = parser.parse_args()

if args.measurement is None:
  print("--measurement argument requred")
  sys.exit(-1)

user = None
if args.user is not None:
  user = int(args.user)

date = None
if args.date is not None:
  date = args.date

till = None
if args.till is not None:
  till = args.till

begin = None
if args.begin is not None:
  begin = args.begin

end = None
if args.end is not None:
  end = args.end

measurement = args.measurement
connection = Database().getConnection()
cursor = connection.cursor(dictionary=True)

qCtl = SurveyController()

date = f" AND date >= STR_TO_DATE('{date}', '%d %m %Y')" if date else ""
till = f" AND date <= STR_TO_DATE('{till}', '%d %m %Y')" if till else ""
qUsers = f"""
  SELECT *
    FROM users
  WHERE measurement = {measurement}
    AND authorized = 1
    {date}
    {till}
"""

cursor.execute(qUsers)
users = cursor.fetchall()

if user:
  users = filter(lambda x: x["id"] == user, users)

print("user,total surveys,completed surveys,response rate")

summrate = 0.0

# amount of users who had answered more than 1 question
active = 0
# amount of users who had answered on 1 question only (probably demographic)
single = 0
# amount of users who have not responded on any question
zero = 0
# total amount of surveys
totalsurveys = 0
for user in users:
  b = f" AND survey.begin >= STR_TO_DATE('{begin}', '%d %m %Y')" if begin else ""
  e = f" AND survey.end <= STR_TO_DATE('{end}', '%d %m %Y')" if end else ""
  qSurveys = f"""
    SELECT survey.*
      FROM survey
    LEFT JOIN users
        ON survey.user = users.id
     WHERE survey.user = {user["id"]}
      {b}
      {e}
  """
  cursor.execute(qSurveys)
  surveys = cursor.fetchall()

  completedSurveys = qCtl.getSurveysByStatus('true', int(user["id"]), measurement, connection)

  total = len(surveys)
  totalsurveys = totalsurveys + total;
  completed = len(completedSurveys)
  if completed > 2:
    active = active + 1

  if completed == 1:
    single = single + 1

  if completed == 0:
    zero = zero + 1

  rate = completed*100/total if total else 0
  summrate = summrate + rate
  print(f"{user['id']},{len(surveys)},{len(completedSurveys)},{rate}")

if users:
  total = len(users)
  average = summrate/total
  print(f"{average:1f} average response rate")
  print(f"{total} total amount of users")
  print(f"{totalsurveys} total amount of surveys")
  print(f"{active} people who had answered more than 2 surveys")
  print(f"{single} people who had answered on 1 survey only")
  print(f"{zero} people who had not answered at all")
