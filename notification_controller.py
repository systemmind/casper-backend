from datetime import datetime, timedelta
import json
from string import Template
from casper_logger import Logger
from configuration import glob as settings
from picture_controller import PicturesController
from xmppcontroller import XmppControllerXmlrpc as XmppController
import fcm

class NotificationController(object):
  def __init__(self, connection):
    self._connection = connection
    self._cursor = self._connection.cursor(dictionary=True)
    self._xmpp = XmppController()

  def getNotifications(self, id):
    self._cursor.execute("SELECT id, body FROM notifications WHERE user = %s AND NOW() >= created AND NOW() < expired AND delivered IS NULL", (id,))
    notifications = self._cursor.fetchall()
    result = []
    for notification in notifications:
      result.append(notification)

    return result

  def mark_as_delivered(self, user, notifications):
    params = [(a, user) for a in notifications]
    self._cursor.executemany("UPDATE notifications SET delivered = NOW() WHERE id = %s AND user = %s;", params)
    self._connection.commit()

  def insertFeedbackNotification(self, user, link, picture, linkApiAnswer):
    templ_string_message = "<p>Hi!</p><p>Daily feedback is available <a href='$href'>here</a></p>"
    message = Template(templ_string_message).substitute(href=link)
    tray_message = "Hi! Daily feedback is available."
    templ_string_picture = "<p>Hi!</p><p>Daily feedback is available <a href='$href'>here</a></p>"
    answer = {"url": linkApiAnswer, "expired": 3600}  #  3600 seconds timer value for qt application. After 3600 seconds user can not send answer on feedback
    obj = {
      "message_chat": message,
      "message_tray": tray_message,
      "answer": answer
    }

    if picture is not None:
      obj.update({"picture": picture})

    body = json.dumps(obj)
    delay = str(datetime.now())
    expired = str(datetime.now() + timedelta(hours=24))
    notification = self.insertNotification(user, body, delay, expired)
    return notification

  def insertNotification(self, user, body, created, expired):
    self._cursor.execute("""INSERT INTO notifications (user, body, created, expired, delivered)
      VALUES (%s,%s,%s, %s, NULL);""",
      (user, body, created, expired))
    Logger.debug(body)
    id = self._cursor.lastrowid
    self._connection.commit()
    self._cursor.execute("SELECT * FROM notifications WHERE id = %s;", (id,))
    result = self._cursor.fetchone()
    return result

  """
  def createSurveyLink(self, host, surveyId, questions, userToken):
    userToken = str(userToken)
    surveyId = int(surveyId)
    host = str(host)
    questions = list(map(lambda x: 'question=' + x, questions))
    questions = '&'.join(questions)
    templ_string_link = 'https://${client}/survey?survey=${survey_id}&${questions}&token=${token}'
    return Template(templ_string_link).substitute(client=host, survey_id=surveyId, questions=questions, token=userToken)
  """

  def createSurveyLink(self, host, user, survey, loader, userToken):
    userToken = str(userToken)
    user=int(user)
    survey = int(survey)
    host = str(host)
    return f"https://{host}/survey?user={user}&survey={survey}&loader={loader}&token={userToken}"

 
  def createFeedbackNotification(self, user, token, feedbackId, picture, connection):
    notificationController = NotificationController(connection)
    linkApiAnswer = self.createAnswerLink(feedbackId)
    linkFeedback = self.createFeedbackLink(feedbackId, token)
    pict = None
    if picture:
      pict = PicturesController().getPicture200x200(picture)["picture"]

    notification = self.insertFeedbackNotification(user, linkFeedback, pict, linkApiAnswer)
    return notification

  def sendFeedbackPushNotification(self, device, token, feedbackId, picture, connection):
    notificationController = NotificationController(connection)
    link = self.createFeedbackLink(feedbackId, token)
    msg = "Hi! Your daily feedback is available."
    fcm.sendMessage(device, msg, None, link)

  def sendFeedbackXmppNotification(self, user, token, feedbackId, picture):
    link = self.createFeedbackLink(feedbackId, token)
    msg = "Hi! Your daily feedback is available."
    #self._xmpp.sendMessage("notification", f"user{user}", json.dumps({"message": msg, "link": link}))
    self._xmpp.sendMessage("notification", f"user{user}", self.createGlobal200x200PictureLink(picture))
    self._xmpp.sendMessage("notification", f"user{user}", msg)
    self._xmpp.sendMessage("notification", f"user{user}", link)

  def createStaticNotification(self, user, link, loader, delay, expired):
    created = str(delay)
    expired = str(expired)
    messageChat = self.createStaticChatMessage(loader, link)
    messageTray = self.createStaticTrayMessage(loader, link)
    body = json.dumps({"message_chat": messageChat, "message_tray": messageTray})
    notification = self.insertNotification(user, body, created, expired)
    return notification

  def createRemindNotification(self, user, link, delay, expired):
    start = str(delay)
    end = str(expired)
    messageChat = self.createRemindChatMessage(link)
    messageTray = "You have an incomplete survey"
    body = json.dumps({"message_chat": messageChat, "message_tray": messageTray})
    notification = self.insertNotification(user, body, start, end)
    return notification

  def sendSurveyPushNotification(self, device, link, loader):
    messageTray = self.createStaticTrayMessage(loader, link)
    fcm.sendMessage(device, messageTray, None, link)

  def sendSurveyXmppNotification(self, user, link, loader):
    messageTray = self.createStaticTrayMessage(loader, link)
    #self._xmpp.sendMessage("notification", f"user{user}", json.dumps({"message_tray": messageTray, "link": link}))
    self._xmpp.sendMessage("notification", f"user{user}", messageTray)
    self._xmpp.sendMessage("notification", f"user{user}", link)
    #self._xmpp.sendMessage("notification", f"user{user}", f"<a href='{link}'>{messageTray}</a>")

  def sendSurveyPushNotificationMsg(self, device, link, msg):
    fcm.sendMessage(device, msg, None, link)

  def sendSurveyXmppNotificationMsg(self, user, link, msg):
    #self._xmpp.sendMessage("notification", f"user{user}", json.dumps({"message": msg, "link": link}))
    self._xmpp.sendMessage("notification", f"user{user}", msg)
    self._xmpp.sendMessage("notification", f"user{user}", link)

  def createDemographicNotification(self, user, link, measurementId):
    measurementId = int(measurementId)
    link = str(link)
    user = int(user)
    start = str(datetime.now())
    self._cursor.execute("""SELECT stop_date FROM measurements WHERE id = %s;""", (measurementId, ))
    stopDate = self._cursor.fetchone()
    messageChat = self.createDemographicChatMessage(link)
    body =  json.dumps({"message_chat": messageChat, "message_tray": "Demographic survey is available."})
    notification = self.insertNotification(user, body, start, stopDate['stop_date'])
    return notification

  def sendDemographicNotification(self, device, link):
    title = "Demographic survey is available."
    body = "Hi! Your demographic survey is available"
    fcm.sendMessage(device, title, body, link)

  def createSupportNotification(self, user, measurementId):
    measurementId = int(measurementId)
    #link = "https://thehappinessatworkproject.com/index.php/en/about-the-project/happiness-measurement-tool"
    user = int(user)
    start = str(datetime.now())
    self._cursor.execute("""SELECT stop_date FROM measurements WHERE id = %s;""", (measurementId, ))
    stopDate = self._cursor.fetchone()
    #messageChat = self.createSupportChatMessage(link)
    messageChat = '<p>If you are experiencing any problems, please visit our <a href="https://casper.thehappinessatworkproject.com/faq">FAQ page</a>. If your issue is still unresolved, contact a member of our tech team through support chat. Happy to help!</p>'
    body = json.dumps({"message_chat": messageChat, "message_tray": "Customer support is available."})
    notification = self.insertNotification(user, body, start, stopDate['stop_date'])
    return notification

  def createMeasurementStopNotification(self, measurementId):
    measurementId = int(measurementId)
    start = str(datetime.now())
    stop = str(datetime.now() + timedelta(days=365240))
    messageChat = self.createMeasurementStopMessage(measurementId)
    body = json.dumps({"message_chat": messageChat, "message_tray": "Measurement stopped."})
    self._cursor.execute("""SELECT id FROM users WHERE measurement = %s;""", (measurementId, ))
    users = self._cursor.fetchall()
    for user in users:
      self.insertNotification(user['id'], body, start, stop)
    return users

  def createAnswerLink(self, feedbackId):
    templ_link_answer = 'https://${client}/api/feedback/${feedbackId}/answer'
    return Template(templ_link_answer).substitute(client=settings.server(), feedbackId=feedbackId)

  def createFeedbackLink(self, feedbackId, token):
    templ_string_link = 'https://${client}/feedback?id=${feedbackId}&token=${token}'
    return Template(templ_string_link).substitute(client=settings.client(),
                                                    feedbackId=feedbackId,
                                                    token=token)

  def createGetPictureLink(self, picture):
    templ_string_picture = 'https://${client}/api/pictures/${picture}'
    return Template(templ_string_picture).substitute(client=settings.server(), picture=picture)

  def createGlobal200x200PictureLink(self, picture):
    return f'https://{settings.serverDomain()}/pictures/{picture}'

  def createStaticChatMessage(self, loader, link):
    if loader == "singleevening":
      return f"<p>Hi!</p><p>Your survey is available <a href='{link}'>here</a>.</p>"
    else:
      return f"<p>Hi!</p><p>Your {loader} survey is available <a href='{link}'>here</a>.</p>"

  def createRemindChatMessage(self, link):
    message = "<p>Hi!</p><p>You have an incomplete survey <a href='$href'>here.</a></p>"
    return Template(message).substitute(href=link)

  def createStaticTrayMessage(self, loader, link):
    if loader == 'singleevening':
      return "Hi! Your survey is available."

    return f"Hi! Your {loader} survey is available."

  def createDemographicChatMessage(self, link):
    message = "<p>Hi!</p><p>Your demographic survey is available <a href='$href'>here.</a></p>"
    return Template(message).substitute(href=link)

  '''
  def createSupportChatMessage(self, link):
    message = "<p>Our customer support live chat is available <a href='$href'>here.</a></p>"
    return Template(message).substitute(href=link)
  '''

  def createMeasurementStopMessage(self, measurementId):
    message = "<p>Measurement $id has been stopped</p>"
    return Template(message).substitute(id=measurementId)
