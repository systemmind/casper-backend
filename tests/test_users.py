from flask import jsonify, json
import re
import pytest


#Users FOREIGN KEY (measurement) REFERENCES measurements (id))
@pytest.fixture(scope='module')
def measurement(init):
  (client, adminToken) = init
  rv = client.post("/measurements", headers=[
    ('Content-Type', 'application/json'),
    ('token-Authorization', adminToken)
  ],
  json={
    "start_date":"2011-08-20 15:30:00",
    "stop_date":"2020-08-20 15:30:00",
    "name":"goko",
    "company_url":"http://hokololol.com",
    "manager_email":"poko@gmail.com",
    "welcome_email":"GG",
    "welcome_email_id": 1,
    "started": 133
  })
  
  data = rv.get_json()
  yield data['id']

  rv = client.delete("/measurements/1", headers=[
    ('Content-Type', 'application/json'),
    ('token-Authorization', adminToken)
  ])

def test_create_User(init, measurement):
  (client, adminToken) = init
  mid = measurement
  rv = client.post("/users", headers=[
    ('Content-Type', 'application/json'),
    ('token-Authorization', adminToken)
  ],
  json={
    "measurement": mid,
    "authorized": 1,
    "emails":["gondor"],
    "devices":["midi"]
  })

  data = rv.get_json()
  assert data["id"] == mid
  assert rv.status_code == 200


def test_read_User(init, measurement):
  (client, adminToken) = init
  mid = measurement 
  rv = client.get("/users/1", headers=[
    ('Content-Type', 'application/json'),
    ('token-Authorization', adminToken)
    ]) 

  data = rv.get_json()
  assert [data['token']] == re.findall(r'\w[A-Za-z0-9]*', data['token'])
  assert data['measurement'] == mid
  assert rv.status_code == 200

def test_read_Users(init, measurement):
  (client, adminToken) = init
  rv = client.get("/users", headers=[
    ('Content-Type', 'application/json'),
    ('token-Authorization', adminToken)
    ])

  assert rv.status_code == 200


def test_edit_User(init, measurement):
  (client, adminToken) = init 
  rv = client.put("/users/1", headers=[
    ('Content-Type', 'application/json'),
    ('token-Authorization', adminToken)
  ],
  json={
    "authorized": 0
  })

  data = json.loads(rv.data)
  assert data['rowcount'] == 1
  assert rv.status_code == 200  

def test_delete_User(init, measurement):
  (client, adminToken) = init
  rv = client.delete("/users/1", headers=[
    ('Content-Type', 'application/json'),
    ('token-Authorization', adminToken)
  ])

  data = json.loads(rv.data)
  assert data['rowcount'] == 1
  assert rv.status_code == 200  
