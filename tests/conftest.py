import pytest
from flask import json
from api import app
from casper_db import Database
from configuration import glob as settings
from admin_controller import AdminController

@pytest.fixture(scope="session")
def init():
  DBUSER = 'caspertest'
  DBPASSWORD = 'test'
  DBPASSHASH = b'$2b$12$DcZy.EZVBHDKhSf1nQz3Bu2jjx6SdX9QIGMuJgR710fzzi2M1taQO'
  db = Database()
  connection = Database().getConnection()
  cursor = connection.cursor()
  cursor.execute(f"DROP DATABASE {settings.databaseName()}")
  cursor.execute(f"CREATE DATABASE {settings.databaseName()}")
  cursor.execute(f"USE {settings.databaseName()}")
  connection.commit()
  db.connect()
  db.init()
  AdminController().createUser(DBUSER, DBPASSHASH, connection)
  #log#cursor.execute("insert into admin_token (user_id, token, expired) values ('2', 'DJRL0UL15IUKRCKSGCYDOMGVNF24BK12B4SJ1PO79CNMWR1QH5FAE3M85FO4A0ZPDCAP31J4YSRN7YGCYHCBA0BU9IUJYFEAD3ZJ0SZL7ZI84SIEDESTADEAGHIX0GOX', '2022-12-10 15:30:00');")
  connection.commit()


  app.config['TESTING'] = True
  client = app.test_client()
  request = client.post("/login", json={
    'login': DBUSER,
    'password': DBPASSWORD
  })
  
  data = json.loads(request.data)
  adminToken = data['token']
  yield client, adminToken

