import pytest
import re


def test_login(init):
  (client, adminToken) = init
  assert [adminToken] == re.findall(r'\w[A-Za-z0-9]*', adminToken)