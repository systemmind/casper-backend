from flask import jsonify, json
import pytest
import re


def test_create_Measurement(init):
  (client, adminToken) = init
  rv = client.post("/measurements", headers=[
    ('Content-Type', 'application/json'),
    ('token-Authorization', adminToken)
  ],
  json={
    "start_date":"2011-08-20 15:30:00",
    "stop_date":"2020-08-20 15:30:00",
    "name":"goko",
    "company_url":"http://hokololol.com",
    "manager_email":"poko@gmail.com",
    "welcome_email":"GG",
    "welcome_email_id": 1,
    "started": 1
  })
  
  data = rv.get_json()
  assert data['id'] == 1
  assert rv.status_code == 200
 
    
def test_read_Measurements(init):
  (client, adminToken) = init
  rv = client.get("/measurements", headers=[
    ('Content-Type', 'application/json'),
    ('token-Authorization', adminToken)
  ])

  data = rv.get_json()
  assert isinstance(data, list)
  for item in data:
    assert isinstance(item['id'], int)
    assert re.match(r'(Sat|Sun|Mon)\, [0-9]{,2} (Aug|Mon) [0-9]{,4} \d{1,2}:\d{1,2}:\d{1,2} (GMT)', item['start_date'])
  
  assert rv.status_code == 200

def test_read_Measurement(init):
  (client, adminToken) = init
  rv = client.get("/measurements/1", headers=[
    ('Content-Type', 'application/json'),
    ('token-Authorization', adminToken)
  ])

  data = rv.get_json()
  assert data['start_date'] == "Sat, 20 Aug 2011 00:00:00 GMT"
  assert data['stop_date'] == "Thu, 20 Aug 2020 00:00:00 GMT"
  assert data['name'] == "goko"
  assert data['company_url'] == "http://hokololol.com"
  assert data['manager_email'] == "poko@gmail.com"
  assert data['welcome_email'] == "GG"
  assert data['welcome_email_id'] == 1
  assert data['started'] == 1
  assert rv.status_code == 200
  

def test_edit_Measurement(init):
  (client, adminToken) = init
  rv = client.put("/measurements/1", headers=[
    ('Content-Type', 'application/json'),
    ('token-Authorization', adminToken)
  ],
  json={
    "start_date":"2011-08-20 15:45:00",
    "stop_date":"2020-08-20 15:45:00",
    "name":"goko_dendi",
    "company_url":"http://hokololol_2.com",
    "manager_email":"pokopo@gmail.com",
    "welcome_email":"GG_com"
  })

  data = rv.get_json()
  assert data['rowcount'] == 1
  assert rv.status_code == 200


def test_delete_Measurement(init):
  (client, adminToken) = init
  rv = client.delete("/measurements/1", headers=[
    ('Content-Type', 'application/json'),
    ('token-Authorization', adminToken)
  ])
  
  data = rv.get_json()
  assert data['rowcount'] == 1
  assert rv.status_code == 200
