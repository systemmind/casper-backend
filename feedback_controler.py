import random
from notification_controller import NotificationController
from picture_controller import PicturesController
import mysql.connector
from tip_controller import TipsController
from fact_controller import FactsController
from emotion_controller import EmotionsController
from casper_db import NotFoundError

class FeedbackController(object):

  def createFeedback(self, user, emotion, connection):
    emotion = EmotionsController().getEmotionByName(emotion, connection)
    emotionId = emotion["id"]
    emotionName = emotion["emotion"]
    facts = FactsController().getFactsByEmotion(emotionId, connection)
    index = random.randint(0,len(facts)-1)
    fact = facts[index]
    tips = TipsController().getTipsByEmotion(emotionId, connection)
    index = random.randint(0,len(tips)-1)
    tip = tips[index]
    try:
      pictures = PicturesController().getPicturesByEmotion(emotionName)
      index = random.randint(0, len(pictures)-1)
      picture = pictures[index]
    except NotFoundError as err:
      picture = None

    feedback = self.insertFeedback(user, emotionId, fact["id"], tip["id"], picture, connection)
    assert(feedback is not None)
    return feedback

  def createFeedbackAndNotification(self, user, token, device, emotion, connection):
    user = int(user)
    token = str(token)
    emotion = str(emotion)
    feedback = self.createFeedback(user, emotion, connection)
    feedbackId = feedback["id"]
    fact = feedback["fact"]
    tip = feedback["tip"]
    picture = feedback["picture"]
    ctrl = NotificationController(connection)
    ctrl.createFeedbackNotification(user, token, feedbackId, picture, connection)
    ctrl.sendFeedbackXmppNotification(user, token, feedbackId, picture)
    if device:
      ctrl.sendFeedbackPushNotification(device, token, feedbackId, picture, connection)

    return feedback

  def _getFeedbackById(self, feedbackId, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM feedback WHERE id = %s""", (feedbackId, ))
    feedback = cursor.fetchone()
    if not feedback:
      raise NotFoundError("feedback not found")

    return feedback

  def updateResponse(self, feedbackId, response, connection):
    self._getFeedbackById(feedbackId, connection)
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""UPDATE feedback SET response = %s WHERE id = %s""", (response, feedbackId))
    connection.commit()
    return self._getFeedbackById(feedbackId, connection)

  def getFeedbackObject(self, feedbackId, connection):
    feedbackId = int(feedbackId)
    cursor = connection.cursor(dictionary=True)
    feedback = self._getFeedbackById(feedbackId, connection)
    factId = feedback["fact"]
    tipId = feedback["tip"]
    pictureName = feedback["picture"]

    tip = TipsController().getTipById(tipId, connection)
    fact = FactsController().getFactById(factId, connection)
    feedbackObj = {
      "tip": tip["body"],
      "fact": fact["body"]
    }
    if pictureName:
      try:
        pictureObj = PicturesController().getPicture(pictureName)
        feedbackObj.update(pictureObj)
      except NotFoundError as err:
        pass

    return feedbackObj

  def insertFeedback(self, userId, emotionId, factId, tipId, picture, connection):
    cursor = connection.cursor(dictionary=True)
    if picture:
      cursor.execute("""INSERT INTO feedback (user, date, fact, tip, emotion, picture)
                        VALUES(%s, NOW(), %s, %s, %s, %s);""",
                        (userId, factId, tipId, emotionId, picture))
    else:
      cursor.execute("""INSERT INTO feedback (user, date, fact, tip, emotion, picture)
                        VALUES(%s, NOW(), %s, %s, %s, NULL);""",
                        (userId, factId, tipId, emotionId))

    id = cursor.lastrowid
    connection.commit()
    return self._getFeedbackById(id, connection)
