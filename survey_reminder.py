from question_controller import QuestionController
from notification_controller import NotificationController
import threading
from datetime import datetime
from casper_db import Database


class SurveyReminder(object):
  def __init__(self, db):
    self._db = db
    self._questions = []
    self._reminds = {}

  def start(self, time, expired):
    timer = threading.Timer(time, self.remindAll, [expired])
    timer.start()

  def appendRemind(self, userId, surveyId, link):
    tmpRemind = {
      userId: {
        "surveyId": surveyId,
        "link": link
        }
      }

    self._reminds.update(tmpRemind)

  def isHaveUnAnswered(self, surveyId):
    for question in self._questions:
      if QuestionController().questionAnswered(str(surveyId), question, Database().getConnection()):
        continue
      else:
        return True
    
    return False

  def initQuestions(self, questions):
    self._questions.extend(questions)

  def remindAll(self, expired):
    keys = self._reminds.keys()
    connection = self._db.getConnection()
    controller = NotificationController(connection)
    for key in keys:
      if self.isHaveUnAnswered(self._reminds[key]["surveyId"]):
        controller.createRemindNotification(key, self._reminds[key]["link"], datetime.now(), expired)

    connection.close()
    del self._questions[:]
    self._reminds.clear()
