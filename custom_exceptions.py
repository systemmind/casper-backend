class UserNotFoundError(Exception):
  pass

class PasswordMismatchError(Exception):
  pass

class MeasurementExpiredError(Exception):
  def __init__(self, id):
    super().__init__("measurement %s expired", (id))

class MeasurementNotFoundError(Exception):
  def __init__(self, id):
    super().__init__("measurement %s not found", (id))
