from flask import request, jsonify
from casper_db import Database
from admin_controller import AdminController
from scheduler_read import readSchedulerById, readSchedulersByMeasurement
from scheduler_create import createScheduler
from scheduler_edit import editScheduler
from scheduler_delete import deleteSchedulerById

def checkToken(connection, token):
  return AdminController().verifyAuthToken(token, connection)

def routeGetSchedulers(measurement):
  connection = Database().getConnection()
  token = request.headers.get('token-Authorization')
  if not checkToken(connection, token):
    return ('access denied', 401)

  result = readSchedulersByMeasurement(connection, measurement)
  return (jsonify(result), 200)

def routeGetScheduler(measurement, scheduler):
  connection = Database().getConnection()
  token = request.headers.get('token-Authorization')
  if not checkToken(connection, token):
    return ('access denied', 401)

  result = readSchedulerById(connection, scheduler)
  return (jsonify(result), 200)

def routeCreateScheduler(measurement):
  connection = Database().getConnection()
  token = request.headers.get('token-Authorization')
  if not checkToken(connection, token):
    return ('access denied', 401)

  body = request.get_json()
  result = createScheduler(connection, measurement=measurement, **body)

  return (jsonify({"id": result}), 200)

def routeEditScheduler(measurement, scheduler):
  connection = Database().getConnection()
  token = request.headers.get('token-Authorization')
  if not checkToken(connection, token):
    return ('access denied', 401)

  body = request.get_json()
  editScheduler(connection, int(scheduler), **body)

  return ({}, 200)

def routeDeleteScheduler(measurement, scheduler):
  connection = Database().getConnection()
  token = request.headers.get('token-Authorization')
  if not checkToken(connection, token):
    return ('access denied', 401)

  deleteSchedulerById(connection, scheduler)
  return ({}, 200)
