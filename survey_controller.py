import mysql.connector
from casper_db import NotFoundError
from casper_logger import Logger as logger
from datetime import datetime
from datetime import timedelta

class SurveyController(object):
  def createSurvey(self, user, measurement, scheduler, connection, expire=None):
    cursor = connection.cursor(dictionary=True)
    if not expire:
      expire = datetime.now() + timedelta(minutes=120)

    '''
    cursor.execute("""
        INSERT INTO survey (begin, end, user, measurement)
          VALUES (NOW(), DATE_ADD(NOW(), INTERVAL 120 MINUTE), %s, %s);""",
            (user, measurement))
    '''

    cursor.execute("""
        INSERT INTO survey (begin, end, user, measurement, scheduler)
          VALUES (NOW(), %s, %s, %s, %s);""",
            (expire, user, measurement, scheduler))

    id = cursor.lastrowid
    connection.commit()
    return int(id)

  def addQuestions(self, survey, questions, connection):
    cursor = connection.cursor(dictionary=True)
    values = [(survey, name) for name in questions]
    cmd = f"INSERT INTO surveyquestions (survey, question) VALUES (%s, %s);"
    cursor.executemany(cmd, values)
    connection.commit()

  def getQuestions(self, survey, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM surveyquestions WHERE survey = %s;""", (survey,))
    questions = cursor.fetchall()
    return questions

  # TODO: finish the method
  def createSurveyRetDict(self, user, measurement, connection):
    surveyId = self.createSurvey(user, measurement, connection)
    # TODO: get record by id and return dictionary!

  # FIXME: rename getUserSurveys() by getSurveysByUser()
  def getUserSurveys(self, user, connection):
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM survey WHERE user = %s;""", (user,))
    surveys = cursor.fetchall()
    if not surveys:
      raise NotFoundError("user surveys not found")

    return surveys

  def getSurveyExpired(self, surveyId, connection):
    cursor = connection.cursor()
    cursor.execute("""SELECT end FROM survey WHERE id = %s;""", (surveyId,))
    survey = cursor.fetchone()
    if not survey:
      raise NotFoundError("survey not found")

    cursor.execute("""SELECT * FROM survey WHERE id = %s AND end > NOW();""", (surveyId,))
    nonexpired = cursor.fetchone()
    return nonexpired is None


  def getSurveysByStatus(self, completed, user, measurement, connection):
    cursor = connection.cursor(dictionary=True)
    completed = True if completed == 'true' else False
    values = (measurement, user) if user else (measurement,)
    
    qSurveys = f"""
      SELECT *
        FROM survey
      WHERE measurement = %s
      {"AND user = %s" if user else ""}
    """

    qQuestion = f"""
      SELECT Surveys.id, surveyquestions.question
        FROM ({qSurveys}) AS Surveys
      LEFT JOIN surveyquestions
          ON surveyquestions.survey = Surveys.id
    """

    cursor.execute(qQuestion,  values)
    result = cursor.fetchall()

    qCompletedSurveys = self.getCompletedSurveysQuery(result, cursor)

    resultQuery = f"""
      SELECT Surveys.*
        FROM ({qSurveys}) AS Surveys
   LEFT JOIN ({qCompletedSurveys}) AS CompletedSurveys
          ON Surveys.id = CompletedSurveys.id
       WHERE CompletedSurveys.id IS {"NOT" if completed == True else ""} NULL
    """

    cursor.execute(resultQuery, values)
    result = cursor.fetchall()
    return result


  # The surveyset should be some subset of surveys table with their questions
  def getCompletedSurveysQuery(self, surveyset, cursor):
    surveys = {}

    for item in surveyset:
      surveys.setdefault(item["id"], []).append(item["question"])

    if not surveys:
      raise NotFoundError("surveys not found")

    completedQueries = []
    for key in surveys:
      surveyId = key
      questions = surveys[key]

      query = {}

      for question in questions:
        if question == "activity":
          query["question_activity"] = "question_activity.activity IS NOT NULL"
        elif question == "duration":
          query["question_duration"] = "question_duration.duration IS NOT NULL"
        elif question == "reason":
          query["question_reason"] = "question_reason.reason IS NOT NULL"
        elif question == "agree":
          query["question_agree"] = "question_agree.date IS NOT NULL"
        elif question == "todo":
          query["question_todo"] = "question_todo.date IS NOT NULL"
        elif question == "age":
          query["question_age"] = "question_age.age IS NOT NULL"
        elif question == "gender":
          query["question_gender"] = "question_gender.gender IS NOT NULL"
        elif question == "experience":
          query["question_experience"] = "question_experience.experience IS NOT NULL"
        elif question == "happiness":
          query["question_happiness"] = "question_happiness.happiness IS NOT NULL"
        elif question == "productivity":
          query["question_productivity"] = "question_productivity.productivity IS NOT NULL"
        elif question == "overalldayevaluation":
          query["question_overalldayevaluation"] = "question_overalldayevaluation.value is not null"
        elif question == "reasonfear":
          query["question_reasonfear"] = "question_reasonfear.value is not null"
        elif question == "experiencefears":
          query["question_experiencefears"] = "question_experiencefears.value is not null"
        elif question == "energylevel":
          query["question_energylevel"] = "question_energylevel.value is not null"
        elif question == "tensionrate":
          query["question_tensionrate"] = "question_tensionrate.value is not null"
        elif question == "sleepquality":
          query["question_sleepquality"] = "question_sleepquality.value is not null"
        elif question == "sleepduration":
          query["question_sleepduration"] = "question_sleepduration.value is not null"
        elif question == "feeling":
          # TODO: modify algorythm to include this part
          # query["question_feeling"] = "question_feeling.date IS NOT NULL"
          continue
        elif question == "rubbishbin":
          continue
        elif question == "goodactivities":
          continue

      if not query:
        continue

      tablesJoin = []
      for table in query:
        tablesJoin.append(f"""
          LEFT JOIN {table}
                 ON survey.id = {table}.survey
        """)

      # find id of one completed survey
      tmp = f"""
        SELECT survey.id
          FROM survey
          {" ".join(tablesJoin)}
         WHERE {" AND ".join(query.values())}
           AND survey.id = {surveyId}
      """

      completedQueries.append(tmp)

    # combine ids of all completed surveys
    unionQuery = " UNION ".join(completedQueries)

    return unionQuery
