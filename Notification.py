import json
from datetime import datetime, timedelta
from casper_db import Database
import fcm
import smtplib
from email.message import EmailMessage
from xmppcontroller import XmppControllerXmlrpc as XmppController
from configuration import glob as defaultSettings

class Notification:
  def __init__(self, **kwargs):
    self._body = str(kwargs['body'])
    self._title = str(kwargs['title'] if 'title' in kwargs else "")
    self._users = list(kwargs['users'] if 'users' in kwargs else [])

  def send(self, *args, **kwargs):
    pass


class Static(Notification):
  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self._expired = kwargs['expired']  # seconds

  def send(self, connection, *args, **kwargs):
    cursor = connection.cursor()
    created = datetime.now()
    expired = datetime.now() + timedelta(seconds=self._expired)
    msg = json.dumps({"message_chat": self._body, "message_tray": self._title})
    ids = []
    for user in self._users:
      cursor.execute("""
        insert into notifications (user, body, created, expired, delivered)
        values (%s,%s,%s, %s, NULL);
      """, (user, msg, created, expired))
      ids.append(cursor.lastrowid)

    connection.commit()

    return ids


class Google(Notification):
  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self._link = kwargs['link']
    self._tokens = kwargs['tokens'] if 'tokens' in kwargs else None

  def send(self, connection=None, *args, **kwargs):
    if not self._tokens:
      self._tokens = self._getTokens(connection)

    for (token,) in self._tokens:
      fcm.sendMessage(token, self._title, self._body, self._link)

  def _getTokens(self, connection):
    cursor = connection.cursor()
    where = " or ".join([f"user={user}" for user in self._users])
    cursor.execute(f"""
      select token from usersdevices where {where}
    """)
    return cursor.fetchall() or ()


class Xmpp(Notification):
  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self._type = str(kwargs['type'])
    self._xmpp = XmppController()

  def send(self, *args, **kwargs):
    for user in self._users:
      self._xmpp.sendMessage(self._type, f'user{user}', self._body)


class Email(Notification):
  def __init__(self, settings=defaultSettings, **kwargs):
    super().__init__(**kwargs)
    self._emails = kwargs['emails'] if 'emails' in kwargs else None
    self._type = kwargs['type'] if 'type' in kwargs else 'plain'
    self._host = settings.smtpHost()
    self._port = settings.smtpPort()
    self._from = settings.smtpUser()
    self._passwd = settings.smtpPassword()

  def send(self, connection=None, *args, **kwargs):
    if not self._emails:
      self._emails = self._getEmails(connection)

    server = self._getServer()
    for email in self._emails:
      server.send_message(self._getEmailMsg(email))

  def _getEmails(self, connection):
    cursor = connection.cursor()
    where = " or ".join([f"user={user}" for user in self._users])
    query = f"""
      select email from useremails where {where}
    """
    cursor.execute(query)
    return cursor.fetchall() or ()

  def _getEmailMsg(self, to):
    msg = EmailMessage()
    msg['From'] = self._from
    msg['To'] = to
    msg['Subject'] = self._title
    msg.add_alternative(self._body, subtype=self._type)
    return msg

  def _getServer(self):
    server = smtplib.SMTP(host=self._host, port=self._port)
    server.starttls()
    server.login(self._from, self._passwd)
    return server


def create(kind, **kwargs):
  classes = {
    'static': Static,
    'google': Google,
    'xmpp': Xmpp,
    'email': Email
  }

  return classes[kind](**kwargs)
