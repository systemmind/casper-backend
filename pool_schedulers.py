from singleton import Singleton
from static_scheduler import StaticScheduler
import mysql.connector
import time
from custom_exceptions import MeasurementExpiredError
from casper_db import Database

class Pool:
  def start(self, measurement_id):
    connection = self._db.getConnection()
    cursor = connection.cursor(dictionary=True)
    cursor.execute("UPDATE measurements SET started=1 WHERE id = %s;", (measurement_id,))
    scheduler = StaticScheduler.create(self._db, int(measurement_id), lambda x: self.__stopStaticScheduler(x))
    # Don't start scheduler, because crontab is used
    # scheduler.start()
    self._measurements.append(scheduler)
    connection.commit()
    cursor.execute("SELECT * FROM measurements WHERE id = %s;", (measurement_id,))
    result = cursor.fetchone()
    connection.close()
    return result

  def stop(self, measurement_id):
    connection = self._db.getConnection()
    cursor = connection.cursor(dictionary=True)
    for i in range(len(self._measurements)):
      if self._measurements[i]._measurement['id'] == int(measurement_id):
        self._measurements[i].stop()
        self._measurements.remove(self._measurements[i])
        cursor.execute("UPDATE measurements SET started=0 WHERE id = %s;", (measurement_id,))
        connection.commit()
        cursor.execute("SELECT * FROM measurements WHERE id = %s;", (measurement_id,))
        result = cursor.fetchone()
        connection.close()
        return result

    connection.close()
    return ('measurement not found', 404)

  def restore(self, db):
    self._measurements = []
    self._db = db
    connection = self._db.getConnection()
    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT * FROM measurements WHERE started > 0 AND stop_date >= DATE(NOW());""")
    measurements = cursor.fetchall()
    try:
      for measurement in measurements:
        scheduler = StaticScheduler(self._db, measurement, lambda x: self.__stopStaticScheduler(x))
        scheduler.start()
        self._measurements.append(scheduler)
    except MeasurementExpiredError as err:
      Logger.error(str(err))
    finally:
      connection.close()

  def __stopStaticScheduler(self, scheduler):
    self._measurements.remove(scheduler)

class PoolSchedulers(Singleton, Pool):
  pass
